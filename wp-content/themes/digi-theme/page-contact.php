<?php
/**
* Template Name: Contact us
*/
?>

<?php get_header(); ?>

<main class="contact-us">
    <div class="container">
        <section class="request-quote-contact">
            <h2 class="h2 bottom-margin">Request A Qoute</h2>
            <?php echo FrmFormsController::get_form_shortcode( array( 'id' => 4, 'title' => false, 'description' => false ) ); ?>
        </section>
    </div>

    <section class="contact-map-section">
        <div class="container">
            <div class="row">

                <div class="col-md-5">
                    <div class="footer-contact-info">
                        <p class="mt-4">Hyex products have been sourced from the world’s leading Hygiene and Washroom
                            Manufacturers</p>
                        <div class="map-box-info">
                            <span class="poppins-light d-flex contact-phone">
                                <span class="material-icons mr-4">phone</span><a class="map-box-phone" href="tel:<?php the_field('phone_number', 'options'); ?>"><?php the_field('phone_number', 'options'); ?></a>
                            </span>
                            <span class="poppins-light d-flex">
                                <span class="material-icons mr-4">email</span><?php the_field('email', 'options'); ?>
                            </span>

                            <div class="footer-address d-flex">
                                <span class="material-icons mr-4">location_on</span>
                                <span class="poppins-light"><?php the_field('address', 'options'); ?></span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-7">
                    <iframe
                        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3317.2372466131246!2d151.14938511505633!3d-33.754534680687975!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6b12a8862c73aa9b%3A0x5e4a94a6ac76ea5!2sExecutive%20Centre%2C%20Suite%201A%20Level%202%2F802%20Pacific%20Hwy%2C%20Gordon%20NSW%202072%2C%20Australia!5e0!3m2!1sen!2s!4v1622029838861!5m2!1sen!2s"
                        width="100%" height="450" style="border:0; border-radius: 5px;" allowfullscreen=""
                        loading="lazy">
                    </iframe>
                </div>
            </div>
            <!--row-->
        </div>
        <!--container-->
    </section>

    <?php get_template_part("content"); ?>
</main>

<?php get_footer(); ?>

<?php
// ADD CUSTOM POST TYPE IF NEEDED

function custom_post_type_service() {
	$labels = [
		"name"               => _x("Services", "post type general name"),
		"singular_name"      => _x("Service", "post type singular name"),
		"add_new"            => _x("Add New", "Service"),
		"add_new_item"       => __("Add New Service"),
		"edit_item"          => __("Edit Service"),
		"new_item"           => __("New Service"),
		"all_items"          => __("All Services"),
		"view_item"          => __("View Service"),
		"search_items"       => __("Search Services"),
		"not_found"          => __("No Services found"),
		"not_found_in_trash" => __("No Services found in the Trash"),
		"parent_item_colon"  => "",
		"menu_name"          => "Services"
	];

	$args = [
		"labels"        => $labels,
		"description"   => "Holds our Services",
		"public"        => true,
		"menu_position" => 8,
		"supports"      => ["title", "editor", "thumbnail", "post-formats" ,"custom-fields"],
		"has_archive"   => true,
		"menu_icon"   => "dashicons-admin-site-alt3",
    "rewrite" => array("slug" => "services","with_front" => false),
	];
	register_post_type("service", $args);
}
add_action("init", "custom_post_type_service");


// ADD CUSTOM POST TYPE IF NEEDED

function custom_post_type_testimonial() {
	$labels = [
		"name"               => _x("Testimonials", "post type general name"),
		"singular_name"      => _x("Testimonial", "post type singular name"),
		"add_new"            => _x("Add New", "Testimonial"),
		"add_new_item"       => __("Add New Testimonial"),
		"edit_item"          => __("Edit Testimonial"),
		"new_item"           => __("New Testimonial"),
		"all_items"          => __("All Testimonials"),
		"view_item"          => __("View Testimonial"),
		"search_items"       => __("Search Testimonials"),
		"not_found"          => __("No Testimonials found"),
		"not_found_in_trash" => __("No Testimonials found in the Trash"),
		"parent_item_colon"  => "",
		"menu_name"          => "Testimonials"
	];

	$args = [
		"labels"        => $labels,
		"description"   => "Holds our Testimonials and Event specific data",
		"public"        => true,
		"menu_position" => 9,
		"supports"      => ["title", "editor", "thumbnail", "post-formats" ,"custom-fields"],
		"has_archive"   => true,
	];
	register_post_type("testimonial", $args);
}
add_action("init", "custom_post_type_testimonial");

?>


<?php
/* WYSIWYG defaults */
/** change tinymce's paste-as-text functionality */
function paste_as_text($mceInit, $editor_id) {
	//turn on paste_as_text by default
	//NB this has no effect on the browser's right-click context menu's paste!
	$mceInit["paste_as_text"] = true;
	return $mceInit;
}
add_filter("tiny_mce_before_init", "paste_as_text", 1, 2);

/** Set the Attachment Display Settings, This function is attached to the 'after_setup_theme' action hook. */
function default_attachment_display_setting() {
	update_option("image_default_align", "left");
	update_option("image_default_link_type", "none");
	update_option("image_default_size", "large");
}
add_action("after_setup_theme", "default_attachment_display_setting");

// CUSTOM MENUS
function custom_menus() {
	register_nav_menus(
		[
			"primary-menu" => __("Primary Menu"),
			"secondary-menu" => __("Secondary Menu"),
			"footer-menu" => __("Footer Menu"),
		]
	);
}
add_action("init", "custom_menus");

// create an ID from a user entered string and removing any unwanted symbols
function create_id($string) {
	$new_id = preg_replace('/[^a-zA-Z]/', '', $string);
	$new_id = strtolower(str_replace(" ", "", $new_id));
	return $new_id;
}

function post_back_link() {
	if (wp_get_referer()) {
		$prev_url = $_SERVER['HTTP_REFERER'];
		return "<a href='". $prev_url ."' class='back-link'><svg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' aria-hidden='true' focusable='false' style='-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);' preserveAspectRatio='xMidYMid meet' viewBox='0 0 512 512'><path d='M216.4 163.7c5.1 5 5.1 13.3.1 18.4L155.8 243h231.3c7.1 0 12.9 5.8 12.9 13s-5.8 13-12.9 13H155.8l60.8 60.9c5 5.1 4.9 13.3-.1 18.4-5.1 5-13.2 5-18.3-.1l-82.4-83c-1.1-1.2-2-2.5-2.7-4.1-.7-1.6-1-3.3-1-5 0-3.4 1.3-6.6 3.7-9.1l82.4-83c4.9-5.2 13.1-5.3 18.2-.3z'></path><rect x='0' y='0' width='512' height='512' fill='rgba(0, 0, 0, 0)'></rect></svg></a>";
	}

}

function check_username(){
    $email =  sanitize_email( $_POST['mail']);
    $exists = email_exists($email);
    if ( $exists ){
        $response = 'That E-mail is registered';
        echo $response;
    }
    die();

}

add_action( 'wp_ajax_check_username', 'check_username' );

add_action('wp_ajax_nopriv_check_username', 'check_username');



function new_user() {
    $useremail  =   sanitize_email( $_POST['mail'] );
    $password   =   $_POST['pass'] ;
    $remember   =  $_POST['checked'] ;
    $status   =  $_POST['status'] ;
    $fname   =  $_POST['fname'] ;
    $lname   =  $_POST['lname'] ;
    $comp_name   =  $_POST['comp_name'] ;
    $country=$_POST['country'] ;
    $receive_status=$_POST['receive_status'];
    $user_id = wp_create_user( $useremail,$password,$useremail );
    $wp_user_obj = new WP_User($user_id);
    $wp_user_obj->set_role($status);
    update_user_meta( $user_id, 'billing_first_name', $fname );
    update_user_meta( $user_id, 'billing_last_name',  $lname );
    update_user_meta( $user_id, 'billing_company', $comp_name );
    //update_user_meta( $user_id, 'billing_phone', $phone );

    update_field('country', $country, 'user_'.$user_id.'');
    update_field('receive_information', $receive_status, 'user_'.$user_id.'');
    //update_field('comment', $comment, 'user_'.$user_id.'');
    update_user_meta( $user_id, 'billing_country', $country );
    update_user_meta( $user_id, 'first_name', $fname );
    if($status=='partner_role' || $status=='dealer_partner_role'){
    }else{
        wp_update_user( array(
            'ID' => $user_id,
        ) );
    }
    wp_set_auth_cookie( $user_id, true );


}

add_action( 'wp_ajax_new_user', 'new_user' );

add_action('wp_ajax_nopriv_new_user', 'new_user');

function mytheme_add_woocommerce_support() {
    add_theme_support( 'woocommerce' );
}
add_action( 'after_setup_theme', 'mytheme_add_woocommerce_support' );

add_theme_support('woocommerce');


function my_front_end_login_fail( $username ) {
    $referrer = $_SERVER['HTTP_REFERER'];  // where did the post submission come from?
    // if there's a valid referrer, and it's not the default log-in screen
    if ( !empty($referrer) && !strstr($referrer,'wp-login') && !strstr($referrer,'wp-admin') ) {
        if ( !strstr($referrer, '?login=failed') ) {

// Redirect to the login page and append a querystring of login failed
            wp_redirect( $referrer . '?login=failed' );
        } else {
            wp_redirect( $referrer );
        } exit;
    }

}
add_action( 'wp_login_failed', 'my_front_end_login_fail' );  // hook failed login

function wti_loginout_menu_link( $items, $args ) {
    ob_start();
    $items_array = array();
    while (false !== ($item_pos = strpos($items, '<li', 4))) {
        $items_array[] = substr($items, 0, $item_pos);
        $items = substr($items, $item_pos);
    }
    $items_array[] = $items;
    if (is_user_logged_in()) {
        array_splice($items_array, 11, 0, '
        <li class="dropdown login-menu-button">
           <span class="dropdown-toggle" data-toggle="dropdown" type="button" aria-haspopup="true" aria-expanded="false" href="'. wp_logout_url() .'">'. __("<span class=\"iconify\" data-icon=\"simple-line-icons:logout\" data-inline=\"false\"></span>") .'</span>
           <ul class="dropdown-menu">
           <li><a href="'.get_permalink(icl_object_id(87035, 'page', true)).'">'. __("My account","digi-theme") .'</a></li>
           <li> <a class=""  href="'. wp_logout_url() .'">
           '. __("Log Out","digi-theme") .'</a></li>
           </ul>
           </li>
           '); // insert custom item after 2nd one
        $items = implode('', $items_array);
    } else {
        array_splice($items_array, 10, 0, '<li class="d-xs-none d-sm-none d-md-none d-lg-block  d-xl-block right login-menu-button ">
         <a class="" href="' . home_url('/login') . '">
    '. __("Log In","digi-theme") .'
         </a></li>'); // insert custom item after 2nd one
        $items = implode('', $items_array);
    }
    ob_end_clean();
    return $items;

}

add_filter( 'wp_nav_menu_main-menu_items', 'wti_loginout_menu_link', 10, 2 );
add_filter( 'wp_nav_menu_main-menu-french_items', 'wti_loginout_menu_link', 10, 2 );


function auto_redirect_after_logout(){
    wp_redirect( home_url() );
    exit();

}
add_action('wp_logout','auto_redirect_after_logout');
// To change add to cart text on single product page
add_filter( 'woocommerce_product_single_add_to_cart_text', 'woocommerce_custom_single_add_to_cart_text' );
function woocommerce_custom_single_add_to_cart_text() {

    return __('Add to Cart', 'woocommerce');
}

// To change add to cart text on product archives(Collection) page
add_filter( 'woocommerce_product_add_to_cart_text', 'woocommerce_custom_product_add_to_cart_text' );
function woocommerce_custom_product_add_to_cart_text() {
    return __('Add to Cart', 'woocommerce');
}
add_filter( 'woocommerce_pagination_args' , 'tq73et_override_pagination_args' );
function tq73et_override_pagination_args( $args ) {
    $args['prev_text'] = __( '<i class="fas fa-chevron-left"></i> Prev' );
    $args['next_text'] = __( 'Next <i class="fas fa-chevron-right"></i>' );
    return $args;
}

add_action( 'wp_head' , 'custom_quantity_fields_css' );
function custom_quantity_fields_css(){
    ?>
    <style>
        .quantity input::-webkit-outer-spin-button,
        .quantity input::-webkit-inner-spin-button {
            display: none;
            margin: 0;
        }
        .quantity input.qty {
            appearance: textfield;
            -webkit-appearance: none;
            -moz-appearance: textfield;
        }
    </style>
    <?php
}


add_action( 'wp_footer' , 'custom_quantity_fields_script' );
function custom_quantity_fields_script(){
    ?>
    <script type='text/javascript'>
        jQuery( function( $ ) {
            if ( ! String.prototype.getDecimals ) {
                String.prototype.getDecimals = function() {
                    var num = this,
                        match = ('' + num).match(/(?:\.(\d+))?(?:[eE]([+-]?\d+))?$/);
                    if ( ! match ) {
                        return 0;
                    }
                    return Math.max( 0, ( match[1] ? match[1].length : 0 ) - ( match[2] ? +match[2] : 0 ) );
                }
            }
            // Quantity "plus" and "minus" buttons
            $( document.body ).on( 'click', '.plus, .minus', function() {
                var $qty        = $( this ).closest( '.quantity' ).find( '.qty'),
                    currentVal  = parseFloat( $qty.val() ),
                    max         = parseFloat( $qty.attr( 'max' ) ),
                    min         = parseFloat( $qty.attr( 'min' ) ),
                    step        = $qty.attr( 'step' );

                // Format values
                if ( ! currentVal || currentVal === '' || currentVal === 'NaN' ) currentVal = 0;
                if ( max === '' || max === 'NaN' ) max = '';
                if ( min === '' || min === 'NaN' ) min = 0;
                if ( step === 'any' || step === '' || step === undefined || parseFloat( step ) === 'NaN' ) step = 1;

                // Change the value
                if ( $( this ).is( '.plus' ) ) {
                    if ( max && ( currentVal >= max ) ) {
                        $qty.val( max );
                    } else {
                        $qty.val( ( currentVal + parseFloat( step )).toFixed( step.getDecimals() ) );
                    }
                } else {
                    if ( min && ( currentVal <= min ) ) {
                        $qty.val( min );
                    } else if ( currentVal > 0 ) {
                        $qty.val( ( currentVal - parseFloat( step )).toFixed( step.getDecimals() ) );
                    }
                }

                // Trigger change event
                $qty.trigger( 'change' );
            });
        });
    </script>
    <?php
}


function get_current_product_category(){

    global $post;

    $terms = get_the_terms( $post->ID, 'product_cat' );

    $nterms = get_the_terms( $post->ID, 'product_tag'  );

    foreach ($terms  as $term  ) {

        $product_cat_id = $term->term_id;

        $product_cat_name = $term->name;

        break;

    }

    echo '<p class="product_cat">'.$product_cat_name.'</p>';;

}
// add_action( 'woocommerce_single_product_summary', 'get_current_product_category', 2 );

function so_32545673_add_sku(){
    global $product;
    if( $product->get_sku() ) {
        echo '<p class="n_sku">SKU: '.$product->get_sku().'</p>';
    }
}
add_action( 'woocommerce_single_product_summary', 'so_32545673_add_sku', 7 );

function add_qy(){

        echo 'Quantity';

}

add_action( 'woocommerce_template_single_sharing', 'add_qy', 49);

remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );

add_filter( 'woocommerce_product_tabs', 'documents_custom_tab' );

function documents_custom_tab( $tabs ) {

    $tabs['documents_custom_tab'] = array(
        'title'    => 'Documents',
        'callback' => 'documents_custom_tab_content', // the function name, which is on line 15
        'priority' => 50,
    );

    return $tabs;

}

function documents_custom_tab_content( $slug, $tab ) {

    echo '<h2>' . $tab['title'] . '</h2><p>Tab Content. You can display something in PHP here as well.</p>';

}
if ( ! function_exists( 'display_product_additional_information' ) ) {

    function display_product_additional_information($atts) {

        // Shortcode attribute (or argument)
        $atts = shortcode_atts( array(
            'id'    => ''
        ), $atts, 'product_additional_information' );

        // If the "id" argument is not defined, we try to get the post Id
        if ( ! ( ! empty($atts['id']) && $atts['id'] > 0 ) ) {
            $atts['id'] = get_the_id();
        }

        // We check that the "id" argument is a product id
        if ( get_post_type($atts['id']) === 'product' ) {
            $product = wc_get_product($atts['id']);
        }
        // If not we exit
        else {
            return;
        }

        ob_start(); // Start buffering

//        do_action( 'woocommerce_single_product_summary', $product );
        do_action( 'woocommerce_product_additional_information', $product );

        return ob_get_clean(); // Return the buffered outpout
    }

    add_shortcode('product_additional_information', 'display_product_additional_information');

}

if ( ! function_exists( 'woocommerce_default_product_tabs' ) ) {

    /**
     * Add default product tabs to product pages.
     *
     * @param array $tabs Array of tabs.
     * @return array
     */
    function woocommerce_default_product_tabs( $tabs = array() ) {
        global $product, $post;

        // Description tab - shows product content.
//        if ( $post->post_content ) {
            $tabs['description'] = array(
                'title'    => __( 'Description', 'woocommerce' ),
                'priority' => 10,
                'callback' => 'woocommerce_product_description_tab',
            );
//        }

        // Additional information tab - shows attributes.
        if ( $product && ( $product->has_attributes() || apply_filters( 'wc_product_enable_dimensions_display', $product->has_weight() || $product->has_dimensions() ) ) ) {
            $tabs['additional_information'] = array(
                'title'    => __( 'Additional information', 'woocommerce' ),
                'priority' => 20,
                'callback' => 'woocommerce_product_additional_information_tab',
            );
        }

        // Reviews tab - shows comments.
        if ( comments_open() ) {
            $tabs['reviews'] = array(
                /* translators: %s: reviews count */
                'title'    => sprintf( __( 'Reviews (%d)', 'woocommerce' ), $product->get_review_count() ),
                'priority' => 30,
                'callback' => 'comments_template',
            );
        }

        return $tabs;
    }
}
add_filter( 'woocommerce_product_tabs', 'bbloomer_remove_product_tabs', 9999 );

function bbloomer_remove_product_tabs( $tabs ) {
    unset( $tabs['additional_information'] );
    return $tabs;
}

function the_breadcrumb() {
    echo '<ul id="crumbs">';
if (!is_home()) {
    echo '<li><a href="';
    echo get_option('home');
    echo '">';
    echo 'Home';
    echo "</a>/</li>";
    if (is_category() || is_single()) {
        echo  '<li>';
        $type = get_post_type();
        $link = get_post_type_archive_link( $type );
        if( $type == "service" ){
          $link = site_url('hygiene-services');
            echo '<a href=" '. $link.' ">'.'All Services'.'</a>' ;
        }else{
             echo '<a href=" '. $link.' ">'.get_post_type().'55</a>' ;
        };
        wp_get_post_terms(' /</li><li> ');
        if (is_single()) {
            echo "/</li><li>";
              the_title();

            echo '</li>';

        }
    } elseif (is_page()) {
        echo '<li>';
        $title= get_the_title();

        if( $title == "Hygiene Services" ||  $title == "hygiene services" ){
            $title = "All Services";
            echo  $title;
          }else{
           echo $title;
          };
        echo '</li>';
    }

}
elseif (is_tag()) {single_tag_title();}
elseif (is_day()) {echo"<li>Archive for "; the_time('F jS, Y'); echo'</li>';}
elseif (is_month()) {echo"<li>Archive for "; the_time('F, Y'); echo'</li>';}
elseif (is_year()) {echo"<li>Archive for "; the_time('Y'); echo'</li>';}
elseif (is_author()) {echo"<li>Author Archive"; echo'</li>';}
elseif (isset($_GET['paged']) && !empty($_GET['paged'])) {echo "<li>Blog Archives"; echo'</li>';}
elseif (is_search()) {echo"<li>Search Results"; echo'</li>';}
echo '</ul>';
}
// add_action( 'pre_get_posts', 'my_set_posts_per_page' );

function my_set_posts_per_page( $query ){
  if ( is_admin() ){
    return;
  }

  //Check if this is a search
  if ( isset( $query->query['s'] ) ) {
    $query->set( 'posts_per_page',2 );
  }
}
function change_search_query() {
    if ( is_search() && isset($_GET['s'])) {
        $args = array();
        query_posts( $args );
    }
}
add_action('init', 'change_search_query');

function woo_hide_product_categories_widget( $list_args ) {
  $list_args[ 'hide_empty' ] = 1;
  return $list_args;
}

add_filter( 'woocommerce_product_categories_widget_args', 'woo_hide_product_categories_widget' );


function woocommerce_template_loop_product_title() {
  echo '<h3 class="' . esc_attr( apply_filters( 'woocommerce_product_loop_title_classes', 'woocommerce-loop-product__title' ) ) . '">' . get_the_title() . '</h3>'; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
}

function add_login_check()
{
    if (is_user_logged_in()) {
        if (is_page(87)){
            wp_redirect( site_url('my-account-2/'));
            exit;
        }
    }
}
add_action('wp', 'add_login_check');

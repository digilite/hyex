<?php get_header(); ?>


<div class="owl-carousel slider-section">
	<?php
if( have_rows("homepage_slider", "option") ):
    while( have_rows("homepage_slider", "option") ) : the_row();
        $image = get_sub_field("image", "option");
		$title = get_sub_field("slider_title", "option");
	?>

	<div class="item" style="background-image: url(<?= $image; ?>);">
		<div class="container slide">
			<div class="slide-content">
				<h1 class="h1"><?= $title; ?></h1>
				<a class="slider-button-link" href="tel:<?php the_field('phone_number', 'options'); ?>">
					<div class="slider-button">
						<i class="fas fa-phone-alt"></i>
						<span>Call now<br><?php the_field('phone_number', 'options'); ?></span>
					</div>
				</a>
			</div>
		</div>
	</div>
	<?php
    endwhile;
endif;
?>
</div>

<main>
	<div class="container">
		<section class="two-col-row">

			<div class="row">
				<div class="col-md-6 col-sm-6 border-right bg-white">
					<div class="first-row-box">
						<p class="heading-blue">Services</p>
						<div class="intro-image-box">
							<img src="<?php echo get_theme_file_uri("/img/cleaning-desktop-table.png") ?>" class="img-fluid" alt="cleaning">
						</div>
						<div class="intro-text-box overflow-hidden">
							<p>
								Our services include Air Freshening, Hand Dryer Rental, Feminine Hygiene, Hand Soap and
								Skin Care, Incontinence Bin.
								An outstanding reputation for quality, integrity and service, we can handle all your
								hygiene and washroom needs.
							</p>
						</div>
						<a class="two-col-row-link" href="<?= get_page_link(19939) ?>">
						Explore	More Services <span class="material-icons">chevron_right</span>
						</a>
					</div><!--ROW BOX-->
				</div>

				<div class="col-md-6 col-sm-6 bg-white">
					<div class="row-box">
						<p class="heading-blue">Products</p>
						<div class="intro-image-box">
							<img src="<?php echo get_theme_file_uri("/img/products-img.png") ?>" class="img-fluid" alt="cleaning">
						</div>
						<div class="intro-text-box overflow-hidden">
							<p>
								Hyex products have been sourced from the world’s leading Hygiene and Washroom
								Manufacturers.
								Our Innovative line of dispenser have endless possibilities and at Hyex we aim to
								provide you with products that are reliable.
							</p>
						</div>
						<a class="two-col-row-link" href="<?php echo site_url( '/shop/' ); ?>">Shop Online
							<span class="material-icons">chevron_right</span>
						</a>
					</div><!--ROW BOX-->
				</div>
			</div><!-- row-->

		</section>

	</div>

	<?php get_template_part("content"); ?>
</main>

<?php get_footer(); ?>

<?php
/*
Template Name: login
*/
get_header();
?>

<div class="container container-margin login-page">
    <h1 class="h2">
        Sing In
    </h1>
 <form name="loginform" class="form" id="loginform" action="<?php echo site_url();?>/wp-login.php" method="post" style="display: block;">
    <div class="error_block">
        <?php if(isset($_GET['login']) && $_GET['login'] == 'failed')
        { ?>
            <div class="error-login">
                <p>Login failed, wrong Username or Password</p>
            </div>

        <?php } ?>
    </div>
  <div class="form-row">
      <div class="form-group col-md-6">
          <label>Username</label>
          <input  type="text" name="log" id="user_login" class="input form-control" value="" size="20"
                  required>
      </div>
      <div class="form-group col-md-6">
          <label>Password</label>
          <input  type="password" name="pwd" id="user_pass" class="input form-control" value="" size="20"
                  required>
          <small class="forgot-pass form-text text-muted"><a href="<?php echo wp_lostpassword_url(); ?>">Password forgotten?</a></small>
      </div>
  </div>

    <div class="login-submit-block col text-center">
        <button  type="submit" name="wp-submit" id="wp-submit" class="yellow-btn">Sign In</button>
        <input type="hidden" name="redirect_to" value="<?php echo site_url();?>/my-account">
    </div>
</form>
</div>
<?php get_footer(); ?>

<?php 
/* Template Name: Service page */
get_header();
?>
<div class="container">
<?php the_breadcrumb(); ?>
<section class="featured-services">
    <div class="row">
    <?php  $args = array(
         'posts_per_page'   => -1,
         'post_type'        => 'service',
   );
    $query = new WP_Query($args);
    if ( $query->have_posts() ) {
        while ( $query->have_posts() ) {
            $query->the_post();
                
         ?>
         <?php get_template_part( "template-parts/featured", "services" ); ?>
        <?php
        }
        wp_reset_postdata();
    }
   ?>
   
    </div>

</section>

</div>
<main class="main-content">
    <?php get_template_part("content"); ?>
</main>

<?php get_footer(); ?>
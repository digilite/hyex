<?php 
 if (is_shop() ||  is_product_category()) {
    if( have_rows("page_content", "2831") ):
        // loop through the rows of data
        while ( have_rows("page_content", "2831") ) : the_row();
            switch (get_row_layout()) :
                case "two_column_content":
                    get_template_part("template-parts/content/two-col-text");
                    break;
                case "why_hyex":
                    get_template_part("template-parts/content/choose-hyex");
                    break;
                case "text_with_button":
                    get_template_part("template-parts/content/text-with-button");
                    break;
                case "show_popular_products":
                    get_template_part("template-parts/content/popular-products");
                    break;
                case "half_text_half_background":
                    get_template_part("template-parts/content/half-text-bg");
                    break;
                case "statistics":
                    get_template_part("template-parts/content/statistics");
                    break;
                case "display_services":
                    get_template_part("template-parts/content/display-services");
                    break;
                case "display_products":
                    get_template_part("template-parts/content/display-products");
                    break;
                case "full_width_background_with_text":
                    get_template_part("template-parts/content/full-text-bg");
                    break;
                case "feauterd_products":
                    get_template_part("template-parts/content/feauterd-products");
                    break;
            endswitch;
        endwhile;
    endif;
} else{
    if( have_rows("page_content") ):
        // loop through the rows of data
        while ( have_rows("page_content") ) : the_row();
            switch (get_row_layout()) :
                case "two_column_content":
                    get_template_part("template-parts/content/two-col-text");
                    break;
                case "why_hyex":
                    get_template_part("template-parts/content/choose-hyex");
                    break;
                case "text_with_button":
                    get_template_part("template-parts/content/text-with-button");
                    break;
                case "show_popular_products":
                    get_template_part("template-parts/content/popular-products");
                    break;
                case "half_text_half_background":
                    get_template_part("template-parts/content/half-text-bg");
                    break;
                case "statistics":
                    get_template_part("template-parts/content/statistics");
                    break;
                case "display_services":
                    get_template_part("template-parts/content/display-services");
                    break;
                case "display_products":
                    get_template_part("template-parts/content/display-products");
                    break;
                case "full_width_background_with_text":
                    get_template_part("template-parts/content/full-text-bg");
                    break;
                case "feauterd_products":
                    get_template_part("template-parts/content/feauterd-products");
                    break;
            endswitch;
        endwhile;
    endif;
}
?>
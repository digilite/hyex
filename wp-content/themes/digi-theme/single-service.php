<?php get_header(); ?>

<div class="container">
<?php the_breadcrumb(); ?>
    <div class="row">
        <div class="col-lg-8 col-sm-12">
                <?php
                if (have_posts()) :
                    while (have_posts()) :
                        the_post(); ?>
                        	<h2 class='h2'><?php the_title(); ?></h2>
                        <p><?php the_content(); ?></p><?php
                    endwhile;
                endif;
                
                if(get_field('get_a_free')){
                    $id_p=get_field('get_a_free');
                    ?>
             <div class="get-free"><img src="<?php bloginfo( 'template_url' ); ?>/img/free.svg">Get a FREE service when you sign up for Oxygen Powered  <a href="<?php get_permalink($id_p); ?>"> <?php  echo get_the_title( $id_p); ?> </a> service.</div>
                 <?php  }  ?>
        <section class="catalog">
           
            <?php
            if( have_rows('service_catalog') ):  
                while( have_rows('service_catalog') ): the_row(); 
            ?>
            
                        <div class="d-flex service_catalog_single">
                            <div class="img-product">
                                <img src="<?php the_sub_field('img'); ?>">                            
                            </div>
                            <div class="cont-product">
                                <div class="title-product">
                                    <?php the_sub_field('title'); ?>
                                </div>
                                <div class="dec-product">
                                    <p><?php the_sub_field('text'); ?></p>
                                </div>
                            </div>
                        </div>
                        
                    <?php endwhile; ?>
                
                <?php endif; ?>
                 
        </section>
         
        </div>
        <div class="col-lg-4 col-sm-12">
            <div class="blue-title-block">Request A Quote</div>
            <?php echo do_shortcode( '[formidable id=5]' ); ?>
        </div>
    </div>
</div>
<main class="main-content">
<?php get_template_part("content"); ?>
</main>
<section class="above-footer-contact">
<div class="full-width-bg" style="background-image: url(<?php the_field("contact_section_background", "option"); ?>);">
	<div class="container">
		<div class="full-width-dark-content">
			<h3 class="h3">Contact Us</h3>
			<p class="lighter-grey"><?php the_field("contact_section_about_text", "option"); ?></p>
			<a class="slider-button-link" href="tel:<?php the_field('phone_number', 'options'); ?>">
			<div class="slider-button">
				<i class="fas fa-phone-alt"></i>
				<span>Call now<br><?php the_field('phone_number', 'options'); ?></span>
			</div>
			</a>
		</div>
	</div>
</div>
</section>

<?php get_footer(); ?>
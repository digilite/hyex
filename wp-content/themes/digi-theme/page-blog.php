<?php
/**
* Template Name: Blog
*/
?>

<?php get_header(); ?>

<div class="container">
  <section class="blog-page">
    <?php

    $current = get_query_var('paged');

    $args = array(
        'post_type' => 'post',
        'posts_per_page' => 12,
        'paged' => $current
    );

    $post_query = new WP_Query($args);
	if ($post_query->have_posts()) : ?>
    <div class="row">
      <?php
	while ($post_query->have_posts()) :
        $post_query->the_post(); ?>
      <div class="col-lg-4 col-md-6 col-sm-6">
        <div class="blog-post-box">
          <div class="blog-thumb">
            <a class="featured-services-image" href="<?php the_permalink(); ?>">
              <img src="<?php the_post_thumbnail_url(); ?>" class="img-fluid blog-img" alt="">
            </a>
          </div>
          <h2 class="h6"><a href="<?php the_permalink(); ?>"><?php the_title() ?></a></h2>
        </div>
      </div>
      <?php
		endwhile; ?>

      <div class="pagination-links">
        <?php
        echo paginate_links(array(
            "total" => $post_query->max_num_pages,
        ));
        ?>
      </div>
      <!--pagination-->
    </div>
    <!--ROW-->
    <?php endif;

    wp_reset_postdata();?>
  </section>
</div>
<?php get_template_part("content"); ?>
<?php get_footer(); ?>

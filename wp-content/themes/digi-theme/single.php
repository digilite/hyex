<?php get_header(); ?>

<div class="container">
<section class="single-post"><?php
	if (have_posts()) : ?>
	<div class="row">
	<?php
	while (have_posts()) :
			the_post(); ?>
			<div class="col-md-8 single-content">
				<img src="<?php the_post_thumbnail_url(); ?>" class="img-fluid" alt="">
				<h1 class="h3"><?php the_title(); ?></h1>
				<div class="user-content">
					<?php the_content(); ?>
				</div>
				<a class="more-blog" href="<?php echo site_url('blog')?>">Read more Blogs</a>
			</div>

			<div class="col-md-4">
			<div class="blog-page">
			<?php
			$args = array(
				"post_type" => "post",
				"posts_per_page" => 7,
				"orderby" => "rand"
			);

			$post_query = new WP_Query($args);
			if ($post_query->have_posts()) : ?>
			<?php
			while ($post_query->have_posts()) :
				$post_query->the_post(); ?>
					<div class="blog-post-box">
					<div class="blog-thumb">
						<a class="featured-services-image" href="<?php the_permalink(); ?>">
							<img src="<?php the_post_thumbnail_url(); ?>" class="img-fluid blog-img" alt="">
						</a>
					</div>
						<h4 class="h6"><a href="<?php the_permalink(); ?>"><?php the_title() ?></a></h4>
					</div>
			<?php
			endwhile; ?>
			<?php endif;
			wp_reset_postdata();?>
			</div>
			</div>
		<?php
		endwhile; ?>
	</div><!--ROW-->
<?php endif; ?>
</section>
</div>

<?php get_footer(); ?>

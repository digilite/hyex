<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo('charset'); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?php echo wp_get_document_title(); ?></title>
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
	<link rel="preconnect" href="//fonts.gstatic.com">
	<link href="//fonts.googleapis.com/css2?family=Poppins:wght@400;500;600;700&display=swap" rel="stylesheet">
	<link href="//fonts.googleapis.com/css2?family=Lato:wght@400;700&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<!-- Generate favicon here http://www.favicon-generator.org/ -->
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
	<div class="request-btn">
		<a href="<?php echo site_url( '/contact-us' ) ?>"><button class="blue-btn">REQUEST A QUOTE</button></a>
	</div>
	<header itemscope itemtype="http://schema.org/WPHeader">

		<div class="topbar">
			<div class="container">
				<div class="topbar-content">
					<span class="topbar-left"><span class="material-icons mr-2">phone</span><a class="phone-link" href="tel:<?php the_field('phone_number', 'options'); ?>"><?php the_field('phone_number', 'options'); ?></a></span>
					<div class="topbar-right d-flex">
						<i class="fa fa-search pr-2" aria-hidden="true"></i>
						<form  method="get"  id="searchform" class="searchform" action="<?php bloginfo("url"); ?>" >
							<div class="mobile-drop-search">
								<input class="mobile-search-icon"  type="image" alt="Search" src="<?php bloginfo( 'template_url' ); ?>/img/search-blue.svg" />
								<input class="topbar-search-icon"  type="image" alt="Search" src="<?php bloginfo( 'template_url' ); ?>/img/search-black-18dp.svg" />
								<input class="topbar-search" type="text" name="s" id="search" placeholder="what are you looking for ?" value="<?php the_search_query(); ?>" />
							</div>
						</form>
						<a href="<?php echo site_url( '/sing-in' ) ?>"><i class="material-icons mr-2">person</i></a>
						<div class="cart-btn">
							<i class="material-icons">shopping_cart</i>
							<div class="cart-bag">
								<?php
								global $woocommerce;
								$items = $woocommerce->cart->get_cart();
								$amount2 =  $woocommerce->cart->get_cart_total();
								if($items){
								foreach($items as $item => $values) {
									$_product =  wc_get_product( $values['data']->get_id());
									$price = get_post_meta($values['product_id'] , '_price', true);
									$symbol= get_woocommerce_currency_symbol();
									?>
									<span class="d-flex cart-block">
										<div class="cart-bag-img">
											<?php  echo get_the_post_thumbnail($values['product_id']);?>
										</div>
										<div>
											<?php
											echo "<p class='bag-title'>".$_product->get_title()."<br><span class='bag-price'>".$symbol.$price."</span></p>";

											?>
										</div>
									</span>
								<?php   } ?>
								<div class="cart-block">
									<div class="subtotal-cart">
										<p>Subtotal:  <span><?php echo $amount2 ?></span></p>
									</div>
									<div class="d-flex justify-content-center">
										<a href="<?php echo wc_get_cart_url(); ?>" class="yellow-btn">View Cart</a>
										<a class="blue-btn">Check out</a>
									</div>
								</div>
								<?php } else { ?>
									<div class='empty-text'>Your Cart Is Empty</div>
							<?php	} ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="d-none-desktop">
			<div class="mobile-header-box ml-5">
				<a class="mobile-logo" itemprop="url" href="<?php echo bloginfo('url') ?>">
					<img itemprop="logo" src="<?php echo get_template_directory_uri(); ?>/img/Hyex-logo.svg">
				</a>
			</div><!-- mobile-header-box -->
		</div><!-- d-none-desktop -->

		<div class="container">
			<nav class="navbar-expand-lg navbar-light" itemscope itemtype="http://schema.org/SiteNavigationElement">

				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
					<span class="burger-one"></span>
					<span class="burger-two"></span>
					<span class="burger-three"></span>
				</button>

				<div class="collapse navbar-collapse" id="navbarNav">
				<?php
				wp_nav_menu([
					'theme_location' => 'primary-menu',
					'menu_class' => 'main-menu navbar-nav',
					'container' => '',
				]); ?>
				<a class="main-menu-btn" href="<?php echo site_url( '/contact-us' ) ?>"><button class="blue-btn">REQUEST A QUOTE</button></a>
				</div>

			</nav>
		</div><!-- container -->
	</header>
	<?php get_template_part( "template-parts/hero", "section" ); ?>

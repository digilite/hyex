<?php
/*
Hero section top featured image
*/

$post_thumbnail_id = get_post_thumbnail_id(get_the_ID());
$image_url = wp_get_attachment_image_url( $post_thumbnail_id, 'hero-thumb' );


if ( is_shop() || is_product_category()) {
    $title = woocommerce_page_title( false );
} else {
    $title = get_the_title(get_the_ID());
}

if ( is_product_category() ){
    global $wp_query;

    $cat = $wp_query->get_queried_object();
    $thumbnail_id = get_term_meta( $cat->term_id, 'thumbnail_id', true );
    $image_url = wp_get_attachment_url( $thumbnail_id );
}

if ( is_singular('service') ){
    $image_url = get_field("single_banner", "options");
}

if ( !empty($image_url) && !is_singular( 'post' ) && !is_product()  && !is_search() && !is_shop() ): ?>

<div class="hero-section">
	<div class="header-image cover" style="background-image: url(<?= $image_url; ?>);">
		<div class="container">
			<div class="header-image-content">
				<h1 class="h1"><?= $title; ?></h1>
			</div>
		</div>
	</div>
</div>

<?php endif; ?>

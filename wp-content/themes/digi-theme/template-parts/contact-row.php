<?php
/* 
template for contact us section with background image and text over it
right above the footer
*/
if (is_shop() ||  is_product_category()) {
	if( get_field('show_contact_section','2831') ) {
		?>
		
		<section class="above-footer-contact">
			<div class="full-width-bg"
				style="background-image: url(<?php the_field("contact_section_background", "option"); ?>);">
				<div class="container">
					<div class="full-width-dark-content">
						<h3 class="h3">Contact Us</h3>
						<p class="lighter-grey"><?php the_field("contact_section_about_text", "option"); ?></p>
						<a class="slider-button-link" href="tel:<?php the_field('phone_number', 'options'); ?>">
							<div class="slider-button">
								<i class="fas fa-phone-alt"></i>
								<span>Call now<br><?php the_field('phone_number', 'options'); ?></span>
							</div>
						</a>
					</div>
				</div>
			</div>
		</section>
		<?php
		}
}else{
	if( get_field('show_contact_section') ) {
		?>
		
		<section class="above-footer-contact">
			<div class="full-width-bg"
				style="background-image: url(<?php the_field("contact_section_background", "option"); ?>);">
				<div class="container">
					<div class="full-width-dark-content">
						<h3 class="h3">Contact Us</h3>
						<p class="lighter-grey"><?php the_field("contact_section_about_text", "option"); ?></p>
						<a class="slider-button-link" href="tel:<?php the_field('phone_number', 'options'); ?>">
							<div class="slider-button">
								<i class="fas fa-phone-alt"></i>
								<span>Call now<br><?php the_field('phone_number', 'options'); ?></span>
							</div>
						</a>
					</div>
				</div>
			</div>
		</section>
		<?php
		}
}

?>
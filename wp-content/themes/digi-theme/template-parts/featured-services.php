<?php
/*
template for services
*/
?>

<div class="col-lg-3 col-md-4 col-sm-6 col-6">
    <div class="featured-services-box">
        <div class="service-thumb">
        <a class="featured-services-image" href="<?php the_permalink(); ?>"><img src="<?php the_post_thumbnail_url(); ?>" class="img-fluid" alt=""></a>
        </div>
        <h3 class="h6"><a href="<?php the_permalink(); ?>"><?php the_title() ?></a></h3>
    </div>
</div>


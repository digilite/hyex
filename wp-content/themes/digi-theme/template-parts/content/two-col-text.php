<?php
$two_col_title = get_sub_field("two_col_title");
?>

<section class="builder-two-col-text">
<div class="container">
    <?php
    if ( $two_col_title ) : ?>
        <h3 class="h2"><?= $two_col_title ?></h3>
    <?php endif; ?>
    <div class="row">
        <div class="col-md-6">
            <p><?php the_sub_field("left_text_area"); ?></p>
        </div>
        <div class="col-md-6">
            <p><?php the_sub_field("right_text_area"); ?></p>
        </div>
    </div>
</div>
</section>
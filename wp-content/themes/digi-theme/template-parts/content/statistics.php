<?php
/*
Statistics section with orange background
*/
?>

<section class="statistics-section">
    <?php if( have_rows('statistics') ): ?>
    <div class="container">
        <div class="stats-full">
        <?php while( have_rows('statistics') ): the_row();
        $stats_title = get_sub_field("statistics_title");
        $stats_num = get_sub_field("statistics_number");
        $stats_symbol = get_sub_field("statistics_symbol");
        ?>
            <div class="stats-box">
                <div class="num-sym">
                  <span class="stats-num"><?= $stats_num; ?></span>
                  <span class=stats-operator><?= $stats_symbol ?></span>
                </div>
                <span class="stats-title"><?= $stats_title; ?></span>
            </div>
            <?php endwhile; ?>
        </div>
        <!--row-->
    </div>
    <!--container-->
    <?php endif; ?>
</section>

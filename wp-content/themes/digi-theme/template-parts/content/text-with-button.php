<?php
/**
* text with button in the middle of the text
*/
$btn_left_text = get_sub_field("button_left_text");
$btn_right_text = get_sub_field("button_right_text");
$btn_link = get_sub_field("button_link");
?>
<section class="contact-invoice-section">
    <div class="container">
        <span><?= $btn_left_text ?>
        <?php if(is_page( 'Hygiene Services' )){ ?> 
            <a><button type="button" class="btn btn-light"  data-toggle="modal" data-target="#formModal" >Send</button></a> 
            <?php } else {?>
                <a target="_blank" href="<?= $btn_link ?>"><button type="button" class="btn btn-light" >Send</button></a> 
        <?php } echo $btn_right_text ?></span>
    </div>
</section>

<!-- Modal -->
<div class="modal fade" id="formModal" tabindex="-1" aria-labelledby="formModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <?php echo do_shortcode( '[formidable id=4]' ); ?>
      </div>
     
    </div>
  </div>
</div>
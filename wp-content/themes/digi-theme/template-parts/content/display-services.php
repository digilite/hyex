<?php
/*
Display Services
*/
$services_title = get_sub_field("services_title");
?>

<section class="featured-services">
<div class="container">
<h2 class="h2 mb-4"><?= $services_title ?></h2>
<?php
$featured_services = get_sub_field("select_services");
if( $featured_services ): ?>
    <div class="row">
    <?php foreach( $featured_services as $post ):

            setup_postdata($post); ?>
            <?php get_template_part( "template-parts/featured", "services" ); ?>

            <?php endforeach; ?>
        </div>
        <?php
        wp_reset_postdata(); ?>
        <?php endif; ?>
        <div class="d-flex justify-content-center">
            <a class="view-all-link" href="<?= get_page_link(19939) ?>">View all</a>
        </div>
    </div>
</section>


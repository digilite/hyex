<?php 
// List of popular Products
$products_sec_title = get_sub_field("title_pro_section");
$products_view_all = get_sub_field("view_all_link");
$products_id = get_sub_field("select_products");

if ( $products_id ): 
?>

<section class="popular-products">
    <div class="container">
        <?php
        if ( $products_sec_title ) {
            echo '<h2 class="h2">' . $products_sec_title . '</h2>';
        }

        $format_ids = implode( ",",$products_id );
        echo do_shortcode("[products ids=". $format_ids ." limit='12']");
        ?>
        <div class="d-flex justify-content-center">
            <a class="view-all-link" href="<?= $products_view_all ?>">View all</a>
        </div>
    </div>
</section>

<?php endif; ?>
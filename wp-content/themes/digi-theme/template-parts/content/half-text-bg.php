<?php
/* 
template for half text, half background
*/
$order = get_sub_field("order");
$choose_theme = get_sub_field("choose_theme");
$light_title = get_sub_field("the_title");
$light_desc = get_sub_field("the_description");
$light_btn_text = get_sub_field("the_button_text");
$light_btn_link = get_sub_field("the_button_link");
$light_background = get_sub_field("the_background");

if ( $light_background ) :
?>

<section class="full-width-cta <?= $order; ?>">
	<div class="half-content <?= $choose_theme; ?>">
		<div class="text-col">
			<?php
			if ( $light_title ) {
				echo '<h3 class="h3">' . $light_title . '</h3>';
			}
			if ( $light_desc ) {
				echo '<p class="hf-paragraph">' . $light_desc . '</p>';
			}
			if ( $light_btn_text ) :
			?>
			<a class="relative-parent" target="_blank" href="<?= $light_btn_link; ?>"><?= $light_btn_text; ?> <span class="material-icons">chevron_right</span></a>
			<?php endif; ?>
		</div>
	</div>
	<div class="full-width-bg-cta" style="background-image: url(<?= $light_background["sizes"]["large"] ?>);"></div>

</section>

<?php endif; ?>

<?php
/* 
template feauterd products
*/
$products_id = get_sub_field("feauterd_products");
if ( $products_id ):
$product = wc_get_product($products_id );
?>
<section class="featured-services">
    <div class="container">
    <h3 class="h2 mb-4">Feauterd Products</h3>
        <div class="row">
            <div class="img-feauterd col-lg-3 col-sm-12">
            <?php 
            echo $product->get_image( );?>
                    
            </div>
            <div class="cont-feauterd col-lg-9 col-sm-12">
                <h3 class="title-feauterd">
                    <a href="<?php echo get_permalink( $id); ?>"><?php  echo $product->get_title(); ?></a>
                    </h3>
                <div class="dec-feauterd">
                <p> <?php  echo get_the_excerpt( $id ); ?></p>
                <a href="<?php  echo get_permalink( $id); ?>">Learn more <img src="<?php bloginfo( 'template_url' ); ?>/img/arrow-right.svg"/></a>
                </div>
            <div>
        </div>
    </div>
</section>
<?php  endif; ?>
<?php 
// 4 column row displaying why chose hyex section
$main_why_title = get_sub_field("main_why_title");
$main_why_description = get_sub_field("main_why_description");
?>
<section>
    <div class="container">
        <div class="why-choose-intro">
            <?php
    if ( $main_why_title ) {
        echo '<h2 class="h2">' . $main_why_title . '</h2>';
    }
    if ( $main_why_description ) {
        echo '<p>' . $main_why_description . '</p>';
    }
    ?>
        </div>
        <div class="flex-block">
            <?php if( have_rows('why_to_choose_hyex') ): ?>
            <?php while( have_rows('why_to_choose_hyex') ): the_row(); 
            $single_title = get_sub_field("single_box_title");
            $single_description = get_sub_field("single_box_description"); 
            ?>
            <div class="why-col">
                <p class="h5"><?php echo $single_title; ?></p>
                <p class="why-choose-text"><?php echo $single_description; ?></p>
            </div>
            <?php endwhile; ?>

            <?php endif; ?>
        </div>
    </div>
    <!--container-->
</section>
<?php
/* 
template for full background with text
*/
$full_title = get_sub_field("full_title");
$full_short_desc = get_sub_field("full_short_description");
$full_desc = get_sub_field("full_description");
$full_btn_text = get_sub_field("full_button_text");
$full_btn_link = get_sub_field("full_button_link");
$full_background = get_sub_field("full_background");


if ( $full_background ) :
?>

<section>
	<div class="full-width-bg" style="background-image: url(<?= $full_background["sizes"]["hero-thumb"] ?>);">
		<div class="container">
			<div class="full-width-dark-content">
			<?php
				if ( $full_title ) {
					echo '<h3 class="h3">' . $full_title . '</h3>';
				}
				if ( $full_short_desc ) {
					echo '<p class="white-bold">' . $full_short_desc . '</p>';
				}
				if ( $full_desc ) {
					echo '<p class="lighter-grey">' . $full_desc . '</p>';
				}
				?>
				<?php 
				if ($full_btn_text) :
				?>
				<a class="two-col-row-link relative-parent" href="<?= $full_btn_link; ?>"><?= $full_btn_text; ?> <span class="material-icons">chevron_right</span></a>
				<?php endif; ?>
			</div>
		</div>
	</div>
</section>

<?php endif; ?>
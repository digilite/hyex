<?php
/*
Template Name: singup
*/
get_header();
?>
<div class="container">
    <form  id="register-form" action="" method="post">
        <div class="form-row">
            <div class="form-group col-md-6">
                <input type="text" name="personf-name" id="personf-name"  class="person-info-name form-control req" placeholder="First Name" required >
            </div>
            <div class="form-group col-md-6">
                <input type="text" name="personl-lname" id="personl-lname" class=" person-info-name form-control req" placeholder="Last Name" required >
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-6">
                <input type="text" name="comp-name" id="comp-name"class="form-control req" placeholder="Company name" required >
            </div>
            <div class="form-group col-md-6">
                <input type="text" name="email" id="email" class="form-control" pattern="[^ @]*@[^ @]*" placeholder="Email" value="" required>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-6">
                <input type="password" name="password" id="password" class="form-control" placeholder="Password" size="8" required>
            </div>
            <div class="form-group col-md-6">
                <input type="password" name="confirm-password" id="confirm-password" class="form-control" placeholder="Confirm Password" required>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-6">
                <input type="text" name="add1" id="add1"class="form-control req" placeholder="Address Line 1" required >
            </div>
            <div class="form-group col-md-6">
                <input type="text" name="add2" id="add2" class="form-control" placeholder="Address Line 2" value="" required>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-6">
                <select  name="country" id="city"  class="form-control"  required >
                    <option value="">city</option>
                    <option value="Canada">Canada</option>
                    <option value="United States(US)">United States(US)</option>
                </select>
            </div>
            <div class="form-group col-md-6">
                <select  name="country" id="country"  class="form-control"  required >
                    <option value="">Country</option>
                    <option value="Canada">Canada</option>
                    <option value="United States(US)">United States(US)</option>
                </select>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-6">
                <input type="text" name="province" id="province"class="form-control req" placeholder="State/Province*" required >
            </div>
            <div class="form-group col-md-6">
                <input type="text" name="zip" id="zip" class="form-control" placeholder="Zip/Postcode*" value="" required>
            </div>
        </div>
        <div class="error_block_reg_mes"></div>

        <div class="form-group">
            <button type="submit" name="register-submit" id="register-submit" class=" register-btn">Create account<span class="iconify" data-icon="bx:bx-right-arrow-alt" data-inline="false"></span></button>
        </div>
        <p>
            <b>Please Note</b>: In order to activate your account please send a copy of your business license by email to: info@autoremotes.ca
        </p>
    </form>
</div>

<?php get_footer(); ?>


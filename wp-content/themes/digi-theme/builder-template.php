<?php 
/* Template Name: Builder Template */
get_header();
?>

<main class="main-content">
    <?php get_template_part("content"); ?>
</main>

<?php get_footer(); ?>
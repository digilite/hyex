(function($) {

    $('#register-form').on('submit', function(e) {
        e.preventDefault();
        var status ='';
        var receive_status ='';
        var valid = '';
        var pass1 = $('#password').val();
        var pass2 = $('#confirm-password').val();
        var mail = $('#email').val();
        var fname=  $('#personf-name').val();
        var lname=  $('#personl-lname').val();
        var comp_name=  $('#comp-name').val();
        var province=  $('#province').val();
        var zip=  $('#zip').val();
        var add1=  $('#add1').val();
        var add2=  $('#add2').val();
        var country= $("#country :selected").val();
        var city= $("#city :selected").val();



        $('.error_block_reg').find('.error').remove();
        $('.error_block_reg_mes').find('.error').remove();
        $.ajax({
            url: login_ajax_object.ajax_url,
            type: 'POST',
            data: {
                action: 'check_username',
                mail: mail
            },

            success: function(response) {
                $('.error_block_reg_mes').append('<div class="error">' + response + '</div>');
                valid = 'no';
            }
        });
        if ($('#dealer').is(':checked')) {
            status = 'unverified_dealer_partner_role';
        }else{
            status = 'unverified_partner_role';
        }
        if ($('#receive_status').is(':checked')) {
            receive_status = 'yes';
        }else{
            receive_status = 'no';
        }

        if($('#password').val().length >8){
            $('.error_block_reg').append(
                '<div class="error">The password  must be 8  character </div>'
            );
            valid = 'no';
        }
        if (pass1 === pass2) {

        } else {
            $('.error_block_reg').append(
                '<div class="error">This field is Not matching</div>'
            );
            valid = 'no';
        }
        if (!$('.remember_me').is(':checked')) {
            checked = 'false';
        } else {
            checked = 'true';
        }

        if (!valid) {
            $.ajax({
                type: 'POST',
                url: login_ajax_object.ajax_url,
                data: {
                    action: 'new_user',
                    mail: mail,
                    pass: pass1,
                    checked: checked,
                    status:status,
                    country:country,
                    fname:fname,
                    lname:lname,
                    comp_name:comp_name,
                    receive_status:receive_status,
                    city:city,
                    add1:add1,
                    add2:add2,
                    zip:zip,
                    province:province
                },
                success: function(msg) {
                     $('.error_block_reg').find('.error').remove();
                     $('.error_block_reg_mes').find('.error').remove();
                    console.log(msg);
                    $('#email').val('');
                    $('#date').val('');
                    $('#password').val('');
                    $('#confirm-password').val('');
                    $('#licence_id').val('');
                    $('#programming_tools').val('');
                    $('#personf-name').val('');
                    $('#personl-lname').val('');
                     $('#comp-name').val('');
                     $('#comment').val('');
                    $(".dealer-info-inputs").find('.req').attr("required", "false");
                    $(".dealer-info-inputs").css("display", "none");
                    $("#defaultUnchecked").prop("checked", false);
                    $("#dealer").prop("checked", false);
                    setTimeout(getprofile, 1000);
                }
            });


        }
    });
    function getprofile() {
        var url='';

        url = window.location + '/my-account';

        $(location).attr('href', url);
    }


    $('.stock').on('click', function(e) {
        console.log($(this));
        $(this).css("background-color", "#B4243D");
        $(this).css("color", "#fff");
        $('.new').css("background-color", "#EEEEEE");
        $('.new').css("color", "#1C1C1C");
        $("#pa_condition").find("[value='old']").attr('selected', 'selected').trigger('change');
    });
    $('.new').on('click', function(e) {
        console.log($(this));
        $(this).css("background-color", "#B4243D");
        $(this).css("color", "#fff");
        $('.stock').css("background-color", "#EEEEEE");
        $('.stock').css("color", "#1C1C1C");
        $("#pa_condition").find("[value='new']").attr('selected', 'selected').trigger('change');
    });
    $(".register-form-block").css("display", "none");
    $(".login-block").css("display", "none");
    $('.slide-login').on('click', function(e) {
        $(".login-block").css("display", "none");
        $(".reg-block").css("display", "block");
        $(".login-form-block").css("display", "block");
        $(".register-form-block").css("display", "none");
    });
    $('.slide-reg').on('click', function(e) {
        $(".reg-block").css("display", "none");
        $(".login-block").css("display", "block");
        $(".login-form-block").css("display", "none");
        $(".register-form-block").css("display", "block");
    });

    $( ".cat-parent" ).addClass("active-m");
    $( ".cat-parent" ).click(function() {
        $(this).toggleClass("display-cats");
        $(this).find(".children").toggle( "slow", function() {

        });
    });

    var url = window.location.href;
    var active = url.split('#');
    var activeTab = active.pop() || active.pop();
    if(active !=''){
      $(".tab-pane").removeClass("active show");
      $("#" + activeTab).addClass("active show");
      $(".nav-link").removeClass("active");
      $('a[href="#'+ activeTab +'"]').addClass('active')
    }



    $('.nav-link').click(function(event) {

        event.preventDefault();

        url_l = url.split('#');
        var lastSegment = url_l.pop() || url_l.pop();
        console.log(lastSegment);
        var attr = $(this).attr('aria-controls');
        document.location.assign( url+"#"+attr);

        });


})(jQuery);

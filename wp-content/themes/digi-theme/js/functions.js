var $ = jQuery;
$(document).ready(function () {
    $(".owl-carousel").owlCarousel({
        animateOut: 'fadeOut',
        items:1,
        loop:true,
        dots:true,
        autoplay:true,
        autoplaySpeed:1000,
        smartSpeed:1500,
    });
});

var slider_section = $('.slider-section');
var testimonials_slider = $(".testimonials-slider");
var service_slider = $(".service-slider");

$(document).ready(function() {
    $("#burger-icon").click(function(){
        $(this).toggleClass("open");
        $("#site-nav").addClass("opend");
    });

	if(testimonials_slider.length) {
		testimonials_slider.owlCarousel(single_slide_bullets_fade);
	}
	if(service_slider.length) {
		service_slider.owlCarousel(full_gutter_24);
    }
    $(".user-content iframe").each(function(){
        $(this).wrap("<div class='embed-responsive embed-responsive-16by9'></div>")
    });
    mobileMenu();

    $(".move-down").click(function(){
        var hero_height = $(this).parent().parent().height();
        var to_scroll = hero_height + $("header").height() + 34;
        $("html, body").animate({ scrollTop:  to_scroll }, 2000);
    });
    if ($(".grid").length) {
        $(".grid").packery({
            // options
            itemSelector: ".grid-item",
        });
    }
});

function mobileMenu() {
    if ($(window).width() < 992 ) {
        $(".menu-item-has-children > a").click(function(e){
            e.preventDefault();
            $(this).toggleClass("clicked");
            $(this).parent().find(".sub-menu").slideToggle();
        });
    }
}

$(document).ready(function() {
    $(".fa-search").click(function() {
       $(".searchform").toggle();
       $(".topbar-search").focus();
     });
});

$(document).ready(function() {
    if ($(window).width() < 992 ) {
        $(".menu-item-has-children > *").click(function() {
            $(this).toggleClass("activated");
        });
    }
});

$(document).ready(function() {
    if ($(window).width() < 992 ) {
        $(".topbar i").click(function() {
            $(this).toggleClass("colored");
        });
    }
});

$(document).ready(function() {
    $(".navbar-toggler").click(function() {
        $(this).toggleClass("change");
     });
});

if ($(window).width() > 992 ) {
$('.stats-num').counterUp({
    delay: 10,
    time: 1000
});
}

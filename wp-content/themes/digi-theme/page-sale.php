<?php
/**
* Template Name: Sale
*/
?>

<?php get_header(); ?>

<div class="container">
    <?php the_breadcrumb(); ?>
    <section>
        <?php echo do_shortcode('[products columns="4" orderby="popularity" on_sale="true" ]'); ?>
    </section>
</div>
<?php get_template_part("content"); ?>

<?php get_footer(); ?>
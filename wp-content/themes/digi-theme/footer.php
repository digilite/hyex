		<?php get_template_part( "template-parts/contact", "row" ); ?>
		<footer itemscope itemtype="http://schema.org/WPFooter">
			<div class="container">
			<div class="footer-content">
				<div class="row">
					<div class="col-md-3 col-sm-6 col-12 change-order">
						<div class="footer-contact-info">
							<a href="<?php bloginfo('url'); ?>"><img src="<?php echo get_theme_file_uri('/img/Hyex-logo.svg') ?>" alt="logo"></a>
							<p class="offers-footer"><?php the_field('hyex_offers', 'options'); ?></p>
							<span><span class="material-icons mr-2">phone</span><a class="phone-link" href="tel:<?php the_field('phone_number', 'options'); ?>"><?php the_field('phone_number', 'options'); ?></a></span>
							<span><span class="material-icons mr-2">email</span><a class="phone-link" href="mailto:<?php the_field('email', 'options'); ?>"><?php the_field('email', 'options'); ?></a></span>

							<div class="footer-address d-flex">
							<span class="material-icons mr-2">location_on</span>
								<p><?php the_field('address', 'options'); ?></p>
							</div>
						</div>
					</div>

					<div class="col-md-3 col-sm-6 col-6 footer-services">
						<p class="h3">Services</p>
						<?php
						$footerServices = new WP_Query(array(
							'post_type' => 'service'
						));?>

						<ul class="footer-lists">
						<?php while ($footerServices->have_posts()) {
							$footerServices->the_post(); ?>
							<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
							<?php } wp_reset_postdata();?>
						</ul>
					</div>

					<div class="col-md-3 col-sm-6 col-6 footer-product-cats">
						<p class="h3">Products</p>
						<?php
						$args = array(
									'taxonomy' => 'product_cat',
									'orderby' => 'name',
									'order'   => 'ASC',
									'parent' => 0
								);

						$cats = get_categories($args); ?>

						<ul class="footer-lists">
						<?php
						foreach($cats as $cat) {
						?>
							<li><a href="<?php echo get_category_link( $cat->term_id ) ?>">
								<?php echo $cat->name; ?>
							</a></li>
						<?php
						}
						?>
						</ul>
					</div>

					<div class="col-md-3 col-sm-6 col-12">

						<div class="footer-newsletter">
							<p class="h3">Newsletter</p>
							<?php echo FrmFormsController::get_form_shortcode( array( 'id' => 3, 'title' => false, 'description' => false ) ); ?>
							<div class="social-links">
								<?php get_template_part( 'social' ); ?>
							</div>
						</div>
					</div>

				</div><!-- row -->
			</div>
			</div><!-- container-->
				<div class="footer-copyright text-center">
					&copy; <?php echo date("Y") . "  " . get_bloginfo("name"); ?>. All rights reserved.
				</div>

		</footer>
		<?php wp_footer(); ?>
	</body>
</html>

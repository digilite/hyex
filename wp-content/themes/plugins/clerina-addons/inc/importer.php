<?php
/**
 * Hooks for importer
 *
 * @package Clerina
 */


/**
 * Importer the demo content
 *
 * @since  1.0
 *
 */
function clerina_vc_addons_importer() {
	return array(
		array(
			'name'       => 'Homepage 1',
			'preview'    => 'https://demo.grixbase.com/soo-importer/clerina/preview.jpg',
			'content'    => 'https://demo.grixbase.com/soo-importer/clerina/demo-content.xml',
			'customizer' => 'https://demo.grixbase.com/soo-importer/clerina/customizer.dat',
			'widgets'    => 'https://demo.grixbase.com/soo-importer/clerina/widgets.wie',
			'sliders'    => 'https://demo.grixbase.com/soo-importer/clerina/sliders.zip',
			'pages'      => array(
				'front_page' => 'HomePage 1',
				'blog'       => 'Blog',
				'shop'       => 'Shop',
				'cart'       => 'Cart',
				'checkout'   => 'Checkout',
				'my_account' => 'My account',
				'portfolio'  => 'Project',
				'service'    => 'Services'
			),
			'menus'      => array(
				'primary' => 'primary',
			),
			'options'    => array(
				'shop_catalog_image_size'   => array(
					'width'  => 480,
					'height' => 480,
					'crop'   => 1,
				),
				'shop_single_image_size'    => array(
					'width'  => 600,
					'height' => 600,
					'crop'   => 1,
				),
				'shop_thumbnail_image_size' => array(
					'width'  => 70,
					'height' => 70,
					'crop'   => 1,
				),
			),
		),
		array(
			'name'       => 'Homepage 2',
			'preview'    => 'https://demo.grixbase.com/soo-importer/clerina/homepage-2/preview.jpg',
			'content'    => 'https://demo.grixbase.com/soo-importer/clerina/demo-content.xml',
			'customizer' => 'https://demo.grixbase.com/soo-importer/clerina/homepage-2/customizer.dat',
			'widgets'    => 'https://demo.grixbase.com/soo-importer/clerina/homepage-2/widgets.wie',
			'sliders'    => 'https://demo.grixbase.com/soo-importer/clerina/homepage-2/sliders.zip',
			'pages'      => array(
				'front_page' => 'HomePage 2',
				'blog'       => 'Blog',
				'shop'       => 'Shop',
				'cart'       => 'Cart',
				'checkout'   => 'Checkout',
				'my_account' => 'My account',
				'portfolio'  => 'Project',
				'service'    => 'Services'
			),
			'menus'      => array(
				'primary' => 'primary',
			),
			'options'    => array(
				'shop_catalog_image_size'   => array(
					'width'  => 480,
					'height' => 480,
					'crop'   => 1,
				),
				'shop_single_image_size'    => array(
					'width'  => 600,
					'height' => 600,
					'crop'   => 1,
				),
				'shop_thumbnail_image_size' => array(
					'width'  => 70,
					'height' => 70,
					'crop'   => 1,
				),
			),
		),
		array(
			'name'       => 'Homepage 3',
			'preview'    => 'https://demo.grixbase.com/soo-importer/clerina/homepage-3/preview.jpg',
			'content'    => 'https://demo.grixbase.com/soo-importer/clerina/demo-content.xml',
			'customizer' => 'https://demo.grixbase.com/soo-importer/clerina/homepage-3/customizer.dat',
			'widgets'    => 'https://demo.grixbase.com/soo-importer/clerina/homepage-3/widgets.wie',
			'sliders'    => 'https://demo.grixbase.com/soo-importer/clerina/homepage-3/sliders.zip',
			'pages'      => array(
				'front_page' => 'HomePage 3',
				'blog'       => 'Blog',
				'shop'       => 'Shop',
				'cart'       => 'Cart',
				'checkout'   => 'Checkout',
				'my_account' => 'My account',
				'portfolio'  => 'Project',
				'service'    => 'Services'
			),
			'menus'      => array(
				'primary' => 'primary',
			),
			'options'    => array(
				'shop_catalog_image_size'   => array(
					'width'  => 480,
					'height' => 480,
					'crop'   => 1,
				),
				'shop_single_image_size'    => array(
					'width'  => 600,
					'height' => 600,
					'crop'   => 1,
				),
				'shop_thumbnail_image_size' => array(
					'width'  => 70,
					'height' => 70,
					'crop'   => 1,
				),
			),
		),
	);
}

add_filter( 'soo_demo_packages', 'clerina_vc_addons_importer', 20 );

/**
 * Update more page options
 *
 * @param $pages
 */
function clerina_addons_import_portfolio( $pages ) {

	if ( isset( $pages['portfolio'] ) ) {
		$portfolio = get_page_by_title( $pages['portfolio'] );

		if ( $portfolio ) {
			update_option( 'clerina_portfolio_page_id', $portfolio->ID );
		}
	}
}

add_action( 'soodi_after_setup_pages', 'clerina_addons_import_portfolio' );

/**
 * Update more page options
 *
 * @param $pages
 */
function clerina_addons_import_services( $pages ) {

	if ( isset( $pages['service'] ) ) {
		$services = get_page_by_title( $pages['service'] );

		if ( $services ) {
			update_option( 'clerina_service_page_id', $services->ID );
		}
	}
}

add_action( 'soodi_after_setup_pages', 'clerina_addons_import_services' );
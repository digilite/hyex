(function ($) {
    'use strict';

    var clerina = clerina || {};

    clerina.init = function () {
        clerina.$body = $(document.body),
            clerina.$window = $(window),
            clerina.$header = $('#masthead');

        // Team carousel
        this.teamCarousel();

        // Image carousel
        this.imageCarousel();
        this.serviceCarousel();
        this.portfolioCarousel();
        this.portfolioSingleImage();
        this.imageBoxCarousel();
        this.videoLightBox();

        //Counter
        this.counter();

        //Coming Soon
        this.eventCountDown();

        //maps
        this.gmaps();

        //testimonial
        this.testimonialCarousel();

    };

    clerina.teamCarousel = function () {
        $.each(clerinaShortCode.teamCarousel, function (id, imagesData) {
            var $sliders = $(document.getElementById(id));
            $sliders.not('.slick-initialized').slick({
                slidesToShow  : imagesData.slide,
                slidesToScroll: 1,
                infinite      : true,
                arrows        : imagesData.nav,
                autoplay      : imagesData.autoplay,
                autoplaySpeed : imagesData.speed,
                dots          : false,
                prevArrow     : '<div class="clerina-left-arrow"><span class="dl-icon-next svg-icon"><i class="fa fa-angle-left"></i></span></div>',
                nextArrow     : '<div class="clerina-right-arrow"><span class="dl-icon-next svg-icon"><i class="fa fa-angle-right"></i></span></div>',
                //appendArrows  : $container,
                responsive    : [
                    {
                        breakpoint: 991,
                        settings  : {
                            arrows: false,
                            dots  : true,
                            slidesToShow: 3
                        }
                    },
                    {
                        breakpoint: 768,
                        settings  : {
                            arrows: false,
                            dots  : true,
                            slidesToShow: 2
                        }
                    },
                    {
                        breakpoint: 480,
                        settings  : {
                            arrows: false,
                            dots  : true,
                            slidesToShow: 1
                        }
                    }
                ]
            });
        });
    };

    clerina.imageCarousel = function () {
        $.each(clerinaShortCode.imageCarousel, function (id, imagesData) {
            var $sliders = $(document.getElementById(id));
            $sliders.not('.slick-initialized').slick({
                infinite     : true,
                arrows       : imagesData.nav,
                autoplay     : imagesData.autoplay,
                autoplaySpeed: imagesData.speed,
                dots         : imagesData.dot,
                cssEase      : 'ease-out',
                rows         : imagesData.row,
                slidesToShow : imagesData.perrow,
                responsive   : [
                    {
                        breakpoint: 991,
                        settings  : {
                            arrows: false,
                            dots  : true
                        }
                    },
                    {
                        breakpoint: 768,
                        settings  : {
                            arrows: false,
                            dots  : true,
                            rows  : 1,
                            slidesToShow: 3,
                        }
                    },
                    {
                        breakpoint: 480,
                        settings  : {
                            arrows: false,
                            dots  : true,
                            rows  : 1,
                            slidesToShow: 3
                        }
                    },
                ]
            });
        });
    };
    clerina.portfolioSingleImage = function () {
        $.each(clerinaShortCode.imageSingle, function (id, styleData) {
            var $imageSingle = $(document.getElementById(id)),
                $image = $imageSingle.find('img');
            console.log(styleData.style.toString());
            $image.css('border-radius', styleData.style.toString());
        });
    };

    clerina.serviceCarousel = function () {
        $.each(clerinaShortCode.serviceCarousel, function (id, imagesData) {
            var $sliders = $(document.getElementById(id));
            $sliders.not('.slick-initialized').slick({
                infinite      : true,
                arrows        : false,
                autoplay      : imagesData.autoplay,
                autoplaySpeed : imagesData.speed,
                dots          : imagesData.dot,
                rows          : imagesData.row,
                slidesToShow  : imagesData.slide,
                slidesToScroll: imagesData.scroll,
                responsive: [
                    {
                        breakpoint: 991,
                        settings  : {
                            arrows: false,
                            dots  : true,
                            slidesToShow: 2
                        }
                    },
                    {
                        breakpoint: 768,
                        settings  : {
                            arrows: false,
                            dots  : true,
                            slidesToShow: 2
                        }
                    },
                    {
                        breakpoint: 480,
                        settings  : {
                            arrows: false,
                            dots  : true,
                            slidesToShow: 1
                        }
                    }
                ]
            });
        });
    };
    /**
     * Init portfolio carousel
     */
    clerina.portfolioCarousel = function () {

        $.each(clerinaShortCode.portfolio, function (id, portfolioData) {
            var $portfolio = $(document.getElementById(id)),
                $filter = $portfolio.closest('.mf-portfolio-carousel').find('ul.nav-filter');

            $portfolio.not('.slick-initialized').slick({
                rtl          : clerinaShortCode.isRTL === "1",
                slidesToShow : 4,
                infinite     : portfolioData.autoplay,
                arrows       : portfolioData.nav,
                autoplay     : portfolioData.autoplay,
                autoplaySpeed: portfolioData.autoplay_speed,
                dots         : portfolioData.dot,
                responsive   : [
                    {
                        breakpoint: 1200,
                        settings  : {
                            slidesToShow: 3,
                            arrows      : false,
                            dots        : false
                        }
                    },
                    {
                        breakpoint: 768,
                        settings  : {
                            slidesToShow: 2,
                            arrows      : false,
                            dots        : false
                        }
                    },
                    {
                        breakpoint: 480,
                        settings  : {
                            slidesToShow: 1,
                            arrows      : false,
                            dots        : false
                        }
                    }
                ]
            });

            $filter.on('click', 'a', function (e) {
                e.preventDefault();
                $filter.find('a').removeClass('active');
                $(this).addClass('active');
                var value = $(this).data('filter');
                $portfolio.slick('slickUnfilter');

                if (value != '*') {
                    $portfolio.slick('slickFilter', value);
                }
            });
        });
    };


    clerina.counter = function () {

        var $counter = $(".counter-value .counter-number");
        if ($counter.length < 1) {
            return;
        }

        var duration = parseFloat($counter.data('duration'));
        duration = duration * 1000;
        $counter.counterUp({
            delay: 10,
            time : duration,
        });

    };


    /**
     * Event CountDown
     */
    clerina.eventCountDown = function () {


        if ($('.cl-time-format').length === 0) {
            return;
        }

        $('.cl-time-format').each(function () {
            var $eventDate = $(this);

            var diff = $(this).find('.cl-time-countdown').html();

            $eventDate.find('.cl-time-countdown').FlipClock(diff, {
                clockFace: 'DailyCounter',
                countdown: true,
                labels   : [clerinaShortCode.days, clerinaShortCode.hours, clerinaShortCode.minutes, clerinaShortCode.seconds]
            });
        });
    };

    clerina.imageBoxCarousel = function () {
        $.each(clerinaShortCode.imageBoxCarousel, function (id, imagesBoxData) {
            var $sliders = $(document.getElementById(id));
            $sliders.not('.slick-initialized').slick({
                slidesToScroll: imagesBoxData.slide,
                slidesToShow  : imagesBoxData.slide,
                infinite      : imagesBoxData.autoplay,
                arrows        : imagesBoxData.nav,
                autoplay      : imagesBoxData.autoplay,
                autoplaySpeed : imagesBoxData.autoplay_speed,
                dots          : imagesBoxData.dot,
                prevArrow     : '<div class="clerina-left-arrow"><span class="dl-icon-next svg-icon"><i class="fa fa-angle-left"></i></span></div>',
                nextArrow     : '<div class="clerina-right-arrow"><span class="dl-icon-next svg-icon"><i class="fa fa-angle-right"></i></span></div>',
                responsive    : [
                    {
                        breakpoint: 991,
                        settings  : {
                            arrows: false,
                            dots  : true,
                            slidesToShow: 2
                        }
                    },
                    {
                        breakpoint: 768,
                        settings  : {
                            arrows: false,
                            dots  : true,
                            slidesToShow: 2
                        }
                    },
                    {
                        breakpoint: 480,
                        settings  : {
                            arrows: false,
                            dots  : true,
                            slidesToShow: 1
                        }
                    }
                ]
            });
        });
    };

    /*
     * Toggle video banner play button
     */
    clerina.videoLightBox = function () {

        var web_width = $(window).width(),
            video_setWidth = web_width/2;


        $('.clerina-video-full').css('width',video_setWidth);

        var $images = $('.clerina-video-banner');

        if (!$images.length) {
            return;
        }

        var $links = $images.find('a.photoswipe'),
            items = [];

        $links.each(function () {
            var $a = $(this);

            items.push({
                html: $a.data('href')
            });

        });

        $images.on('click', 'a.photoswipe', function (e) {
            e.preventDefault();

            var index = $links.index($(this)),
                options = {
                    index              : index,
                    bgOpacity          : 0.85,
                    showHideOpacity    : true,
                    mainClass          : 'pswp--minimal-dark',
                    barsSize           : {top: 0, bottom: 0},
                    captionEl          : false,
                    fullscreenEl       : false,
                    shareEl            : false,
                    tapToClose         : true,
                    tapToToggleControls: false
                };

            var lightBox = new PhotoSwipe(document.getElementById('pswp'), window.PhotoSwipeUI_Default, items, options);
            lightBox.init();

            lightBox.listen('close', function () {
                $('.clerina-video-wrapper').find('iframe').each(function () {
                    $(this).attr('src', $(this).attr('src'));
                });
            });
        });
    };

    /**
     * Init Google maps
     */
    clerina.gmaps = function () {

        if (clerinaShortCode.length === 0 || typeof clerinaShortCode.map === 'undefined') {
            return;
        }

        var mapOptions = {
                scrollwheel       : false,
                draggable         : true,
                zoom              : 10,
                mapTypeId         : google.maps.MapTypeId.ROADMAP,
                panControl        : false,
                zoomControl       : true,
                zoomControlOptions: {
                    style: google.maps.ZoomControlStyle.SMALL
                },
                scaleControl      : false,
                streetViewControl : false

            },
            customMap;

        var bounds = new google.maps.LatLngBounds();
        var infoWindow = new google.maps.InfoWindow();

        var style1 = [
            {
                "featureType": "administrative",
                "elementType": "all",
                "stylers"    : [
                    {
                        "visibility": "on"
                    },
                    {
                        "lightness": 33
                    }
                ]
            },
            {
                "featureType": "landscape",
                "elementType": "all",
                "stylers"    : [
                    {
                        "color": "#f7f7f7"
                    }
                ]
            },
            {
                "featureType": "poi",
                "elementType": "all",
                "stylers"    : [
                    {
                        "visibility": "simplified"
                    }
                ]
            },
            {
                "featureType": "poi.business",
                "elementType": "all",
                "stylers"    : [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "poi.government",
                "elementType": "all",
                "stylers"    : [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "poi.medical",
                "elementType": "all",
                "stylers"    : [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "poi.park",
                "elementType": "geometry",
                "stylers"    : [
                    {
                        "color": "#deecdb"
                    }
                ]
            },
            {
                "featureType": "poi.park",
                "elementType": "labels",
                "stylers"    : [
                    {
                        "visibility": "on"
                    },
                    {
                        "lightness": "25"
                    }
                ]
            },
            {
                "featureType": "poi.place_of_worship",
                "elementType": "all",
                "stylers"    : [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "poi.school",
                "elementType": "all",
                "stylers"    : [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "poi.sports_complex",
                "elementType": "all",
                "stylers"    : [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "all",
                "stylers"    : [
                    {
                        "lightness": "25"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "labels.icon",
                "stylers"    : [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "geometry",
                "stylers"    : [
                    {
                        "color": "#ffffff"
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "labels",
                "stylers"    : [
                    {
                        "saturation": "-90"
                    },
                    {
                        "lightness": "25"
                    }
                ]
            },
            {
                "featureType": "road.arterial",
                "elementType": "all",
                "stylers"    : [
                    {
                        "visibility": "on"
                    }
                ]
            },
            {
                "featureType": "road.arterial",
                "elementType": "geometry",
                "stylers"    : [
                    {
                        "color": "#ffffff"
                    }
                ]
            },
            {
                "featureType": "road.local",
                "elementType": "geometry",
                "stylers"    : [
                    {
                        "color": "#ffffff"
                    }
                ]
            },
            {
                "featureType": "transit.line",
                "elementType": "all",
                "stylers"    : [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "transit.station",
                "elementType": "all",
                "stylers"    : [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "all",
                "stylers"    : [
                    {
                        "visibility": "on"
                    },
                    {
                        "color": "#e0f1f9"
                    }
                ]
            }
        ];
        var style2 = [
            {
                "featureType": "landscape",
                "stylers"    : [
                    {
                        "hue": "#FFBB00"
                    },
                    {
                        "saturation": 43.400000000000006
                    },
                    {
                        "lightness": 37.599999999999994
                    },
                    {
                        "gamma": 1
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "stylers"    : [
                    {
                        "hue": "#FFC200"
                    },
                    {
                        "saturation": -61.8
                    },
                    {
                        "lightness": 45.599999999999994
                    },
                    {
                        "gamma": 1
                    }
                ]
            },
            {
                "featureType": "road.arterial",
                "stylers"    : [
                    {
                        "hue": "#FF0300"
                    },
                    {
                        "saturation": -100
                    },
                    {
                        "lightness": 51.19999999999999
                    },
                    {
                        "gamma": 1
                    }
                ]
            },
            {
                "featureType": "road.local",
                "stylers"    : [
                    {
                        "hue": "#FF0300"
                    },
                    {
                        "saturation": -100
                    },
                    {
                        "lightness": 52
                    },
                    {
                        "gamma": 1
                    }
                ]
            },
            {
                "featureType": "water",
                "stylers"    : [
                    {
                        "hue": "#0078FF"
                    },
                    {
                        "saturation": -13.200000000000002
                    },
                    {
                        "lightness": 2.4000000000000057
                    },
                    {
                        "gamma": 1
                    }
                ]
            },
            {
                "featureType": "poi",
                "stylers"    : [
                    {
                        "hue": "#00FF6A"
                    },
                    {
                        "saturation": -1.0989010989011234
                    },
                    {
                        "lightness": 11.200000000000017
                    },
                    {
                        "gamma": 1
                    }
                ]
            }
        ];

        $.each(clerinaShortCode.map, function (id, mapData) {
            var map_color = mapData.map_color;

            if (mapData.style == 1) {
                var styles = style1;
            } else {
                var styles = style2;
            }

            customMap = new google.maps.StyledMapType(styles,
                {name: 'Styled Map'});

            if (mapData.number > 1) {
                mutiMaps(infoWindow, bounds, mapOptions, mapData, id, styles, customMap);
            } else {
                singleMap(mapOptions, mapData, id, styles, customMap);
            }

        });
    };

    function singleMap(mapOptions, mapData, id, styles, customMap) {
        var map,
            marker,
            location = new google.maps.LatLng(mapData.lat, mapData.lng);

        // Update map options
        mapOptions.zoom = parseInt(mapData.zoom, 10);
        mapOptions.center = location;
        mapOptions.mapTypeControlOptions = {
            mapTypeIds: [google.maps.MapTypeId.ROADMAP]
        };

        // Init map
        map = new google.maps.Map(document.getElementById(id), mapOptions);

        // Create marker options
        var markerOptions = {
            map     : map,
            position: location
        };
        if (mapData.marker) {
            markerOptions.icon = {
                url: mapData.marker
            };
        }

        map.mapTypes.set('map_style', customMap);
        map.setMapTypeId('map_style');

        // Init marker
        marker = new google.maps.Marker(markerOptions);

        if (mapData.info) {
            var infoWindow = new google.maps.InfoWindow({
                content : '<div class="info-box mf-map">' + mapData.info + '</div>',
                maxWidth: 600
            });

            google.maps.event.addListener(marker, 'click', function () {
                infoWindow.open(map, marker);
            });
        }
    }

    function mutiMaps(infoWindow, bounds, mapOptions, mapData, id, styles, customMap) {

        // Display a map on the page
        mapOptions.zoom = parseInt(mapData.zoom, 10);
        mapOptions.mapTypeControlOptions = {
            mapTypeIds: [google.maps.MapTypeId.ROADMAP]
        };

        var map = new google.maps.Map(document.getElementById(id), mapOptions);
        map.mapTypes.set('map_style', customMap);
        map.setMapTypeId('map_style');
        for (var i = 0; i < mapData.number; i++) {
            var lats = mapData.lat,
                lng = mapData.lng,
                info = mapData.info;

            var position = new google.maps.LatLng(lats[i], lng[i]);
            bounds.extend(position);

            // Create marker options
            var markerOptions = {
                map     : map,
                position: position
            };
            if (mapData.marker) {
                markerOptions.icon = {
                    url: mapData.marker
                };
            }

            // Init marker
            var marker = new google.maps.Marker(markerOptions);

            // Allow each marker to have an info window
            googleMaps(infoWindow, map, marker, info[i]);

            // Automatically center the map fitting all markers on the screen
            map.fitBounds(bounds);
        }
    }

    function googleMaps(infoWindow, map, marker, info) {
        google.maps.event.addListener(marker, 'click', function () {
            infoWindow.setContent(info);
            infoWindow.open(map, marker);
        });
    }

    /**
     * Init testimonial carousel
     */
    clerina.testimonialCarousel = function () {
        if ( clerinaShortCode.length === 0 || typeof clerinaShortCode.testimonial === 'undefined' ) {
            return;
        }

        $.each( clerinaShortCode.testimonial, function ( id, postData ) {
            if ( postData.is_carousel == 1 ) {
                $( document.getElementById( id ) ).slick( {
                    rtl           : clerinaShortCode.isRTL === "1",
                    dots          : postData.dot,
                    autoplay      : postData.autoplay,
                    autoplaySpeed : postData.autoplay_speed,
                    arrows        : postData.nav,
                    infinite      : true,
                    slidesToShow  : postData.column,
                    slidesToScroll: postData.column,
                    responsive    : [
                        {
                            breakpoint: 480,
                            settings  : {
                                arrows: false,
                                slidesToShow: 1
                            }
                        }
                    ]
                } );
            }
        } );

    };

    /**
     * Document ready
     */
    $(function () {
        clerina.init();
    });

})(jQuery);
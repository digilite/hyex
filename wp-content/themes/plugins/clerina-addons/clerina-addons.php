<?php
/**
 * Plugin Name: Clerina Addons
 * Plugin URI: http://grixbase.com/plugins/clerina-addons.zip
 * Description: Extra elements for WPBakery Page Builder. It was built for Clerina theme.
 * Version: 1.0.0
 * Author: Grixbase
 * Author URI: https://themeforest.net/user/grixbase
 * License: GPL2+
 * Text Domain: clerina
 * Domain Path: /lang/
 */
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

if ( ! defined( 'CLERINA_ADDONS_DIR' ) ) {
	define( 'CLERINA_ADDONS_DIR', plugin_dir_path( __FILE__ ) );
}

if ( ! defined( 'CLERINA_ADDONS_URL' ) ) {
	define( 'CLERINA_ADDONS_URL', plugin_dir_url( __FILE__ ) );
}

require_once CLERINA_ADDONS_DIR . '/inc/visual-composer.php';
require_once CLERINA_ADDONS_DIR . '/inc/shortcodes.php';
require_once CLERINA_ADDONS_DIR . '/inc/portfolio.php';
require_once CLERINA_ADDONS_DIR . '/inc/services.php';
require_once CLERINA_ADDONS_DIR . '/inc/testimonials.php';
require_once CLERINA_ADDONS_DIR . '/inc/socials.php';

if ( is_admin() ) {
	require_once CLERINA_ADDONS_DIR . '/inc/importer.php';
}

/**
 * Init
 */
function clerina_vc_addons_init() {
	load_plugin_textdomain( 'clerina', false, dirname( plugin_basename( __FILE__ ) ) . '/lang' );

	new Clerina_VC;
	new Clerina_Shortcodes;
	new Clerina_Portfolio;
	new Clerina_Services;
	new Clerina_Testimonials;

}

add_action( 'after_setup_theme', 'clerina_vc_addons_init', 20 );
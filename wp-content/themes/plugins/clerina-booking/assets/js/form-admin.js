jQuery( document ).ready( function( $ ) {
	var $fields = $( '#cpb-form-avaiable-fields' ),
		$fieldSettings = $( '#cpb-form-field-settings' ),
		$formbuilder = $( '#cpb-form-fields' );

	// Click to add field.
	$fields.on( 'click', '.add-field', function( event ) {
		event.preventDefault();

		var $field = $( this ).closest( '.cpb-form-field' ).clone();

		addNewFieldToForm( $field );

		if ( $formbuilder.children().not( '.empty-fieldset' ).length !== 0 ) {
			$formbuilder.children( '.empty-fieldset' ).hide();
		}

		updateFieldsName();
	} );

	// Remove field from the form.
	$formbuilder.on( 'click', '.remove-field', function( event ) {
		event.preventDefault();

		if ( confirm( cpbForm.l10n.remove_message ) ) {
			$( this ).closest( '.cpb-form-field' ).fadeOut( function() {
				$( this ).remove();

				if ( $formbuilder.children().not( '.empty-fieldset' ).length === 0 ) {
					$formbuilder.children( '.empty-fieldset' ).show();
				}

				updateFieldsName();
			} );
		}
	} );

	// Drag fields.
	$( '.cpb-form-field', $fields ).draggable( {
		cancel: '.field-control',
		revert: 'invalid',
		helper: 'clone',
    	cursor: 'move',
    	containment: '#clerina-booking-form-data',
		revertDuration: 0,
		connectToSortable: '#cpb-form-fields'
	} );

	// Sortable.
	$formbuilder.sortable( {
		items: '> .cpb-form-field',
		placeholder: 'sortable-placeholder',
		cancel: '.cpb-form-field-actions',
		update: function( event, ui ) {
			ui.item.removeAttr( 'style' ).find( '.add-field' ).remove();
			prepareField( ui.item );

			if ( ui.item.length !== 0 ) {
				$formbuilder.children( '.empty-fieldset' ).hide();
			}

			updateFieldsName();
		},
		receive: function( event, ui ) {
			if ( $formbuilder.children().not( '.empty-fieldset' ).length !== 0 ) {
				$formbuilder.children( '.empty-fieldset' ).hide();
			}
		},
		remove: function( event, ui ) {
			if ( $formbuilder.children().not( '.empty-fieldset' ).length === 0 ) {
				$formbuilder.children( '.empty-fieldset' ).show();
			}
		}
	} );

	// Drop new field to the form.
	function addNewFieldToForm( $field ) {
		prepareField( $field );
		$field.appendTo( $formbuilder );
	}

	// Prepare field before adding to the form.
	function prepareField( $field ) {
		var type = $field.data( 'type' ),
			settings = $field.data( 'settings' ) || {};

		settings = parseSettings( settings, type );

		if ( 'columns' === type ) {
			var template = wp.template( 'cpb-field-' + type );

			// Don't destroy initialized columns.
			if ( ! $field.data( 'initialized' ) ) {
				$field.html( template( settings ) );
				$field.data( 'initialized', true );
			}

			$( '.cpb-form-fields-container', $field ).sortable( {
				placeholder: 'sortable-placeholder',
				cancel: '.cpb-form-field-actions',
				connectWith: '#cpb-form-fields, .cpb-field-single-column .cpb-form-fields-container',
				update: function( event, ui ) {
					ui.item.removeAttr( 'style' ).find( '.add-field' ).remove();
					prepareField( ui.item );

					updateFieldsName();
					updateColumnFieldsName( $field );
				}
			} );

			$formbuilder.sortable( 'option', 'connectWith', '.cpb-field-single-column .cpb-form-fields-container' );

			$( '.cpb-form-field', $fields ).draggable( 'option', 'connectToSortable', '#cpb-form-fields, .cpb-field-single-column .cpb-form-fields-container' );
		} else {
			$field.find( '.add-field' ).remove();
		}

		// Add settings to a new field. Stop if this is an update for an exists field.
		if ( $field.children( '.cpb-field-settings' ).length === 0 ) {
			var settingsTemplate = wp.template( 'cpb-field-' + type + '-settings' ),
				$settings = $( '<div class="cpb-field-settings"/>' );

			$settings.html( settingsTemplate( settings ) ).appendTo( $field );
		}
	}

	/**
	 * Parse settings
	 *
	 * @param settings
	 * @param type
	 */
	function parseSettings( settings, type ) {
		if ( ! settings.type ) {
			settings.type = type;
		}

		if ( ! settings.name ) {
			settings.name = type + '_' + new Date().getTime();
		}

		if ( ! settings.index ) {
			settings.index = 999999;
		}

		return settings;
	}

	/**
	 * Update the fields name after the sortable list changed.
	 */
	function updateFieldsName() {
		var basename = $( '#cpb-field-base-name' ).val();

		$formbuilder.children( '.cpb-form-field' ).each( function( index, field ) {
			var $field = $( field ),
				type = $field.data( 'type' );

			$( '> .cpb-field-settings :input', $field ).each( function( inputIndex, input ) {
				var $input = $( input );

				if ( $input.hasClass( 'column-base-name' ) ) {
					input.setAttribute( 'value', basename + '[' + index + '][fields]' );
				} else if ( $input.hasClass( 'option-base-name' ) ) {
					input.setAttribute( 'value', basename + '[' + index + '][options]' );
				} else {
					var setting = input.getAttribute( 'name' ).match( /\[(.*?)\]/g );

					setting = setting[setting.length - 1];

					input.setAttribute( 'name', basename + '[' + index + ']' + setting );
				}
			} );

			$( '> .cpb-field-settings [data-index]', $field ).attr( 'data-index', index );

			switch ( type ) {
				case 'columns':
					updateColumnFieldsName( $field );
					break;

				case 'select':
				case 'radio':
				case 'checklist':
					updateFieldOptionsName( $field.find( '> .cpb-field-settings .field-options' ).children() );
					break;
			}
		} );
	}

	/**
	 * Update fields name inside columns.
	 */
	function updateColumnFieldsName( $columns ) {
		var basename = $columns.find( '> .cpb-field-settings > .column-base-name' ).val();

		$( '> .columns-row > .cpb-field-single-column', $columns ).each( function( columnIndex, column ) {
			var $column = $( column );

			$( '> .cpb-form-fields-container > .cpb-form-field', $column ).each( function( fieldIndex, field ) {
				var $field = $( field ),
					type = $field.data( 'type' );

				$( '> .cpb-field-settings :input', field ).each( function( inputIndex, input ) {
					var $input = $( input );

					if ( $input.hasClass( 'column-base-name' ) ) {
						input.setAttribute( 'value', basename + '[' + columnIndex + '][' + fieldIndex + '][fields]' );
					} else if ( $input.hasClass( 'option-base-name' ) ) {
						input.setAttribute( 'value', basename + '[' + columnIndex + '][' + fieldIndex + '][options]' );
					} else {
						var setting = input.getAttribute( 'name' ).match( /\[(.*?)\]/g );

						setting = setting[setting.length - 1];

						input.setAttribute( 'name', basename + '[' + columnIndex + '][' + fieldIndex + ']' + setting );
					}
				} );

				$( '> .cpb-field-settings [data-index]', field ).attr( 'data-index', fieldIndex );

				switch ( type ) {
					case 'columns':
						updateColumnFieldsName( $field );
						break;

					case 'select':
					case 'radio':
					case 'checklist':
						updateFieldOptionsName( $field.find( '> .cpb-field-settings .field-options' ).children() );
						break;
				}
			} );
		} );
	}

	// Add new column.
	$formbuilder.on( 'click', '.columns-actions .add-column', function( event ) {
		event.preventDefault();

		var $columns = $( this ).closest( '.cpb-form-field-columns' ),
			$row = $columns.children( '.columns-row' ),
			columnTemplate = wp.template( 'cpb-field-single-column' );

		$row.append( columnTemplate() );

		var $newColumn = $row.children().last();

		$( '.cpb-form-fields-container', $newColumn ).sortable( {
			placeholder: 'sortable-placeholder',
			cancel: '.cpb-form-field-actions',
			connectWith: '#cpb-form-fields, .cpb-field-single-column .cpb-form-fields-container',
			update: function( event, ui ) {
				ui.item.removeAttr( 'style' ).find( '.add-field' ).remove();
				prepareField( ui.item );

				updateFieldsName();
				updateColumnFieldsName( $columns );
			}
		} );

		$formbuilder.sortable( 'option', 'connectWith', '.cpb-field-single-column .cpb-form-fields-container' );

		$( '.cpb-form-field', $fields ).draggable( 'option', 'connectToSortable', '#cpb-form-fields, .cpb-field-single-column .cpb-form-fields-container' );
	} );

	// Remove a column.
	$formbuilder.on( 'click', '.column-actions .remove-column', function( event ) {
		event.preventDefault();

		var $column = $( this ).closest( '.cpb-field-single-column' ),
			$columns = $column.closest( '.cpb-form-field-columns' );
		
		$column.fadeOut( function() {
			$column.remove();
			updateColumnFieldsName( $columns );
		} );
	} );

	// Edit a field.
	$formbuilder.on( 'click', '.edit-field', function( event ) {
		event.preventDefault();

		var $field = $( this ).closest( '.cpb-form-field' ),
			$settings = $field.children( '.cpb-field-settings' );

		$fieldSettings.data( 'original_settings', $settings.clone() );
		$fieldSettings.data( 'field', $field );
		$fieldSettings.find( '.cpb-field-settings' ).remove();
		$settings.prependTo( $fieldSettings );

		$fieldSettings.trigger( 'field_settings_rendered' );

		$fieldSettings.fadeIn();
	} );

	// Update selected option state.
	$fieldSettings.on( 'change', '.cpb-field-settings select', function() {
		$( 'option[value=' + this.value + ']', this ).prop( 'selected' , true ).siblings().removeAttr( 'selected' );
	});

	// Save or cancel field editing.
	$fieldSettings.on( 'click', '.save', function( event ) {
		event.preventDefault();

		var $field = $fieldSettings.data( 'field' ),
			$settings = $fieldSettings.find( '.cpb-field-settings' );

		if ( ! $field ) {
			return;
		}

		// Update label.
		var $labelInput = $( 'input.field-label-input', $settings ),
			$adminLabelInput = $( 'input.admin-label-input', $settings );

		if ( $labelInput.length || $adminLabelInput.length ) {
			$field.children( '.field-label' ).remove();

			if ( $labelInput.length && $labelInput.val() ) {
				$field.children( 'h4' ).addClass( 'field-type-right' ).after( $( '<h4 class="field-label" />' ).text( $labelInput.val() ) );
			} else if ( $adminLabelInput.length && $adminLabelInput.val() ) {
				$field.children( 'h4' ).addClass( 'field-type-right' ).after( $( '<h4 class="field-label" />' ).text( $adminLabelInput.val() ) );
			} else {
				$field.children( 'h4' ).removeClass( 'field-type-right' );
			}
		}

		// Update the setting container.
		$field.children( '.cpb-field-settings' ).remove();
		$fieldSettings.fadeOut( function() {
			$settings.appendTo( $field );
		} );

		$fieldSettings.data( 'field', false );
	} ).on( 'click', '.cancel', function( event ) {
		event.preventDefault();

		var $field = $fieldSettings.data( 'field' ),
			$settings = $fieldSettings.data( 'original_settings' );

		$fieldSettings.fadeOut( function() {
			$fieldSettings.find( '.cpb-field-settings' ).remove();
		} );

		$field.append( $settings );
		$fieldSettings.data( 'field', false );
		$fieldSettings.data( 'original_settings', false );
	} );

	// Field options sortable.
	$fieldSettings.on( 'field_settings_rendered', function() {
		var $options = $fieldSettings.find( '.field-options' );

		if ( ! $options.length ) {
			return;
		}

		$options.sortable( {
			handle: '.field-option-handle',
			update: function( event, ui ) {
				updateFieldOptionsName( $options.children() );
			}
		} );
	} );

	// Field options actions.
	$fieldSettings.on( 'click', '.field-options .field-option-remove', function( event ) {
		var $option = $( this ).closest( '.field-option' ),
			$options = $option.parent();

		$option.fadeOut( function() {
			$option.remove();
			updateFieldOptionsName( $options.children() );
		} );

	} ).on( 'click', '.field-options-add-new', function( event ) {
		event.preventDefault();

		var $addnew = $( this ),
			$options = $addnew.parent().siblings( '.field-options' ),
			length = $options.children().length,
			optionTemplate = _.template( $( '#tmpl-cpb-field-' + $addnew.data( 'type' ) + '-option' ).html(), {
				evaluate:    /<#([\s\S]+?)#>/g,
				interpolate: /\{\{\{([\s\S]+?)\}\}\}/g,
				escape:      /\{\{([^\}]+?)\}\}(?!\})/g
			} ), // Don't use wp.template
			data = {
				data: {
					index: $options.data( 'index' ),
					selected: ''
				},
				key: length,
				value: {
					label: '',
					value: '',
					price: ''
				}
			};

		$options.append( optionTemplate( data ) );

		updateFieldOptionsName( $options.children() );
	} );

	/**
	 * Update field options name.
	 */
	function updateFieldOptionsName( $items ) {
		var basename = $items.closest( '.cpb-field-settings' ).find( 'input.option-base-name' ).val();

		$items.each( function( index, item ) {
			$( ':input', item ).each( function( inputIndex, input ) {
				if ( $( input ).hasClass( 'field-options-default-option' ) ) {
					if ( 'checkbox' === input.type ) {
						input.setAttribute( 'name', basename.replace( '[options]', '[selected][]' ) );
					} else {
						input.setAttribute( 'name', basename.replace( '[options]', '[selected]' ) );
					}

					input.value = index;
				} else {
					var setting = input.getAttribute( 'name' ).match( /\[(.*?)\]/g );
					setting = setting[setting.length - 1];

					input.setAttribute( 'name', basename + '[' + index + ']' + setting );
				}
			} );

		} );
	}

	/**
	 * Display saved form fields.
	 */
	function buildFormFields() {
		var fields = $( '#cpb-form-fields-data' ).data( 'fields' );

		if ( _.isEmpty( fields ) ) {
			return;
		}

		_.each( fields, function( field ) {
			buildField( field );
		} );
	}

	/**
	 * Add a saved field to the form builder interface.
	 *
	 * @param field
	 * @param $holder
	 */
	function buildField( field, $holder ) {
		if ( _.isEmpty( $holder ) ) {
			$holder = $formbuilder;
		}

		var $field = $fields.children( '[data-type="' + field.type + '"]' );

		if ( !$field.length ) {
			return;
		}

		if ( 'columns' === field.type && !field.fields ) {
			return;
		}

		$field = $field.clone();
		$field.data( 'settings', field );

		prepareField( $field );
		$holder.append( $field );

		updateFieldsName();

		// Update label.
		if ( 'paragrap' !== field.type ) {
			if ( field.label ) {
				$field.children( 'h4' ).addClass( 'field-type-right' ).after( $( '<h4 class="field-label" />' ).text( field.label ) );
			} else if ( field.text ) {
				$field.children( 'h4' ).addClass( 'field-type-right' ).after( $( '<h4 class="field-label" />' ).text( field.text ) );
			} else if ( field.admin_label ) {
				$field.children( 'h4' ).addClass( 'field-type-right' ).after( $( '<h4 class="field-label" />' ).text( field.admin_label ) );
			} else {
				$field.children( 'h4' ).removeClass( 'field-type-right' );
			}
		}

		// Build column fields.
		if ( 'columns' === field.type ) {
			_.each( field.fields, function( fields, column ) {
				var $column = $field.find( '> .columns-row > .cpb-field-single-column' ).eq( column ).children( '.cpb-form-fields-container' );

				_.each( fields, function( field ) {
					buildField( field, $column );
				} );
			} );
		}
	}

	// Run when page loaded.
	buildFormFields();

	// JS and CSS editors.
	if( $('#code_editor_form_js').length ) {
		var editorSettings = wp.codeEditor.defaultSettings ? _.clone( wp.codeEditor.defaultSettings ) : {};
		editorSettings.codemirror = _.extend(
			{},
			editorSettings.codemirror,
			{
				indentUnit: 2,
				tabSize: 2,
				mode: 'javascript'
			}
		);
		wp.codeEditor.initialize( $('#code_editor_form_js'), editorSettings );
	}

	if( $('#code_editor_form_css').length ) {
		var editorSettings = wp.codeEditor.defaultSettings ? _.clone( wp.codeEditor.defaultSettings ) : {};
		editorSettings.codemirror = _.extend(
			{},
			editorSettings.codemirror,
			{
				indentUnit: 2,
				tabSize: 2,
				mode: 'css'
			}
		);
		wp.codeEditor.initialize( $('#code_editor_form_css'), editorSettings );
	}

	// Toggle button action input.
	$( document.body ).on( 'change', 'select.button-action-toggle', function() {
		if ( 'open_new_form' === $( this ).val() ) {
			$( this ).next( 'input' ).removeClass( 'hidden' );
		} else {
			$( this ).next( 'input' ).addClass( 'hidden' );
		}
	} );
} );
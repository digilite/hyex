/** Global cpbData */
(function( $ ) {
	/**
	 * Estimate price.
	 */
	function estimatePrice( form ) {
		var $form = form ? $( form ) : $( '.clerina-booking-form' ),
			$estimate = $( '.cpb-estimate-field', $form ),
			base = {},
			total = 0;

		if ( ! $form.length || ! $estimate.length ) {
			return;
		}

		$( '[data-price]', $form ).each( function() {
			var $field = $( this ),
				priceData = $field.data( 'price' ),
				val = 0;

			if ( ! priceData ) {
				return;
			}

			if ( $field.hasClass( 'cpb-select-field' ) ) {
				val = $field.find( 'select' ).val();

				if ( priceData[val] ) {
					total += parseFloat( priceData[val] );
				}
			} else if ( $field.hasClass( 'cpb-radio-field' ) ) {
				val = $field.find( 'input:checked' ).val();

				if ( priceData[val] ) {
					total += parseFloat( priceData[val] );
				}
			} else if ( $field.hasClass( 'cpb-checklist-field' ) ) {
				$field.find( 'input:checked' ).each( function() {
					val = $( this ).val();

					if ( priceData[val] ) {
						total += parseFloat( priceData[val] );
					}
				} );
			} else if ( $field.hasClass( 'cpb-checkbox-field' ) ) {
				if ( $( 'input[type=checkbox]', $field ).is( ':checked' ) ) {
					total += parseFloat( priceData );
				}
			} else {
				val = $( ':input', $field ).val();
				val = val ? parseFloat( val ) : 0;

				total += ( val * parseFloat( priceData ) );
			}
		} );

		$estimate.each( function() {
			var $price = $( this );

			base = $price.data( 'settings' );

			var totalPrice = total + parseFloat( base.base );
			totalPrice = formatPrice( totalPrice, parseInt( base.num_decimals ) );

			$( '.cpb-price-amount', $price ).text( totalPrice );
			$( 'input.estimate-price-input', $price ).val( totalPrice );
		} );
	}

	/**
	 * Format price.
	 */
	function formatPrice( price, decimals ) {
		if ( typeof decimals === undefined ) {
			decimals = 2;
		}
		return new Intl.NumberFormat( false, {
			maximumFractionDigits: decimals,
			minimumFractionDigits: decimals
		} ).format( price.toFixed( decimals ) );
	}

	/**
	 * Get form summary. It is used for form confirmation before submitting.
	 */
	function getFormSummary( $form ) {
		var $fields = $( '.cpb-field:visible', $form ),
			data = [];

		$fields.each( function( index, field ) {
			var $field = $( field ),
				$input = $( ':input', $field ),
				name = $input.prop( 'name' ),
				label = $field.attr( 'aria-label' ),
				value = '',
				html = '',
				price = false;

			if ( ! name || undefined === label ) {
				return;
			}

			if ( $field.hasClass( 'cpb-checkbox-field' ) ) {
				value = $input.is( ':checked' ) ? $input.val() : '';
			} else if ( $field.hasClass( 'cpb-radio-field' ) ) {
				$input = $input.filter( ':checked' );

				var valueText = $input.next( 'span' ).text().trim();

				if ( valueText ) {
					value = valueText;
				} else {
					value = $input.val();
				}
			} else if ( $field.hasClass( 'cpb-checklist-field' ) ) {
				$input.filter( ':checked' ).each( function() {
					var $checked = $( this ),
						valueText = $checked.parent().text().trim();

					if ( valueText ) {
						value = value ? value + ', ' + valueText : valueText;
					} else {
						value = value ? value + ', ' + $checked.val() : $checked.val();
					}
				} );
			} else if ( $field.hasClass( 'cpb-select-field' ) ) {
				var select = $input.get( 0 );
				value = select.options[select.selectedIndex].text;
			} else if ( $field.hasClass( 'cpb-estimate-field' ) ) {
				if ( $field.parent().hasClass( 'cpb-pricing_table-field' ) ) {
					return;
				}

				value = $input.val();
				html = $field.find( '.cpb-price' ).clone();
				price = true;
			} else if ( $field.hasClass( 'cpb-pricing_table-field' ) ) {
				$input = $( 'input.pricing-table-name-input', $field );

				if ( ! $input.is(':checked') ) {
					return;
				}

				name = $input.prop( 'name' );
				value = $input.val();

				data.push( {
					label: '',
					name: $( 'input.estimate-price-input', $field ).attr( 'name' ),
					value: $( 'input.estimate-price-input', $field ).val(),
					price: true,
					html: $( '.cpb-estimate-field', $field ).find( '.cpb-price' ).clone()
				} );
			} else {
				value = $input.val();
			}

			data.push( {
				label: label,
				name: name,
				value: value,
				price: price,
				html: html
			} );
		} );

		return data;
	}

	/**
	 * Open dialog
	 */
	function openDialog( $dialog ) {
		if ( ! $dialog.length ) {
			return;
		}

		$dialog.fadeIn();
	}

	/**
	 * Close dialog
	 */
	function closeDialog( $dialog ) {
		if ( ! $dialog ) {
			$( '.cpb-dialog' ).fadeOut();
		} else {
			$dialog.fadeOut();
		}
	}

	/**
	 * Validate form fields.
	 */
	function validateForm( $form ) {
		var $fields = $( ':input:visible[required]', $form ),
			valid = true;

		$fields.each( function() {
			if ( '' == this.value ) {
				valid = false;
				$( this ).addClass( 'novalid' );
			}
		} );

		return valid;
	}

	// Run the first time when the documentation ready.
	$( function() {
		// Estimate price.
		estimatePrice();

		$( document.body ).on( 'input change', '[data-price] :input', function() {
			var form = $( this ).closest( '.clerina-booking-form' );

			estimatePrice( form );
		} );

		// Update range helper input.
		$( document.body ).on( 'input change', '.cpb-range-field input[type=range]', function() {
			var $range = $( this ),
				$helper = $range.next( '.cpb-range-input-helper' );

			$helper.val( $range.val() );
		} );

		// Change the selected pricing table.
		$( document.body ).on( 'click', '.cpb-pricing_table-field button', function( event ) {
			// event.preventDefault();

			var $button = $( this ),
				$table = $button.parent();

			$( '.estimate-price-input, .pricing-table-name-input', $table ).prop( 'checked', true );
		} );

		// Confirm before submit.
		$( document.body )
			.on( 'click', '.clerina-booking-form button[data-action="confirm"]', function( event ) {
				event.preventDefault();

				var $button = $( this ),
					$form = $button.closest( 'form' ),
					$dialog = $form.next( '.cpb-dialog' );

				if ( ! $button.closest( '.cpb-dialog' ).length && ! $button.parent().hasClass( 'cpb-pricing_table-field' ) ) {
					$( '.cpb-pricing_table-field', $form ).find( ':input' ).prop( 'checked', false );
				}

				if ( ! validateForm( $form ) ) {
					return;
				}

				var data = getFormSummary( $form ),
					price = [],
					summary = '';

				for ( var i = 0; i < data.length; i++ ) {
					if ( data[i].price ) {
						price.push( data[i] );
					} else {
						summary += '<li><span class="cpb-label">' + data[i]['label'] + ':</span><span class="cpn-value">' + data[i]['value'] + '</span></li>';
					}
				}

				summary = '<ul class="cpb-selected-options">' + summary + '</ul>';

				for ( var i = 0; i < price.length; i++ ) {
					summary += '<div class="cpb-form-summary-price"><span class="cpb-label">' + price[i].label + '</span>' + price[i].html[0].outerHTML + '</div>';
				}

				$dialog.find( '.cpb-dialog-summary .cpb-dialog-summary-body' ).html( summary );

				// Close other dialogs.
				closeDialog();

				// Open summary dialog.
				$dialog.addClass( 'open-summary' ).fadeIn();
			} )
			.on( 'click', '.cpb-backdrop, .cpb-dialog-cancel-button', function( event ) {
				event.preventDefault();

				var $dialog = $( this ).closest( '.cpb-dialog' );

				if ( $dialog.hasClass( 'loading' ) ) {
					return;
				}

				$dialog.fadeOut( function() {
					$dialog.removeClass( 'open-summary open-fields success failed error loading' );

					$( '.cpb-linked-form-id-input', '.clerina-booking-form' ).prop( 'checked', false );
				} );
			} )
			.on( 'click', '.cpb-dialog-confirm-button', function( event ) {
				event.preventDefault();

				var $button = $( this ),
					$dialog = $button.closest( '.cpb-dialog' ),
					$form = $dialog.prev( 'form' ),
					data = $form.serialize();

				if ( ! data ) {
					return;
				}

				data += '&action=cpb_submit_form';

				$dialog.removeClass( 'open-summary success failed error' ).addClass( 'loading' );

				$.post( cpbData.ajax_url, data, function( response ) {
					$dialog.removeClass( 'open-summary success failed error loading' ).addClass( response.success ? 'success' : 'failed' );
					$( '.cpb-linked-form-id-input', $form ).prop( 'checked', false );
				} ).fail( function() {
					$dialog.removeClass( 'open-summary success failed error loading' ).addClass( 'error' );
					$( '.cpb-linked-form-id-input', $form ).prop( 'checked', false );
				} );
			} )
			.on( 'click', '.clerina-booking-form button[data-action="open_new_form"]', function( event ) {
				event.preventDefault();

				var $button = $( this ),
					$form = $button.closest( 'form' ),
					form_id = $button.data( 'linked_form' ),
					$dialog = $form.find( '.cpb-linked-form-' + form_id );

				if ( ! validateForm( $form ) ) {
					return;
				}

				$( '.cpb-linked-form-id-input', $form ).prop( 'checked', false );
				$( '.cpb-linked-form-id-input', $dialog ).prop( 'checked', true );

				$dialog.addClass( 'open-fields' ).fadeIn();
			} )
			.on( 'click', '.cpb-linked-form button[data-action=submit]', function( event ) {
				event.preventDefault();

				var $button = $( this ),
					$dialog = $button.closest( '.cpb-dialog' ),
					$form = $dialog.closest( 'form' ),
					data = $form.serialize();

				if ( ! data ) {
					return;
				}

				if ( ! validateForm( $form ) ) {
					return;
				}

				data += '&action=cpb_submit_form';

				$dialog.removeClass( 'open-fields success failed error' ).addClass( 'loading' );

				$.post( cpbData.ajax_url, data, function( response ) {
					$dialog.removeClass( 'open-fields success failed error loading' ).addClass( response.success ? 'success' : 'failed' );
					$( '.cpb-linked-form-id-input', $form ).prop( 'checked', false );
				} ).fail( function() {
					$dialog.removeClass( 'open-fields success failed error loading' ).addClass( 'error' );
					$( '.cpb-linked-form-id-input', $form ).prop( 'checked', false );
				} );
			} )
			.on( 'change input', '.clerina-booking-form :input.novalid', function() {
				$( this ).removeClass( 'novalid' );
			} );
	} );
})( jQuery );
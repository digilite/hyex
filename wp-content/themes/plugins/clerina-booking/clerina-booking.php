<?php
/*
Plugin Name: Clerina Booking
Plugin URI: http://grixbase.com/plugins/clerina-booking.zip
Description: Primium booking plugin built for Clerina theme.
Author: Grixbase
Author URI: https://themeforest.net/user/grixbase
Text Domain: clerina-booking
Domain Path: /languages/
Version: 1.0.0
*/


final class Clerina_Booking {
	/**
	 * The single instance of the class
	 */
	protected static $instance = null;

	/**
	 * Initialize
	 */
	public static function instance() {
		if ( null == self::$instance ) {
			self::$instance = new self();
		}

		return self::$instance;
	}

	/**
	 * Class construction
	 */
	public function __construct() {
		$this->define_constants();
		$this->includes();
		$this->init();
	}

	/**
	 * Define contstants
	 */
	public function define_constants() {
		define( 'CLERINA_BOOKING_VERSION', '1.0.0' );
		define( 'CLERINA_BOOKING_PLUGIN', __FILE__ );
		define( 'CLERINA_BOOKING_BASENAME', plugin_basename( CLERINA_BOOKING_PLUGIN ) );
		define( 'CLERINA_BOOKING_NAME', trim( dirname( CLERINA_BOOKING_BASENAME ), '/' ) );
		define( 'CLERINA_BOOKING_DIR', untrailingslashit( dirname( CLERINA_BOOKING_PLUGIN ) ) );
		define( 'CLERINA_BOOKING_URL', untrailingslashit( plugins_url( '', CLERINA_BOOKING_PLUGIN ) ) );
	}

	/**
	 * Include required core files used in admin and on the frontend.
	 */
	public function includes() {
		include_once 'includes/functions.php';
		include_once 'includes/order.php';
		include_once 'includes/field.php';
		include_once 'includes/form.php';
		include_once 'includes/form-handler.php';
		include_once 'includes/render.php';
		include_once 'includes/shortcode.php';

		if ( is_admin() ) {
			include_once 'includes/admin/admin.php';
		}

		$this->includes_fields();
	}

	/**
	 * Include field files.
	 */
	public function includes_fields() {
		foreach ( clerina_booking_get_fields() as $field ) {
			$file = CLERINA_BOOKING_DIR . '/includes/fields/' . $field['type'] . '.php';

			if ( file_exists( $file ) ) {
				require_once $file;
			}
		}
	}

	/**
	 * Plugin init
	 */
	public function init() {
		add_action( 'init', array( $this, 'load_textdomain' ) );

		add_action( 'init', array( 'Clerina_Booking_Order', 'init' ) );
		add_action( 'init', array( 'Clerina_Booking_Form', 'init' ) );
		add_action( 'init', array( 'Clerina_Booking_Form_Handler', 'init' ) );
		add_action( 'init', array( 'Clerina_Booking_Shortcode', 'init' ) );
	}

	/**
	 * Load plugin text domain
	 */
	public function load_textdomain() {
		load_plugin_textdomain( 'clerina-booking', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
	}
}

Clerina_Booking::instance();
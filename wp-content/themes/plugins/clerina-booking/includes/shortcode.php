<?php

class Clerina_Booking_Shortcode {
	/**
	 * Initialize.
	 */
	public static function init() {
		add_shortcode( 'clerina_booking_form', array( __CLASS__, 'form_shortcode' ) );

		add_action( 'wp_enqueue_scripts', array( __CLASS__, 'scripts' ) );
	}

	/**
	 * Scripts for shortcodes.
	 */
	public static function scripts() {
		wp_enqueue_style( 'clerina-booking-form', CLERINA_BOOKING_URL . '/assets/css/form.css' );
		wp_enqueue_script( 'clerina-booking-form', CLERINA_BOOKING_URL . '/assets/js/form.js', array( 'jquery' ), CLERINA_BOOKING_VERSION, true );

		wp_localize_script( 'clerina-booking-form', 'cpbData', array(
			'ajax_url' => admin_url( 'admin-ajax.php' ),
		) );
	}

	/**
	 * Booking form shortcode.
	 *
	 * @param $atts
	 *
	 * @return string
	 */
	public static function form_shortcode( $atts ) {
		$atts = shortcode_atts( array(
			'id' => '',
			'title' => '',
		), $atts, 'clerina_booking_form' );

		$form_id = intval( $atts['id'] );

		if ( ! $form_id && ! empty( $atts['title'] ) ) {
			$form = get_page_by_title( $atts['title'], OBJECT, 'cpb_form' );
			$form_id = $form ? $form->ID : false;
		}

		if ( ! $form_id ) {
			return '';
		}

		$output = Clerina_Booking_Render::instance()->render_form( $form_id );

		return $output;
	}
}
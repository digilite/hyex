<?php

class Clerina_Booking_Columns_Field extends Clerina_Booking_Field {
	public $type = 'columns';

	/**
	 * Template for field settings.
	 */
	public function settings_template() {
		?>

		<script type="text/html" id="tmpl-cpb-field-<?php echo $this->type ?>-settings">
			<?php $this->settings_template_content(); ?>
		</script>

		<script type="text/html" id="tmpl-cpb-field-<?php echo $this->type ?>">
			<?php $this->columns_template_content(); ?>
		</script>

		<script type="text/html" id="tmpl-cpb-field-single-column">
			<?php $this->single_column_template_content(); ?>
		</script>

		<?php
	}

	/**
	 * Template for field settings.
	 */
	public function settings_template_content() {
		?>
		<input type="hidden" value="<?php echo esc_attr( $this->type ) ?>"
			   name="<?php echo $this->setting_name( 'type' ); ?>">
		<input type="hidden" value="<?php echo $this->setting_name( 'fields' ); ?>" class="column-base-name">

		<table class="form-table">
			<tr>
				<th><label><?php _e( 'Class', 'clerina-booking' ) ?></label></th>
				<td><input type="text" value="{{data.class}}" name="<?php echo $this->setting_name( 'class' ); ?>"
						   class="widefat"></td>
			</tr>
		</table>
		<?php
	}

	/**
	 * Template for columns.
	 */
	public function columns_template_content() {
		?>
		<div class="columns-actions cpb-form-field-actions">
			<span class="field-control add-column"><i
						class="dashicons dashicons-plus"></i> <?php _e( 'Add Column', 'clerina-booking' ) ?></span>
			<span class="field-control edit-field"><i class="dashicons dashicons-edit"></i></span>
			<span class="field-control remove-field"><i class="dashicons dashicons-no-alt"></i></span>
		</div>
		<h4><?php _e( 'Columns' ) ?></h4>
		<div class="columns-row">
			<# if ( data.fields.length ) { #>
			<# _.each( data.fields, function( fields ) { #>
			<?php $this->single_column_template_content(); ?>
			<# } ); #>
			<# } else { #>
			<?php $this->single_column_template_content(); ?>
			<?php $this->single_column_template_content(); ?>
			<# } #>
		</div>
		<?php
	}

	/**
	 * Template for single column.
	 */
	public function single_column_template_content() {
		?>
		<div class="cpb-field-single-column">
			<div class="column-actions">
				<span class="remove-column"><i class="dashicons dashicons-no-alt"></i></span>
			</div>
			<div class="cpb-form-fields-container"></div>
		</div>
		<?php
	}

	/**
	 * Render the field.
	 *
	 * @param array $settings
	 *
	 * @return string
	 */
	public function render( $settings = array() ) {
		$settings = wp_parse_args( $settings, $this->get_default_settings() );
		$render = Clerina_Booking_Render::instance();

		$output = '<div class="cpb-field cpb-columns-field columns-field-' . count( $settings['fields'] ) . ' ' . esc_attr( $settings['class'] ) . '">';

		foreach ( $settings['fields'] as $column => $fields ) {
			$output .= '<div class="cpb-column cpb-column-' . ( $column + 1 ) . '">';

			foreach ( $fields as $field ) :

				$output .= $render->render_field( $field );

			endforeach;

			$output .= '</div>';
		}

		$output .= '</div>';

		return $output;
	}
}
<?php
class Clerina_Booking_Estimate_Field extends Clerina_Booking_Field {
	public $type = 'estimate';

	/**
	 * Template for field settings.
	 */
	public function settings_template_content() {
		?>

		<input type="hidden" value="<?php echo esc_attr( $this->type ) ?>" name="<?php echo $this->setting_name( 'type' ); ?>">

		<table class="form-table">
			<tr>
				<th><label><?php _e( 'Label', 'clerina-booking' ) ?></label></th>
				<td><input type="text" value="{{data.label}}" name="<?php echo $this->setting_name( 'label' ); ?>" class="widefat"></td>
			</tr>
			<tr>
				<th><label><?php _e( 'Name', 'clerina-booking' ) ?></label></th>
				<td><input type="text" value="{{data.name}}" name="<?php echo $this->setting_name( 'name' ); ?>" class="widefat"></td>
			</tr>
			<tr>
				<th><label><?php _e( 'Base Price', 'clerina-booking' ) ?></label></th>
				<td><input type="text" value="{{data.base}}" name="<?php echo $this->setting_name( 'base' ); ?>"></td>
			</tr>
			<tr>
				<th><label><?php _e( 'Currency', 'clerina-booking' ) ?></label></th>
				<td><input type="text" value="{{data.currency}}" name="<?php echo $this->setting_name( 'currency' ); ?>"></td>
			</tr>
			<tr>
				<th><label><?php _e( 'Currency Position', 'clerina-booking' ) ?></label></th>
				<td>
					<select value="{{data.currency_position}}" name="<?php echo $this->setting_name( 'currency_position' ); ?>">
						<option value="left" {{'left' == data.currency_position ? 'selected' : ''}}><?php _e( 'Left', 'clerina-booking' ) ?></option>
						<option value="right" {{'right' == data.currency_position ? 'selected' : ''}}><?php _e( 'Right', 'clerina-booking' ) ?></option>
						<option value="left_space" {{'left_space' == data.currency_position ? 'selected' : ''}}><?php _e( 'Left with space', 'clerina-booking' ) ?></option>
						<option value="right_space" {{'right_space' == data.currency_position ? 'selected' : ''}}><?php _e( 'Right with space', 'clerina-booking' ) ?></option>
					</select>
				</td>
			</tr>
			<tr>
				<th><label><?php _e( 'Number of decimals', 'clerina-booking' ) ?></label></th>
				<td><input type="number" value="{{data.num_decimals}}" name="<?php echo $this->setting_name( 'num_decimals' ); ?>"></td>
			</tr>
			<tr>
				<th><label><?php _e( 'Class', 'clerina-booking' ) ?></label></th>
				<td><input type="text" value="{{data.class}}" name="<?php echo $this->setting_name( 'class' ); ?>" class="widefat"></td>
			</tr>
			<tr>
				<th><label><?php _e( 'Admin Label', 'clerina-booking' ) ?></label></th>
				<td>
					<input type="text" value="{{data.admin_label}}" name="<?php echo $this->setting_name( 'admin_label' ); ?>" class="widefat admin-label-input">
					<p class="description"><?php _e( 'The label to be used in the order', 'clerina-booking' ) ?></p>
				</td>
			</tr>
		</table>

		<?php
	}

	/**
	 * Render the field.
	 *
	 * @param array $settings
	 *
	 * @return string
	 */
	public function render( $settings = array() ) {
		$settings = wp_parse_args( $settings, $this->get_default_settings() );
		$format = $settings;
		unset( $format['name'] );
		unset( $format['type'] );
		unset( $format['class'] );

		$label = empty( $settings['label'] ) ? '' : sprintf( '<span class="cpb-input-label">%s</span>', esc_html( $settings['label'] ) );
		$price = clerina_booking_format_price( $settings['base'], $settings['num_decimals'], $settings['currency'], $settings['currency_position'] );

		return sprintf(
			'<div class="cpb-field cpb-estimate-field %s" data-settings="%s" aria-label="%s">%s<input type="%s" name="%s" value="%s" class="estimate-price-input">%s</div>',
			esc_attr( $settings['class'] ),
			esc_attr( json_encode( $format ) ),
			esc_attr( $this->get_aria_label( $settings ) ),
			$label,
			'estimate' === $this->type ? 'hidden' : 'radio',
			esc_attr( $settings['name'] ),
			esc_attr( strip_tags( $price ) ),
			$price
		);
	}
}
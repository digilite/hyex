<?php
class Clerina_Booking_Checklist_Field extends Clerina_Booking_Select_Field {
	public $type = 'checklist';

	/**
	 * Template for a single option.
	 */
	public function option_settings_template_content() {
		?>

		<div class="field-option">
			<span class="field-option-handle"><i class="dashicons dashicons-menu"></i></span>
			<input type="checkbox" name="<?php echo $this->setting_name( 'selected' ); ?>[]" value="{{key}}" {{_.contains( data.selected, key.toString() ) ? "checked" : ""}} class="field-options-default-option">
			<input type="text" name="<?php echo $this->setting_name( 'options' ); ?>[{{key}}][label]" placeholder="<?php esc_attr_e( 'Label', 'clerina-booking' ) ?>" value="{{value.label}}">
			<input type="text" name="<?php echo $this->setting_name( 'options' ); ?>[{{key}}][value]" placeholder="<?php esc_attr_e( 'Value', 'clerina-booking' ) ?>" value="{{value.value}}">
			<input type="text" name="<?php echo $this->setting_name( 'options' ); ?>[{{key}}][price]" placeholder="<?php esc_attr_e( 'Price', 'clerina-booking' ) ?>" value="{{value.price}}">
			<span class="field-option-remove"><i class="dashicons dashicons-no-alt"></i></span>
		</div>

		<?php
	}

	/**
	 * Render the field.
	 *
	 * @param array $settings
	 *
	 * @return string
	 */
	public function render( $settings = array() ) {
		$settings = wp_parse_args( $settings, $this->get_default_settings() );
		$price_data = $this->get_price_data( $settings['options'] );

		$output = '<div class="cpb-field cpb-checklist-field ' . esc_attr( $settings['class'] ) . '" data-price="' . esc_attr( json_encode( $price_data ) ) . '" aria-label="' . esc_attr( $this->get_aria_label( $settings ) ) . '">';
		$output .= empty( $settings['label'] ) ? '' : sprintf( '<span class="cpb-input-label">%s</span>', esc_html( $settings['label'] ) );
		$output .= '<ul>';

		foreach ( $settings['options'] as $index => $option ) {
			$output .= sprintf(
				'<li><label><input type="checkbox" name="%s[]" value="%s" %s> %s</label></li>',
				esc_attr( $settings['name'] ),
				esc_attr( $option['value'] ),
				checked( in_array( $index, $settings['selected'] ), true, false ),
				esc_html( $option['label'] )
			);
		}

		$output .= '</ul></div>';

		return $output;
	}
}
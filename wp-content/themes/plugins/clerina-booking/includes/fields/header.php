<?php
class Clerina_Booking_Header_Field extends Clerina_Booking_Field {
	public $type = 'header';

	/**
	 * Template for field settings.
	 */
	public function settings_template_content() {
		?>

		<input type="hidden" value="<?php echo esc_attr( $this->type ) ?>" name="<?php echo $this->setting_name( 'type' ); ?>">

		<table class="form-table">
			<tr>
				<th><label><?php _e( 'Text', 'clerina-booking' ) ?></label></th>
				<td><input type="text" value="{{data.text}}" name="<?php echo $this->setting_name( 'text' ); ?>" class="widefat field-label-input"></td>
			</tr>
			<tr>
				<th><label><?php _e( 'Type', 'clerina-booking' ) ?></label></th>
				<td>
					<select name="<?php echo $this->setting_name( 'tag' ); ?>">
						<option value="h1" {{'h1' == data.tag ? 'selected' : ''}}>H1</option>
						<option value="h2" {{'h2' == data.tag ? 'selected' : ''}}>H2</option>
						<option value="h3" {{'h3' == data.tag ? 'selected' : ''}}>H3</option>
						<option value="h4" {{'h4' == data.tag ? 'selected' : ''}}>H4</option>
						<option value="h5" {{'h5' == data.tag ? 'selected' : ''}}>H5</option>
					</select>
				</td>
			</tr>
			<tr>
				<th><label><?php _e( 'Class', 'clerina-booking' ) ?></label></th>
				<td><input type="text" value="{{data.class}}" name="<?php echo $this->setting_name( 'class' ); ?>" class="widefat"></td>
			</tr>
		</table>

		<?php
	}

	/**
	 * Render the field.
	 *
	 * @param array $settings
	 *
	 * @return string
	 */
	public function render( $settings = array() ) {
		$settings = wp_parse_args( $settings, $this->get_default_settings() );

		return sprintf(
			'<%1$s class="cpb-field cpb-header-field %2$s">%3$s</%1$s>',
			esc_attr( $settings['tag'] ),
			esc_attr( $settings['class'] ),
			esc_attr( $settings['text'] )
		);
	}
}
<?php
class Clerina_Booking_Admin_Menus {
	/**
	 * Hook in tabs.
	 */
	public function __construct() {
		// Add menus.
		add_action( 'admin_menu', array( $this, 'admin_menu' ) );
		// add_action( 'admin_menu', array( $this, 'settings_menu' ), 50 );
		// add_action( 'admin_menu', array( $this, 'statics_menu' ), 60 );
	}

	/**
	 * Add menu items.
	 */
	public function admin_menu() {
		global $menu;

		if ( current_user_can( 'manage_options' ) ) {
			$menu[48.1] = array( '', 'read', 'separator separator-clerina-booking', '', 'wp-menu-separator' ); // WPCS: override ok.
		}

		add_menu_page( __( 'Booking', 'clerina-booking' ), __( 'Booking', 'clerina-booking' ), 'manage_options', 'clerina-booking', null, 'dashicons-calendar-alt', 49 );
	}

	/**
	 * Add menu item of Settings.
	 */
	public function settings_menu() {
		$settings_page = add_submenu_page( 'clerina-booking', __( 'Booking Settings', 'clerina-booking' ), __( 'Settings', 'clerina-booking' ), 'manage_options', 'clerina-booking-settings', array( 'Clerina_Booking_Settings', 'settings_page' ) );

		add_action( 'load-' . $settings_page, array( 'Clerina_Booking_Settings', 'save' ) );
	}

	/**
	 * Add menu item of Status.
	 */
	public function statics_menu() {
		add_submenu_page( 'clerina-booking', __( 'Statics', 'clerina-booking' ), __( 'Statics', 'clerina-booking' ), 'manage_options', 'clerina-booking-statics', array( 'Clerina_Booking_Statics', 'statics_page' ) );
	}
}

return new Clerina_Booking_Admin_Menus();
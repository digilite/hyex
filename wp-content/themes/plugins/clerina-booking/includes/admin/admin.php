<?php
class Clerina_Booking_Admin {
	/**
	 * Constructor.
	 */
	public function __construct() {
		add_action( 'init', array( $this, 'includes' ) );
	}

	/**
	 * Include any classes we need within admin.
	 */
	public function includes() {
		include_once dirname( __FILE__ ) . '/menus.php';
		include_once dirname( __FILE__ ) . '/settings.php';
	}
}

return new Clerina_Booking_Admin();
<?php
class Clerina_Booking_Form {
	private $post_type = 'cpb_form';

	/**
	 * The single instance of the class
	 */
	protected static $instance = null;

	/**
	 * Initialize
	 */
	public static function init() {
		if ( null == self::$instance ) {
			self::$instance = new self();
		}

		return self::$instance;
	}

	/**
	 * Construction
	 */
	public function __construct() {
		$this->register_post_type();

		add_action( 'admin_enqueue_scripts', array( $this, 'admin_scripts' ) );

		add_action( 'edit_form_after_title', array( $this, 'shortcode_tag' ) );
		add_action( 'add_meta_boxes', array( $this, 'add_meta_boxes' ) );
		add_action( 'save_post', array( $this, 'save_meta_boxes' ), 10, 2 );

		add_action( 'admin_footer', array( $this, 'print_js_template' ) );
	}

	/**
	 * Register the order post type.
	 */
	public function register_post_type() {
		register_post_type( $this->post_type, array(
			'labels'              => array(
				'name'                  => esc_html__( 'Forms', 'clerina-booking' ),
				'singular_name'         => esc_html__( 'Order', 'clerina-booking' ),
				'menu_name'             => esc_html__( 'Forms', 'clerina-booking' ),
				'all_items'             => esc_html__( 'Forms', 'clerina-booking' ),
				'add_new'               => esc_html__( 'Add New', 'clerina-booking' ),
				'add_new_item'          => esc_html__( 'Add New', 'clerina-booking' ),
				'edit_item'             => esc_html__( 'Edit Form', 'clerina-booking' ),
				'new_item'              => esc_html__( 'New Form', 'clerina-booking' ),
				'view_item'             => esc_html__( 'View Form', 'clerina-booking' ),
				'search_items'          => esc_html__( 'Search forms', 'clerina-booking' ),
				'not_found'             => esc_html__( 'No forms found', 'clerina-booking' ),
				'not_found_in_trash'    => esc_html__( 'No forms found in Trash', 'clerina-booking' ),
				'filter_items_list'     => esc_html__( 'Filter forms list', 'clerina-booking' ),
				'items_list_navigation' => esc_html__( 'Form list navigation', 'clerina-booking' ),
				'items_list'            => esc_html__( 'Form list', 'clerina-booking' ),
			),
			'supports'            => array( 'title' ),
			'public'              => false,
			'show_ui'             => true,
			'show_in_menu'        => current_user_can( 'manage_options' ) ? 'clerina-booking' : true,
			'show_in_nav_menus'   => false,
			'rewrite'             => false,
			'capability_type'     => 'page',
			'publicly_queryable'  => false,
			'exclude_from_search' => true,
			'hierarchical'        => false,
			'has_archive'         => false,
			'query_var'           => is_admin(),
		) );
	}

	/**
	 * Enqueue scripts
	 *
	 * @param string $hook
	 */
	public function admin_scripts( $hook ) {
		if ( ! in_array( $hook, array( 'post.php', 'post-new.php' ) ) ) {
			return;
		}

		$screen = get_current_screen();

		if( is_object( $screen ) && $this->post_type == $screen->post_type ) {
			wp_enqueue_code_editor( array( 'type' => 'text/html' ) );
			wp_enqueue_style( 'clerina-booking-form-admin', CLERINA_BOOKING_URL . '/assets/css/form-admin.css' );
			wp_enqueue_script( 'clerina-booking-form-admin', CLERINA_BOOKING_URL . '/assets/js/form-admin.js', array( 'wp-util', 'jquery-ui-draggable', 'jquery-ui-sortable' ), null, true );
			wp_localize_script( 'clerina-booking-form-admin', 'cpbForm', array(
				'l10n' => array(
					'remove_message' => __( 'Are you sure want to remove this field?', 'clerina-booking' ),
				)
			) );
		}
	}

	/**
	 * Display the shortcode to copy.
	 */
	public function shortcode_tag() {
		$screen = get_current_screen();

		if( is_object( $screen ) && $this->post_type == $screen->post_type && isset( $_GET['post'] ) ) :
			?>
			<div class="shortcode-to-copy">
				<p class="description"><?php _e( 'Copy this shortcode and paste it into your post, page, or text widget content', 'clerina-booking' ); ?></p>
				<div class="shortcode wp-ui-highlight">
					[clerina_booking_form id="<?php echo esc_attr( $_GET['post'] ) ?>" title="<?php echo esc_attr( get_the_title( $_GET['post'] ) ) ?>"]
				</div>
			</div>
			<?php
		endif;
	}

	/**
	 * Add meta boxes.
	 */
	public function add_meta_boxes() {
		add_meta_box(
			'clerina-booking-form-data',
			__( 'Form Fields', 'clerina-booking' ),
			array( $this, 'fields_meta_box' ),
			$this->post_type,
			'advanced',
			'high'
		);
		add_meta_box(
			'clerina-booking-form-mail',
			__( 'Mail Settings', 'clerina-booking' ),
			array( $this, 'mail_meta_box' ),
			$this->post_type,
			'advanced',
			'high'
		);
		add_meta_box(
			'clerina-booking-form-message',
			__( 'Messages', 'clerina-booking' ),
			array( $this, 'messages_meta_box' ),
			$this->post_type,
			'advanced',
			'high'
		);
		add_meta_box(
			'clerina-booking-form-scripts',
			__( 'Form Scripts', 'clerina-booking' ),
			array( $this, 'scripts_meta_box' ),
			$this->post_type,
			'advanced',
			'high'
		);
	}

	/**
	 * Form fields meta box.
	 *
	 * @param object $post
	 */
	public function fields_meta_box( $post ) {
		wp_nonce_field( 'cpb_form_data', 'cpb_nonce' );

		$fields = get_post_meta( $post->ID, '_form_fields', true );

		include dirname( __FILE__ ) . '/views/meta-box-form-fields.php';
	}

	/**
	 * Form mail meta box.
	 *
	 * @param object $post
	 */
	public function mail_meta_box( $post ) {
		$settings = get_post_meta( $post->ID, '_form_mail_settings', true );
		$settings = wp_parse_args( $settings, array(
			'field_email' => '',
			'field_name' => '',
			'mail_subject' => __( 'You have just made an order on website [site_name]', 'clerina-booking' ),
			'mail_body' => __( 'Hello [customer_name],

You have just made an order on our website - [site_name]. This is your order.

[order_details]', 'clerina-booking' ),
		) );

		include dirname( __FILE__ ) . '/views/meta-box-form-mail.php';
	}

	/**
	 * Form messages meta box.
	 *
	 * @param object $post
	 */
	public function messages_meta_box( $post ) {
		$messages = get_post_meta( $post->ID, '_form_messages', true );
		$messages = wp_parse_args( $messages, array(
			'confirm' => __( '<h3>Confirm</h3><p>Please confirm the information bellow.</p>', 'clerina-booking' ),
			'success' => __( '<h3>Thank you</h3><p>Our staffs will contact you in a short time.</p>', 'clerina-booking' ),
			'failed' => __( 'Can not verify your data. Please try again.', 'clerina-booking' ),
			'error' => __( 'Unexpected error occurs. Please try again later.', 'clerina-booking' ),
		) );

		include dirname( __FILE__ ) . '/views/meta-box-form-messages.php';
	}

	/**
	 * Form scripts meta box.
	 *
	 * @param object $post
	 */
	public function scripts_meta_box( $post ) {
		$scripts = get_post_meta( $post->ID, '_form_scripts', true );
		$scripts = wp_parse_args( $scripts, array(
			'js'        => '',
			'css'       => '',
		) );

		$meta = get_post_meta( $post->ID, '_form_meta', true );
		$meta = wp_parse_args( $meta, array( 'class' => '' ) );

		include dirname( __FILE__ ) . '/views/meta-box-form-scripts.php';
	}

	/**
	 * Handles saving the meta box.
	 *
	 * @param int     $post_id Post ID.
	 * @param WP_Post $post    Post object.
	 * @return null
	 */
	public function save_meta_boxes( $post_id, $post ) {
		// Add nonce for security and authentication.
		$nonce_name   = isset( $_POST['cpb_nonce'] ) ? $_POST['cpb_nonce'] : '';
		$nonce_action = 'cpb_form_data';

		// Check if nonce is set.
		if ( ! isset( $nonce_name ) ) {
			return;
		}

		// Check if nonce is valid.
		if ( ! wp_verify_nonce( $nonce_name, $nonce_action ) ) {
			return;
		}

		// Check if user has permissions to save data.
		if ( ! current_user_can( 'edit_post', $post_id ) ) {
			return;
		}

		// Check if not an autosave.
		if ( wp_is_post_autosave( $post_id ) ) {
			return;
		}

		// Check if not a revision.
		if ( wp_is_post_revision( $post_id ) ) {
			return;
		}

		if ( isset( $_POST['_form_fields'] ) ) {
			update_post_meta( $post_id, '_form_fields', $_POST['_form_fields'] );
		} else {
			delete_post_meta( $post_id, '_form_fields' );
		}

		if ( isset( $_POST['_form_scripts'] ) ) {
			update_post_meta( $post_id, '_form_scripts', $_POST['_form_scripts'] );
		}

		if ( isset( $_POST['_form_meta'] ) ) {
			update_post_meta( $post_id, '_form_meta', $_POST['_form_meta'] );
		}

		if ( isset( $_POST['_form_mail_settings'] ) ) {
			update_post_meta( $post_id, '_form_mail_settings', $_POST['_form_mail_settings'] );
		}

		if ( isset( $_POST['_form_messages'] ) ) {
			update_post_meta( $post_id, '_form_messages', $_POST['_form_messages'] );
		}
	}

	/**
	 * Print js templates at footer.
	 */
	public function print_js_template() {
		$screen = get_current_screen();

		if( is_object( $screen ) && $this->post_type == $screen->post_type ) :
			foreach ( clerina_booking_get_fields() as $field ) {
				$classname = clerina_booking_get_field_classname( $field['type'] );

				if ( class_exists( $classname ) ) {
					$field_object = new $classname;

					$field_object->settings_template();
				}
			}
		endif;
	}
}

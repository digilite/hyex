<table class="form-table">
	<tr>
		<th><label for="code_editor_form_js"><?php _e( 'JavaScript', 'clerina-booking' ) ?></label></th>
		<td>
			<textarea id="code_editor_form_js" rows="5" name="_form_scripts[js]" class="widefat textarea"><?php echo wp_unslash( $scripts['js'] ); ?></textarea>
			<p class="description"><?php _e( 'Just write javascript, without the tag', 'clerina-booking' ) ?> <code>&lt;script&gt;</code>. <?php _e( 'The ID of this form is', 'clerina-booking' ) ?> <code>#clerina-booking-form-<?php echo $post->ID ?></code></p>
		</td>
	</tr>

	<tr>
		<th><label for="code_editor_form_css"><?php _e( 'CSS', 'clerina-booking' ) ?></label></th>
		<td>
			<textarea id="code_editor_form_css" rows="5" name="_form_scripts[css]" class="widefat textarea"><?php echo wp_unslash( $scripts['css'] ); ?></textarea>
			<p class="description"><?php _e( 'Just write CSS, without the tag', 'clerina-booking' ) ?> <code>&lt;style&gt;</code></p>
		</td>
	</tr>

	<tr>
		<th><label for="_form_meta_class"><?php _e( 'Form Class', 'clerina-booking' ) ?></label></th>
		<td>
			<input type="text" name="_form_meta[class]" value="<?php echo esc_attr( $meta['class'] ); ?>" class="widefat" id="_form_meta_class">
			<p class="description"><?php _e( 'Add custom CSS classes to the form.', 'clerina-booking' ) ?></p>
		</td>
	</tr>
</table>
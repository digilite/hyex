<table class="form-table">
	<tr>
		<th><label for="_form_mail_settings-field_email"><?php _e( 'Customer email field', 'clerina-booking' ) ?></label></th>
		<td>
			<input type="text" name="_form_mail_settings[field_email]" value="<?php echo esc_attr( $settings['field_email'] ) ?>" placeholder="your_email" class="widefat" id="_form_mail_settings-field_email">
			<p class="description"><?php _e( 'The customer email field name', 'clerina-booking' ) ?></p>
		</td>
	</tr>

	<tr>
		<th><label for="_form_mail_settings-field_name"><?php _e( 'Customer name field', 'clerina-booking' ) ?></label></th>
		<td>
			<input type="text" name="_form_mail_settings[field_name]" value="<?php echo esc_attr( $settings['field_name'] ) ?>" class="widefat" id="_form_mail_settings-field_name" placeholder="your_name">
			<p class="description"><?php _e( 'The customer name field name', 'clerina-booking' ) ?></p>
		</td>
	</tr>

	<tr>
		<th><label for="_form_mail_settings-email-subject"><?php _e( 'Email subject', 'clerina-booking' ) ?></label></th>
		<td>
			<input type="text" name="_form_mail_settings[mail_subject]" value="<?php echo esc_attr( $settings['mail_subject'] ) ?>" class="widefat" placeholder="<?php esc_attr_e( 'You have just made an new order on website [site_name]', 'clerina-booking' ) ?>" id="_form_mail_settings-email-subject">
			<p class="description"><?php _e( 'The subject of the notification email. You can use this mail-tag [site_name]', 'clerina-booking' ) ?></p>
		</td>
	</tr>

	<tr>
		<th><label for="_form_mail_settings-email-body"><?php _e( 'Email content', 'clerina-booking' ) ?></label></th>
		<td>
			<textarea name="_form_mail_settings[mail_body]" rows="7" class="widefat" id="_form_mail_settings-email-body"><?php echo esc_textarea( $settings['mail_body'] ) ?></textarea>
			<p class="description"><?php _e( 'The email content. You can use these mail-tags', 'clerina-booking' ) ?> [order_details], [site_name], [customer_name], [customer_email]</p>
		</td>
	</tr>
</table>
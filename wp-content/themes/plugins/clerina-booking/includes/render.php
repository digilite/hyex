<?php
class Clerina_Booking_Render {
	/**
	 * The single instance of the class
	 */
	protected static $instance = null;

	/**
	 * The array of field objects.
	 * @var array
	 */
	public $fields;

	/**
	 * The array of linked forms
	 * @var array
	 */
	protected $linked_forms;

	/**
	 * Initialize
	 */
	public static function instance() {
		if ( null == self::$instance ) {
			self::$instance = new self();
		}

		return self::$instance;
	}

	/**
	 * Render form fields.
	 *
	 * @param int  $form_id
	 * @param bool $nested
	 *
	 * @return string
	 */
	public function render_form( $form_id, $nested = false ) {
		if ( ! $form_id ) {
			return '';
		}

		$fields = get_post_meta( $form_id, '_form_fields', true );
		$scripts = get_post_meta( $form_id, '_form_scripts', true );
		$meta = get_post_meta( $form_id, '_form_meta', true );
		$messages = get_post_meta( $form_id, '_form_messages', true );

		if ( empty( $fields ) ) {
			return '';
		}

		$output = '';

		if ( ! $nested ) {
			$this->linked_forms = array();
		}

		foreach ( $fields as $field ) {
			$output .= $this->render_field( $field );
		}

		if ( empty( $output ) ) {
			return '';
		}

		if ( ! $nested ) {
			$dialog = '
				<div class="cpb-confirm-dialog cpb-dialog">
					<div class="cpb-backdrop"></div>
					<div class="cpb-dialog-content">
						<div class="cpb-dialog-summary">
							<div class="cpb-dialog-summary-header">' . $messages['confirm'] . '</div>
							<div class="cpb-dialog-summary-body"></div>
							<div class="cpb-dialog-summary-footer cpb-dialog-summary-actions">
								<button type="button" class="cpb-dialog-confirm-button">' . __( 'Confirm', 'clerina-booking' ) . '</button>
								<button type="button" class="cpb-dialog-cancel-button">' . __( 'Cancel', 'clerina-booking' ) . '</button>
							</div>
						</div>
						<div class="cpb-dialog-loading">' . __( 'Submitting your order...', 'clerina-booking' ) . '</div>
						<div class="cpb-dialog-success">' . $messages['success'] . '</div>
						<div class="cpb-dialog-failed">' . $messages['failed'] . '</div>
						<div class="cpb-dialog-error">' . $messages['error'] . '</div>
					</div>
				</div>';

			if ( ! empty( $this->linked_forms ) ) {
				foreach ( $this->linked_forms as $form ) {
					$output .= $this->render_form( $form, true );
				}
			}

			$action = '<input type="hidden" name="cpb_form" value="' . esc_attr( $form_id ) . '">';
			$action .= '<input type="hidden" name="cpb_nonce" value="' . wp_create_nonce( 'cpb_form_' . $form_id ) . '">';

			$report = isset( $_GET['order_created'] ) ? '<div class="cpb-form-message success">' . $messages['success'] . '</div>' : '';

			$output = '
				<div id="clerina-booking-form-' . esc_attr( $form_id ) . '" class="clerina-booking-form ' . ( empty( $meta['class'] ) ? '' : $meta['class'] ) . '">'
				          . $report . '
					<form method="post">' . $action . $output . '</form>' . $dialog . '
				</div>';
		} else {
			$output = '
				<div class="cpb-linked-form-' . esc_attr( $form_id ) . ' cpb-linked-form cpb-dialog" data-form_id="' . esc_attr( $form_id ) . '">
					<div class="cpb-backdrop"></div>
					<div class="cpb-dialog-content">
						<div class="cpb-dialog-fields">' . $output . '</div>
						<div class="cpb-dialog-loading">' . __( 'Submitting your order...', 'clerina-booking' ) . '</div>
						<div class="cpb-dialog-success">' . $messages['success'] . '</div>
						<div class="cpb-dialog-failed">' . $messages['failed'] . '</div>
						<div class="cpb-dialog-error">' . $messages['error'] . '</div>
					</div>
				</div>';
		}

		if ( ! empty( $scripts['js'] ) ) {
			$output = '<script type="text/javascript">' . wp_unslash( $scripts['js'] ) . '</script>' . $output;
		}

		if ( ! empty( $scripts['css'] ) ) {
			$output = '<style type="text/css">' . wp_unslash( $scripts['css'] ) . '</style>' . $output;
		}

		return $output;
	}

	/**
	 * Render a field.
	 *
	 * @param array $field
	 *
	 * @return string
	 */
	public function render_field( $field ) {
		if ( empty( $field['type'] ) ) {
			return '';
		}

		$type = $field['type'];
		$field_obj = $this->field( $type );

		if ( ! $field_obj ) {
			return '';
		}

		if ( 'button' == $type && 'open_new_form' == $field['behavior'] ) {
			$this->linked_forms[ $field['linked'] ] = $field['linked'];
		} elseif ( 'pricing_table' == $type && 'open_new_form' == $field['button_behavior'] ) {
			$this->linked_forms[ $field['button_linked'] ] = $field['button_linked'];
		}

		return $field_obj->render( $field );
	}

	/**
	 * Check if a field object is created.
	 *
	 * @param  string  $type
	 * @return boolean
	 */
	protected function has_field_object( $type ) {
		return ! empty( $this->fields[ $type ] );
	}

	/**
	 * Get field object.
	 *
	 * @param  string $type
	 * @return object
	 */
	public function field( $type ) {
		if ( $this->has_field_object( $type ) ) {
			return $this->fields[ $type ];
		} else {
			$field_class = clerina_booking_get_field_classname( $type );

			if ( class_exists( $field_class ) ) {
				$this->fields[ $type ] = new $field_class;
				return $this->fields[ $type ];
			}
		}

		return false;
	}
}
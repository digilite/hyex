<?php
abstract class Clerina_Booking_Field {
	public $type = 'text';

	protected $meta_key = '_form_fields';

	/**
	 * Template for field settings.
	 */
	public function settings_template() {
		?>

		<script type="text/html" id="tmpl-cpb-field-<?php echo $this->type ?>-settings">
			<?php $this->settings_template_content(); ?>
		</script>

		<?php
	}

	protected function settings_template_content() {}

	/**
	 * Get field setting value.
	 * @param  string $name
	 */
	public function setting_value( $name ) {

	}

	/**
	 * Get the input name attribute.
	 * 
	 * @param  string $name
	 * @return string
	 */
	public function setting_name( $name ) {
		return $this->meta_key . '[{{data.index}}][' . $name . ']';
	}

	/**
	 * Get the input name attribute.
	 * 
	 * @param  string $name
	 * @return string
	 */
	public function setting_id( $name ) {
		return $this->meta_key . '-{{data.index}}-' . $name;
	}

	/**
	 * Get the input name attribute.
	 * 
	 * @param  string $name
	 * @return string
	 */
	public function setting_checked( $name, $default = '1' ) {
		return '{{ data.' . $name . ' == ' . $default . ' ? "checked" : "" }}';
	}

	/**
	 * Render the field.
	 *
	 * @param array $settings
	 *
	 * @return string
	 */
	public function render( $settings = array() ) {
		$settings = wp_parse_args( $settings, $this->get_default_settings() );
		$label = empty( $settings['label'] ) ? '' : sprintf( '<span class="cpb-input-label">%s</span>', esc_html( $settings['label'] ) );

		return sprintf(
			'<div class="cpb-field cpb-%s-field %s" aria-label="%s"><label>%s<input type="%s" name="%s" value="%s" class="cpb-input" placeholder="%s" %s></label></div>',
			esc_attr( $this->type ),
			esc_attr( $settings['class'] ),
			esc_attr( $this->get_aria_label( $settings ) ),
			$label,
			esc_attr( $this->type ),
			esc_attr( $settings['name'] ),
			esc_attr( $settings['value'] ),
			esc_attr( $settings['placeholder'] ),
			! empty( $settings['required'] ) ? 'required' : ''
		);
	}

	/**
	 * Get default settings.
	 *
	 * @return array|bool
	 */
	public function get_default_settings() {
		$fields = clerina_booking_get_fields();

		foreach ( $fields as $field ) {
			if ( $this->type == $field['type'] ) {
				return $field['settings'];
			}
		}

		return false;
	}

	/**
	 * Get the label.
	 * 
	 * @param  array $settings
	 * @return string
	 */
	protected function get_aria_label( $settings ) {
		if ( ! empty( $settings['admin_label'] ) ) {
			return $settings['admin_label'];
		} elseif ( ! empty( $settings['label'] ) ) {
			return $settings['label'];
		} elseif ( ! empty( $settings['text'] ) ) {
			return $settings['text'];
		}

		return '';
	}
}
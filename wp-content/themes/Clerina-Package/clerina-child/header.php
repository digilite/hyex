<?php

/**

 * The header for our theme

 *

 * This is the template that displays all of the <head> section and everything up until <div id="content">

 *

 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials

 *

 * @package 'Clerina'

 */



?>

<!doctype html>

<html <?php language_attributes(); ?>>

<head>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-PT5DG5Z');</script>
<!-- End Google Tag Manager -->
    
	<meta charset="<?php bloginfo( 'charset' ); ?>">

	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="profile" href="https://gmpg.org/xfn/11">

	<script defer href="../clerina-child/functions.js"></script>

<!-- Start of HubSpot Embed Code --> <script type="text/javascript" id="hs-script-loader" async defer src="//js.hs-scripts.com/7206479.js"></script> <!-- End of HubSpot Embed Code -->

	<?php wp_head(); ?>
	
	<meta name="google-site-verification" content="REGtrXtsiu70XC140HYOKiqSL0CGsWoCgj0vnGLlf7o" />

</head>



<body <?php body_class(); ?>>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PT5DG5Z"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->


	<!-- SVG elements -->

	<svg style="display: none;">

		<?php get_template_part("template-svg-symbols"); ?>

	</svg>

<div id="page" class="site">

	<?php do_action('clerina_before_header'); ?>

    <header id="site-header" class="site-header">

		<?php do_action('clerina_header'); ?>

    </header>

    <!-- #masthead -->

	<?php do_action('clerina_after_header'); ?>



    <div id="content" class="site-content">

		<?php do_action( 'clerina_after_site_content_open' ); ?>
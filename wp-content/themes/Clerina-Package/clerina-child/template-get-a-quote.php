<?php
/**
 * Template Name: Get a Quote
 */

get_header();
?>
	<div id="primary" class="content-area">
		<?php if(have_posts()): while ( have_posts() ) : the_post(); ?>
			<div><?php the_content(); ?></div>
		<?php endwhile; endif;?>
		<!-- Quotient Lead Form -->
		<div id="quotient-lead-form"></div>
		<script>
		    (function (w, s, o, g, i, a, m) {
		        a = s.createElement(o);
		        m = s.getElementsByTagName(o)[0];
		        a.async = 1; a.src = g; w._qLead = function(){};
		        m.parentNode.insertBefore(a, m);
		    })(window, document, 'script', 'https://www.quotientapp.com/e/30060-dbe23f18be0345dda37cab119dc5ddb4/form/embed.js');
		    _qLead.id = 'quotient-lead-form';
		</script>
		<!-- End Quotient Lead Form -->

	</div>

<?php
get_footer();
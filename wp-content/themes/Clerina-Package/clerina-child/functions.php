<?php
add_action( 'wp_enqueue_scripts', 'clerina_child_enqueue_scripts', 20 );

function clerina_child_enqueue_scripts() {
	wp_enqueue_style( 'clerina-child-style', get_stylesheet_uri() );
	if ( is_rtl() ) {
		wp_enqueue_style( 'clerina-rtl', get_template_directory_uri() . '/rtl.css', array(), '20181024' );
	}
}

function add_file_types_to_uploads($file_types){
    $new_filetypes = array();
    $new_filetypes['svg'] = 'image/svg+xml';
    $file_types = array_merge($file_types, $new_filetypes );
    return $file_types;
}
add_action('upload_mimes', 'add_file_types_to_uploads');
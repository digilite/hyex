<?php

/**
 * Plugin Name: Unific 
 * Plugin URI: http://www.unific.com/
 * Description: Unific integrates Woocommerce with Marketing Platforms like HubSpot, Infusionsoft and ActiveCampaign.
 * Version: 1.0.6
 * Author: Unific
 * Author URI: http://www.unific.com/
 * Requires at least: 4.1
 * Tested up to: 4.1
 * Woocommerce version at least: 2.4
 *
 * Text Domain: unific
 * Domain Path: /i18n/languages/
 *
 * @package unific
 * @category Core
 * @author Unific
 */


if ( ! defined( 'ABSPATH' ) ) { 
    exit; // Exit if accessed directly
}


/**
 * Check if WooCommerce is active
 **/
if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {

	/****************** Newly added for WC abandoned cart  ****************************/

	register_activation_hook ( __FILE__, 'unific_activate');
	//fire de-activate event after de-activate the plugin	
	register_deactivation_hook( __FILE__, 'unific_deactivate' );
	//fire unistall event after deletion of plugin
	register_uninstall_hook( __FILE__, 'unific_uninstall');

	// Put plugin activation code here
	if(!function_exists('unific_activate')){
		function unific_activate(){
			global $wpdb; 
			$wcap_collate = '';
			if ( $wpdb->has_cap( 'collation' ) ) {
				$wcap_collate = $wpdb->get_charset_collate();
			}

			$table_name = $wpdb->prefix . "usermeta_unific";
			$index1 = "ix_{$table_name}_entityid_entitytype";
			$index2 = "ix_{$table_name}_entityid_entitytype_metakey";
			$sql = "CREATE TABLE IF NOT EXISTS {$table_name} (

				`unific_meta_id` int(11) NOT NULL AUTO_INCREMENT,

				`entity_id` varchar(20) COLLATE utf8_unicode_ci NOT NULL,

				`entity_type` varchar(45) COLLATE utf8_unicode_ci NOT NULL,

				`created` datetime NULL,

				`modified` datetime NULL,

				`meta_key` varchar(100) COLLATE utf8_unicode_ci NOT NULL,

				`meta_value` mediumtext COLLATE utf8_unicode_ci NOT NULL,

				PRIMARY KEY (`unific_meta_id`)

					) {$wcap_collate} AUTO_INCREMENT=1 ";

			require_once ( ABSPATH . 'wp-admin/includes/upgrade.php' );
			dbDelta( $sql );

			//check and create index1
			$sql_check_index1 = "SELECT * FROM INFORMATION_SCHEMA.STATISTICS WHERE table_schema=DATABASE() AND table_name = %s AND index_name = %s";
			$sql_check_index1_result = $wpdb->get_results($wpdb->prepare($sql_check_index1, $table_name, $index1));
			if(count( $sql_check_index1_result ) == 0){
				$sql_index1 = "CREATE INDEX {$index1} ON {$table_name} (`entity_id`,`entity_type`)";
				$wpdb->get_results( $sql_index1 );
			}

			//check and create index2
			$sql_check_index2 = "SELECT * FROM INFORMATION_SCHEMA.STATISTICS WHERE table_schema=DATABASE() AND table_name = %s AND index_name = %s";
			$sql_check_index2_result = $wpdb->get_results($wpdb->prepare($sql_check_index2, $table_name, $index2));
			if(count( $sql_check_index2_result ) == 0){
				$sql_index2 = "CREATE INDEX {$index2} ON {$table_name} (`entity_id`,`entity_type`,`meta_key`)";
				$wpdb->get_results( $sql_index2 );
			}
		}
	}

	// Put plugin de-activate code here
	if(!function_exists('unific_deactivate')){
		function unific_deactivate(){
			
		}
	}

	// Put plugin uninstall code here
	if(!function_exists('unific_uninstall')){
		function unific_uninstall(){
			global $wpdb;

			$table_name = $wpdb->prefix . "usermeta_unific";

			$sql = "DROP TABLE IF EXISTS " . $table_name ;

			require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
			$wpdb->get_results( $sql );
		}
	}

	/****************************** End here  *************************************/

	if(!function_exists('unific_save_extra_checkout_fields')){
		function unific_save_extra_checkout_fields($order_id){
			// don't forget appropriate sanitization if you are using a different field type
			$unific_affiliate_id = !empty($_COOKIE["unific_affiliate_id"]) ? trim($_COOKIE["unific_affiliate_id"]) : null;
			setcookie("unific_affiliate_id", "",time()-(60*60),"/");
			if(!empty($unific_affiliate_id)) {
				update_post_meta($order_id,'affiliate',sanitize_text_field($unific_affiliate_id));
			}

			// don't forget appropriate sanitization if you are using a different field type
			$hubspotutk = !empty($_COOKIE["hubspotutk"]) ? trim($_COOKIE["hubspotutk"]) : null;
			if(!empty($hubspotutk)) {
				update_post_meta($order_id,'hubspotutk',sanitize_text_field($hubspotutk));
			}

			/****************** Newly added for WC abandoned cart  ****************************/
			// don't forget appropriate sanitization if you are using a different field type
			$unific_cart_hash = !empty($_COOKIE["unific_cart_hash"]) ? trim($_COOKIE["unific_cart_hash"]) : null;
			setcookie("unific_cart_hash", "",time()-(60*60),"/");
			if(!empty($unific_cart_hash)) {
				update_post_meta($order_id,'unific_cart_hash',sanitize_text_field($unific_cart_hash));
			}
			/****************************** End here  *************************************/
		}
	}

	/****************************** Newly added for WC abandoned cart  *************************************/
	if(!function_exists('unific_checkout_create')){
		function unific_checkout_create($unific_cart_hash){
			//add code here
			//return WC_API_Unific_Checkout::get_checkout($unific_cart_hash);

			return $unific_cart_hash;
		}
	}

	if(!function_exists('unific_checkout_update')){
		function unific_checkout_update($unific_cart_hash){
			//add code here
			//return WC_API_Unific_Checkout::get_checkout($unific_cart_hash);
			
			//return array("checkout" => $unific_cart_hash);
			return $unific_cart_hash;
		}
	}

	if(!function_exists('load_user_data_js')){
		function load_user_data_js(){
			$unific_cart_hash = !empty(@$_COOKIE["unific_cart_hash"]) ? trim(@$_COOKIE["unific_cart_hash"]) : null;
			$email = null;
			$billing_first_name = null;
			$billing_last_name = null;

			if(!empty($unific_cart_hash)){
				global $wpdb;

				$query = "SELECT meta_key,meta_value FROM `".$wpdb->prefix."usermeta_unific` WHERE entity_id = %d && entity_type = 'checkout' && (meta_key = 'email' || meta_key = 'first_name' || meta_key = 'last_name')";

				$results = $wpdb->get_results($wpdb->prepare($query,$unific_cart_hash));

				if(!empty($results)){
					foreach($results as $result){
						if(!empty($result->meta_key) && !empty($result->meta_value)){
							switch ($result->meta_key) {
								case 'email':
									$email = $result->meta_value;
									break;
								case 'first_name':
									$billing_first_name = $result->meta_value;
									break;
								case 'last_name':
									$billing_last_name = $result->meta_value;
									break;
							}
						}
					}
				}elseif(is_user_logged_in()){
					$current_user = wp_get_current_user();
					$email = (!empty($current_user->user_email)) ? $current_user->user_email : null;
					$billing_first_name = (!empty($current_user->user_firstname)) ? $current_user->user_firstname : null;
					$billing_last_name = (!empty($current_user->user_lastname)) ? $current_user->user_lastname : null;
				}
			}

			wp_register_script( 'unific', plugin_dir_url( __FILE__ ) . 'js/unific.js', array('jquery'), false, false );
			wp_enqueue_script( 'unific');
			wp_localize_script( 'unific', 'unific_object',
				array(
					'nonce'=> wp_create_nonce( "ahsdsad66a7sas7" ),
					'email' => $email,
					'billing_first_name' => $billing_first_name,
					'billing_last_name' => $billing_last_name,
					'admin_ajax_url' => get_admin_url().'admin-ajax.php'
				)
			);
		}
	}

	if(!function_exists('startsWith')){
		function startsWith($haystack, $needle){
			$length = strlen($needle);
			return (substr($haystack, 0, $length) === $needle);
		}
	}

	if(!function_exists('save_user_callback')){
		function save_user_callback() {		
			//if ( ! is_user_logged_in() ) {            
				global $woocommerce;   

				$cart = array(); 
				$billing_address = array();
				$shipping_address = array();
				if(!empty($_POST['billing_email'])){
					$email = strtolower(trim($_POST['billing_email']));
				}else{
					$email = null;
				}
				if (!empty($_POST['billing_first_name'])){
					$billing_first_name = trim($_POST['billing_first_name']);
				} else {
					$billing_first_name = '';
				}
				if (!empty($_POST['billing_last_name'])){
					$billing_last_name = trim($_POST['billing_last_name']);
				} else {
					$billing_last_name = '';
				}
				if ( function_exists('WC') ) {
					$cart['cart'] = WC()->session->cart;
				} else {
					$cart['cart'] = $woocommerce->session->cart;
				}

				foreach($_POST as $key => $val){
					if(startsWith($key, "billing_")){
						$billing_address[str_replace("billing_","",$key)] = $val;
					}
					if(startsWith($key, "shipping_")){
						$shipping_address[str_replace("shipping_","",$key)] = $val;
					}
				}

				// If a record is present in the users table for the same email id
				if(!empty($email) && !empty($billing_first_name) && !empty($billing_last_name) && !empty($cart['cart'])){
					$unific_cart_hash = !empty($_COOKIE["unific_cart_hash"]) ? trim($_COOKIE["unific_cart_hash"]) : null;
					if(empty($unific_cart_hash)){
						//$unific_cart_hash = uniqid();
						$unific_cart_hash = (int)(microtime(true) * 1000);
						setcookie("unific_cart_hash",$unific_cart_hash ,time()+(60*60),"/");
					}
					if(!empty($unific_cart_hash)){
						saveCheckoutData($unific_cart_hash,$email,$billing_first_name,$billing_last_name,$billing_address, $shipping_address, $cart['cart']);
					}
				}
			//}
		}
	}	

	if(!function_exists('atleast_wc_version')){
		function atleast_wc_version( $version = '3.5.0' ) {
			if ( class_exists( 'WooCommerce' ) ) {
				global $woocommerce;
				if ( version_compare( $woocommerce->version, $version, ">=" ) ) {
					return true;
				}
			}
			return false;
		}
	}

	if(!function_exists('saveCheckoutData')){
		function saveCheckoutData($unific_cart_hash,$email,$billing_first_name,$billing_last_name,$billing_address,$shipping_address,$cart,$user_id = 0){
			global $wpdb;

			if(!empty($email) && !empty($billing_first_name) && !empty($billing_last_name) && !empty($cart) && !empty($unific_cart_hash)){
				$cart_data = serialize($cart);
				$email_query = "SELECT unific_meta_id,meta_key,meta_value FROM `".$wpdb->prefix."usermeta_unific` WHERE entity_id = %d && entity_type = 'checkout' && meta_key = 'email'";
				$first_name_query = "SELECT unific_meta_id,meta_key,meta_value FROM `".$wpdb->prefix."usermeta_unific` WHERE entity_id = %d && entity_type = 'checkout' && meta_key = 'first_name'";
				$last_name_query = "SELECT unific_meta_id,meta_key,meta_value FROM `".$wpdb->prefix."usermeta_unific` WHERE entity_id = %d && entity_type = 'checkout' && meta_key = 'last_name'";
				$cart_query = "SELECT unific_meta_id,meta_key,meta_value FROM `".$wpdb->prefix."usermeta_unific` WHERE entity_id = %d && entity_type = 'checkout' && meta_key = 'unific_persistent_cart'";
				$billing_address_query = "SELECT unific_meta_id,meta_key,meta_value FROM `".$wpdb->prefix."usermeta_unific` WHERE entity_id = %d && entity_type = 'checkout' && meta_key = 'billing_address'";
				$shipping_address_query = "SELECT unific_meta_id,meta_key,meta_value FROM `".$wpdb->prefix."usermeta_unific` WHERE entity_id = %d && entity_type = 'checkout' && meta_key = 'shipping_address'";
				if(!empty($user_id)){
					$user_id_query = "SELECT unific_meta_id,meta_key,meta_value FROM `".$wpdb->prefix."usermeta_unific` WHERE entity_id = %d && entity_type = 'checkout' && meta_key = 'user_id'";
				}

				$email_result = $wpdb->get_row($wpdb->prepare($email_query,$unific_cart_hash));
				unset($email_query);
				$first_name_result = $wpdb->get_row($wpdb->prepare($first_name_query,$unific_cart_hash));
				unset($first_name_query);
				$last_name_result = $wpdb->get_row($wpdb->prepare($last_name_query,$unific_cart_hash));
				unset($last_name_query);
				$cart_result = $wpdb->get_row($wpdb->prepare($cart_query,$unific_cart_hash));
				unset($cart_query);

				$billing_address_result = $wpdb->get_row($wpdb->prepare($billing_address_query,$unific_cart_hash));
				unset($billing_address_query);
				$shipping_address_result = $wpdb->get_row($wpdb->prepare($shipping_address_query,$unific_cart_hash));
				unset($shipping_address_query);


				if(!empty($user_id_query)){
					$user_id_result = $wpdb->get_row($wpdb->prepare($user_id_query,$unific_cart_hash));
					unset($user_id_query);
				}

				$current_time = current_time( 'mysql' );

				$is_update = false;

				$email_query = '';
				$first_name_query = '';
				$last_name_query = '';
				$cart_query = '';
				$billing_address_query = '';
				$shipping_address_query = '';
				$user_id_query = '';
				if(!empty($user_id)){
					if(!empty($user_id_result) && !empty($user_id_result->unific_meta_id)){
						if($user_id_result->meta_value !== $user_id){
							$user_id_query = "UPDATE `" . $wpdb->prefix . "usermeta_unific` SET meta_value = '".$user_id."', modified = '".$current_time."' WHERE unific_meta_id = '".$user_id_result->unific_meta_id."' ";
						}
					}else{
						$user_id_query = "INSERT INTO `".$wpdb->prefix . "usermeta_unific`( entity_id, entity_type, meta_key, meta_value, created, modified) VALUES ( '".$unific_cart_hash."', 'checkout', 'user_id', '".$user_id."', '".$current_time."', '".$current_time."' )";
					}
					if(!empty($user_id_query)){
						$wpdb->query( $user_id_query );
					}
				}

				if(!empty($email_result) && !empty($email_result->unific_meta_id)){
					if($email_result->meta_value !== $email){
						$email_query = "UPDATE `" . $wpdb->prefix . "usermeta_unific` SET meta_value = '".$email."', modified = '".$current_time."' WHERE unific_meta_id = '".$email_result->unific_meta_id."' ";
						$is_update = true;
					}
				}else{
					$email_query = "INSERT INTO `".$wpdb->prefix . "usermeta_unific`( entity_id, entity_type, meta_key, meta_value, created, modified) VALUES ( '".$unific_cart_hash."', 'checkout', 'email', '".$email."', '".$current_time."', '".$current_time."' )";
				}
				if(!empty($email_query)){
					$wpdb->query( $email_query );
				}

				if(!empty($first_name_result) && !empty($first_name_result->unific_meta_id)){
					if($first_name_result->meta_value !== $billing_first_name){
						$first_name_query = "UPDATE `" . $wpdb->prefix . "usermeta_unific` SET meta_value = '".$billing_first_name."', modified = '".$current_time."' WHERE unific_meta_id = '".$first_name_result->unific_meta_id."' ";
						$is_update = true;
					}
				}else{
					$first_name_query = "INSERT INTO `".$wpdb->prefix . "usermeta_unific`( entity_id, entity_type, meta_key, meta_value, created, modified) VALUES ( '".$unific_cart_hash."', 'checkout', 'first_name', '".$billing_first_name."', '".$current_time."', '".$current_time."' )";
				}
				if(!empty($first_name_query)){
					$wpdb->query( $first_name_query );
				}

				if(!empty($last_name_result) && !empty($last_name_result->unific_meta_id)){
					if($last_name_result->meta_value !== $billing_last_name){
						$last_name_query = "UPDATE `" . $wpdb->prefix . "usermeta_unific` SET meta_value = '".$billing_last_name."', modified = '".$current_time."' WHERE unific_meta_id = '".$last_name_result->unific_meta_id."' ";
						$is_update = true;
					}
				}else{
					$last_name_query = "INSERT INTO `".$wpdb->prefix . "usermeta_unific`( entity_id, entity_type, meta_key, meta_value, created, modified) VALUES ( '".$unific_cart_hash."', 'checkout', 'last_name', '".$billing_last_name."', '".$current_time."', '".$current_time."' )";
				}
				if(!empty($last_name_query)){
					$wpdb->query( $last_name_query );
				}

				if(!empty($cart_result) && !empty($cart_result->unific_meta_id)){
					if($cart_result->meta_value !== $cart_data){
						$cart_query = "UPDATE `" . $wpdb->prefix . "usermeta_unific` SET meta_value = '".$cart_data."', modified = '".$current_time."' WHERE unific_meta_id = '".$cart_result->unific_meta_id."' ";
						$is_update = true;
					}
				}else{
					$cart_query = "INSERT INTO `".$wpdb->prefix . "usermeta_unific`( entity_id, entity_type, meta_key, meta_value, created, modified) VALUES ( '".$unific_cart_hash."', 'checkout', 'unific_persistent_cart', '".$cart_data."', '".$current_time."', '".$current_time."' )";
				}
				if(!empty($cart_query)){
					$wpdb->query( $cart_query );
				}

				if(!empty($billing_address_result) && !empty($billing_address_result->unific_meta_id)){
					if($billing_address_result->meta_value !== serialize($billing_address)){
						$billing_address_query = "UPDATE `" . $wpdb->prefix . "usermeta_unific` SET meta_value = '".serialize($billing_address)."', modified = '".$current_time."' WHERE unific_meta_id = '".$billing_address_result->unific_meta_id."' ";
						$is_update = true;
					}
				}else{
					$billing_address_query = "INSERT INTO `".$wpdb->prefix . "usermeta_unific`( entity_id, entity_type, meta_key, meta_value, created, modified) VALUES ( '".$unific_cart_hash."', 'checkout', 'billing_address', '".serialize($billing_address)."', '".$current_time."', '".$current_time."' )";
				}
				if(!empty($billing_address_query)){
					$wpdb->query( $billing_address_query );
				}

				if(!empty($shipping_address_result) && !empty($shipping_address_result->unific_meta_id)){
					if($shipping_address_result->meta_value !== serialize($shipping_address)){
						$shipping_address_query = "UPDATE `" . $wpdb->prefix . "usermeta_unific` SET meta_value = '".serialize($shipping_address)."', modified = '".$current_time."' WHERE unific_meta_id = '".$shipping_address_result->unific_meta_id."' ";
						$is_update = true;
					}
				}else{
					$shipping_address_query = "INSERT INTO `".$wpdb->prefix . "usermeta_unific`( entity_id, entity_type, meta_key, meta_value, created, modified) VALUES ( '".$unific_cart_hash."', 'checkout', 'shipping_address', '".serialize($shipping_address)."', '".$current_time."', '".$current_time."' )";
				}
				if(!empty($shipping_address_query)){
					$wpdb->query( $shipping_address_query );
				}

				//send customer update webhook for logged in user for checkout create
				if((!$is_update) && !empty($user_id)){
					do_action( 'woocommerce_update_customer', $user_id);
				}

				if($is_update) {
					trigger_webhook($unific_cart_hash, "woocommerce_unific_checkout_update_action");
				}else{
					trigger_webhook($unific_cart_hash, "woocommerce_unific_checkout_create_action");
				}
			}
		}
	}

	if(!function_exists('trigger_webhook')){
		function trigger_webhook( $unific_cart_hash, $action ) {
			global $wpdb;
			$delivery_urls = $wpdb->get_results( "SELECT distinct delivery_url, secret FROM {$wpdb->prefix}wc_webhooks where status = 'active' and delivery_url like '%unific.com%'" );
			if(!empty($unific_cart_hash) && !empty($action) && $delivery_urls) {

				foreach ($delivery_urls as $delivery_url_object) {

					if(isset($delivery_url_object->delivery_url) && !empty($delivery_url_object->delivery_url)) {

						$delivery_url = $delivery_url_object->delivery_url;
						$secret = $delivery_url_object->secret;

						$checkout_details = unific_get_checkout( $unific_cart_hash );

						if ( $checkout_details ) {
							$checkout_details = array("action" => $action, "arg" => $checkout_details);
							$trigger_details = json_encode($checkout_details);
							$headers = array(
									"Content-Type" => 'application/json',
									"X-WC-Webhook-Signature" => generate_signature_unific($trigger_details, $secret),
									"X-WC-Webhook-Event" => $action,
									"X-WC-Webhook-Topic" => "action.".$action,
									"X-WC-Webhook-Resource" => "action",
									"X-WC-Webhook-ID" => 0
								);

							wp_remote_post(
								$delivery_url,
								array(
									'body'        => $trigger_details,
									'timeout'     => '45',
									'redirection' => '5',
									'httpversion' => '1.0',
									'blocking'    => true,
									'headers'     => $headers,
									'cookies'     => array(),
								)
							);
						}
					}
				}
			}
		}
	}

	if(!function_exists('generate_signature_unific')){
		function generate_signature_unific( $payload, $secret ) {
			$hash_algo = apply_filters( 'woocommerce_webhook_hash_algorithm', 'sha256', $payload, 0 );

			return base64_encode( hash_hmac( $hash_algo, $payload, wp_specialchars_decode( $secret, ENT_QUOTES ), true ) );
		}
	}

	if(!function_exists('get_latest_date')){
		 function get_latest_date($current,$fromSource){
			if(!empty($fromSource)){
				if(empty($current)){
					$current = $fromSource;
				}else{
					if($fromSource > $current){
						$current = $fromSource;
					}
				}
			}
			return $current;
		}
	}

	if(!function_exists('unific_get_checkout')){
		function unific_get_checkout($id) {
			global $wpdb;
			global $woocommerce;
			$cart_url = wc_get_cart_url();

			$ret = array();
			$customer_data = array();
			$created = $modified = null;
			$cart_url = $cart_url.'?unific_cart_hash='.$id;

			$query = "SELECT meta_key,meta_value,created,modified FROM `".$wpdb->prefix."usermeta_unific` WHERE entity_id = %d && entity_type = 'checkout'";

			$results = $wpdb->get_results($wpdb->prepare($query,$id));

			if(!empty($results)){
				foreach($results as $result){
					if(!empty($result->meta_key) && !empty($result->meta_value)){
						switch ($result->meta_key) {
							case 'email':
								$customer_data['email'] = $result->meta_value;
								if(!empty($result->created)){
									$created = get_latest_date($created,$result->created);
								}
								if(!empty($result->modified)){
									$modified = get_latest_date($created,$result->modified);
								}
								break;
							case 'first_name':
								$customer_data['first_name'] = $result->meta_value;
								if(!empty($result->created)){
									$created = get_latest_date($created,$result->created);
								}
								if(!empty($result->modified)){
									$modified = get_latest_date($created,$result->modified);
								}
								break;
							case 'last_name':
								$customer_data['last_name'] = $result->meta_value;
								if(!empty($result->created)){
									$created = get_latest_date($created,$result->created);
								}
								if(!empty($result->modified)){
									$modified = get_latest_date($created,$result->modified);
								}
								break;
							case 'user_id':
								$customer_data['id'] = $result->meta_value;
								break;
							case 'billing_address':
								$customer_data['billing_address'] = (!empty($result->meta_value)) ? custom_unserialize($result->meta_value) : array();
								break;
							case 'shipping_address':
								$customer_data['shipping_address'] = (!empty($result->meta_value)) ? custom_unserialize($result->meta_value) : array();
								if(!empty($customer_data['shipping_address']['method'])){
									unset($customer_data['shipping_address']['method']);
								}
								break;
							case 'unific_persistent_cart':
								$cart = custom_unserialize($result->meta_value);
								if(!empty($cart)){
									$ret['id'] = $id;
									$ret['cart_url'] = $cart_url;
									$ret['cart'] = $cart;
								}
								if(!empty($result->created)){
									$created = get_latest_date($created,$result->created);
								}
								if(!empty($result->modified)){
									$modified = get_latest_date($created,$result->modified);
								}
								break;
						}
					}
				}
			}

			if(!empty($ret['cart'])){
				if(!empty($created)){
					$ret['created_at'] = convert_to_datetime( $created );
				}
				if(!empty($modified)){
					$ret['updated_at'] = convert_to_datetime( $modified );
				}
				if(!empty($customer_data)){
					$ret['customer'] = $customer_data;
				}
			}

		        return $ret;
		}
	}
	
	if(!function_exists('convert_to_datetime')){
		function convert_to_datetime( $timestamp, $convert_to_utc = false, $convert_to_gmt = false ) {
			if ( $convert_to_gmt ) {
				if ( is_numeric( $timestamp ) ) {
					$timestamp = date( 'Y-m-d H:i:s', $timestamp );
				}

				$timestamp = get_gmt_from_date( $timestamp );
			}

			if ( $convert_to_utc ) {
				$timezone = new DateTimeZone( wc_timezone_string() );
			} else {
				$timezone = new DateTimeZone( 'UTC' );
			}

			try {

				if ( is_numeric( $timestamp ) ) {
					$date = new DateTime( "@{$timestamp}" );
				} else {
					$date = new DateTime( $timestamp, $timezone );
				}

				// convert to UTC by adjusting the time based on the offset of the site's timezone
				if ( $convert_to_utc ) {
					$date->modify( -1 * $date->getOffset() . ' seconds' );
				}
			} catch ( Exception $e ) {

				$date = new DateTime( '@0' );
			}

			return $date->format( 'Y-m-d\TH:i:s\Z' );
		}
	}

	if(!function_exists('unific_save_custom_persistant_cart')){
		function unific_save_custom_persistant_cart(){
			global $wpdb;
			global $woocommerce;
			$user_id = get_current_user_id();
			$current_cart = null;
			if ( function_exists('WC') && !empty(WC()->session) ) {
				if(!empty(WC()->session->cart)) {
					$current_cart = WC()->session->cart;
				}else {
					$current_cart = WC()->session->get( 'cart' );
				}
			} elseif(!empty($woocommerce) && !empty($woocommerce->session) && !empty($woocommerce->session->cart)) {
				$current_cart = $woocommerce->session->cart;
			}
			$unific_cart_hash = !empty(@$_COOKIE["unific_cart_hash"]) ? trim(@$_COOKIE["unific_cart_hash"]) : null;
			if (!empty($user_id) && !empty( $current_cart ) && is_array( $current_cart ) &&  sizeof( $current_cart ) > 0 ){
				if(empty($unific_cart_hash)){
					//$unific_cart_hash = uniqid();
					$unific_cart_hash = (int)(microtime(true) * 1000);
                                        setcookie("unific_cart_hash",$unific_cart_hash ,time()+(60*60),"/");
                                }

				$email_query = "SELECT unific_meta_id,meta_key,meta_value FROM `".$wpdb->prefix."usermeta_unific` WHERE entity_id = %d && entity_type = 'checkout' && meta_key = 'email'";
				$first_name_query = "SELECT unific_meta_id,meta_key,meta_value FROM `".$wpdb->prefix."usermeta_unific` WHERE entity_id = %d && entity_type = 'checkout' && meta_key = 'first_name'";
				$last_name_query = "SELECT unific_meta_id,meta_key,meta_value FROM `".$wpdb->prefix."usermeta_unific` WHERE entity_id = %d && entity_type = 'checkout' && meta_key = 'last_name'";
				$billing_address_query = "SELECT unific_meta_id,meta_key,meta_value FROM `".$wpdb->prefix."usermeta_unific` WHERE entity_id = %d && entity_type = 'checkout' && meta_key = 'billing_address'";
				$shipping_address_query = "SELECT unific_meta_id,meta_key,meta_value FROM `".$wpdb->prefix."usermeta_unific` WHERE entity_id = %d && entity_type = 'checkout' && meta_key = 'shipping_address'";

				$email_result = $wpdb->get_row($wpdb->prepare($email_query,$unific_cart_hash));
				unset($email_query);
				$first_name_result = $wpdb->get_row($wpdb->prepare($first_name_query,$unific_cart_hash));
				unset($first_name_query);
				$last_name_result = $wpdb->get_row($wpdb->prepare($last_name_query,$unific_cart_hash));
				unset($last_name_query);
				$billing_address_result = $wpdb->get_row($wpdb->prepare($billing_address_query,$unific_cart_hash));
				unset($billing_address_query);
				$shipping_address_result = $wpdb->get_row($wpdb->prepare($shipping_address_query,$unific_cart_hash));
				unset($shipping_address_query);

				$current_user = wp_get_current_user();
				$current_user_billing_address = array();
				$current_user_shipping_address = array();
				if(!empty($user_id)){
					$current_user_meta = get_user_meta($user_id, "", true);
					if(!empty($current_user_meta)){
						foreach($current_user_meta as $key => $val){
							if(startsWith($key, "billing_")){
								if(!empty($val['0'])){
									$current_user_billing_address[str_replace("billing_","",$key)] = $val['0'];
								}else{
									$current_user_billing_address[str_replace("billing_","",$key)] = "";
								}
							}
							if(startsWith($key, "shipping_")){
								if(!empty($val['0'])){
									$current_user_shipping_address[str_replace("shipping_","",$key)] = $val['0'];
								}else{
									$current_user_shipping_address[str_replace("shipping_","",$key)] = "";
								}
							}
						}
					}
				}
				if(!empty($email_result) && !empty($email_result->meta_value)){
					$email = $email_result->meta_value;
				}else{
					$email = $current_user->user_email;
				}
				if(!empty($first_name_result) && !empty($first_name_result->meta_value)){
					$billing_first_name = $first_name_result->meta_value;
				}else{
					$billing_first_name = (!empty($current_user->user_firstname)) ? $current_user->user_firstname : 'No Name';
				}
				if(!empty($last_name_result) && !empty($last_name_result->meta_value)){
					$billing_last_name = $last_name_result->meta_value;
				}else{
					$billing_last_name = (!empty($current_user->user_lastname)) ? $current_user->user_lastname : 'No Name';
				}

				if(!empty($billing_address_result) && !empty($billing_address_result->meta_value)){
					$billing_address = custom_unserialize($billing_address_result->meta_value);
					if(empty($billing_address)){
						$billing_address = $current_user_billing_address;
					}
				}else{
					$billing_address = $current_user_billing_address;
				}

				if(!empty($shipping_address_result) && !empty($shipping_address_result->meta_value)){
					$shipping_address = custom_unserialize($shipping_address_result->meta_value);
					if(empty($shipping_address)){
						$shipping_address = $current_user_shipping_address;
					}
				}else{
					$shipping_address = $current_user_shipping_address;
				}

				if(!empty($unific_cart_hash) && !empty($email) && !empty($billing_first_name) && !empty($billing_last_name)){
					saveCheckoutData($unific_cart_hash,$email,$billing_first_name,$billing_last_name,$billing_address,$shipping_address,$current_cart,$user_id);
				}
			}
		}
	}

	if(!function_exists('custom_unserialize')){
		function custom_unserialize($meta_value){
			if ( @unserialize($meta_value) !== true &&  preg_match('/^[aOs]:/', $meta_value) ) {
				$meta_value = preg_replace_callback( '/s\:(\d+)\:\"(.*?)\";/s',    function($matches){return 's:'.strlen($matches[2]).':"'.$matches[2].'";'; },   $meta_value );
			}
			return unserialize($meta_value);
		}
	}

	if(!function_exists('check_and_load_cart_from_abandoned_url')){
		function check_and_load_cart_from_abandoned_url(){
			if(is_cart() && !empty($_GET['unific_cart_hash'])){
				global $wpdb, $woocommerce;
				
				$unific_cart_hash = trim($_GET['unific_cart_hash']);
				$cart_url = wc_get_cart_url();
		
				$cart_query = "SELECT meta_value FROM `".$wpdb->prefix."usermeta_unific` WHERE entity_id = %d && entity_type = 'checkout' && meta_key = 'unific_persistent_cart'";

				$meta = $wpdb->get_row($wpdb->prepare($cart_query,$unific_cart_hash));
				unset($cart_query);

				if(!empty($meta->meta_value)){
					$meta_value = custom_unserialize($meta->meta_value);
					if(!empty($meta_value)){
						//add products of meta values in cart
						foreach($meta_value as $line_item){
							if(!empty($line_item['product_id']) && !empty($line_item['quantity'])){
								//added this code to show variants by Vilas
								$variation_id = 0;
								$variation = array();
								if(!empty($line_item['variation_id'])){
									$variation_id = $line_item['variation_id'];
								}
								if(!empty($line_item['variation'])){
									$variation = $line_item['variation'];
								}
								WC()->cart->add_to_cart( $line_item['product_id'], $line_item['quantity'], $variation_id, $variation );
							}
						}
						setcookie("unific_cart_hash",$unific_cart_hash,time()+(60*60),"/");
						wp_redirect($cart_url);
					}
				}
			}
		}
	}

	if(!function_exists('logout_callback')){
		function logout_callback(){
			setcookie("unific_cart_hash", "",time()-(60*60),"/");
		}
	}

	if(!function_exists('add_meta_to_woocommerce_webhook_payload')){
		function add_meta_to_woocommerce_webhook_payload($payload, $resource, $resource_id, $this_id){
			if ($resource == 'order') {
				if(!empty($resource_id)){
					$order_meta = get_post_meta($resource_id, "", true);
					$meta = array();
					if(!empty($order_meta)){
						foreach($order_meta as $key => $val){
							if(!empty($key) && !empty($val)){
								//exclude default meta keys which starts with underscore
								if(!(strpos($key, "_") === 0)){
									$meta[$key] = $val[0];
								}
							}
						}
					}
					if(!empty($payload['order'])){
						$payload['order']['order_meta'] = $meta;
					}elseif(!empty($payload)){
						$payload['order_meta'] = $meta;
					}
					return $payload;
				}
			}
			return $payload;
		}
	}

	//fire customer update webhook for order create/update
	if(!function_exists('action_woocommerce_order')){
		function action_woocommerce_order($order_id){
			$order = wc_get_order( $order_id );
			$user_id = $order->get_user_id();
			if(!empty($user_id)){
				do_action( 'woocommerce_update_customer', $user_id);
			}
		}
	}

	/****************************** End here  *************************************/

	if(!function_exists('unific_parse_request')){
		function unific_parse_request($wp){
			$query_params = $wp->query_vars;
			if(!empty($query_params['affiliate'])){
				setcookie("unific_affiliate_id", trim($query_params['affiliate']),time()+(3600*24*30),"/");
			}
		}
	}

	if(!function_exists('unific_add_query_vars_filter')){
		function unific_add_query_vars_filter( $vars ){
			$vars[] = "affiliate";
			return $vars;
		}
	}

	//add_action($hook,$callback_function,$priority,$no_of_parameter);
	add_filter( 'query_vars', 'unific_add_query_vars_filter' );
	add_action( 'parse_query', 'unific_parse_request');
	add_action('woocommerce_checkout_update_order_meta', 'unific_save_extra_checkout_fields', 10, 1);

	/****************************** Newly added for WC abandoned cart  *************************************/
	add_filter( 'woocommerce_webhook_payload', 'add_meta_to_woocommerce_webhook_payload', 10, 4);

	add_action( 'wp_ajax_save_user', 'save_user_callback' );
	add_action( 'wp_ajax_nopriv_save_user', 'save_user_callback' );

	add_action('woocommerce_after_checkout_billing_form', 'load_user_data_js');

	add_action('template_redirect', 'check_and_load_cart_from_abandoned_url',10,1);
	add_action('woocommerce_checkout_init', 'unific_save_custom_persistant_cart', 10, 1);
	add_action('wp_logout', 'logout_callback');
				
	add_action( 'woocommerce_new_order', 'action_woocommerce_order', 10, 1 );
	add_action( 'woocommerce_update_order', 'action_woocommerce_order', 10, 1 );

	/********************* Custom hook function************************/
	add_action('woocommerce_unific_checkout_update_action', 'unific_checkout_update', 10, 1);
	add_action('woocommerce_unific_checkout_create_action', 'unific_checkout_create', 10, 1);	
	
	/******************************************************************/	
	
	add_action( 'woocommerce_api_loaded', function(){
			include_once( 'class-wc-api-unific-checkout.php' );
			});

	add_filter( 'woocommerce_api_classes', function( $classes ){
			$classes[] = 'WC_API_Unific_Checkout';
			return $classes;
			});

	/****************************** End here  *************************************/
}
?>

jQuery('input#billing_email').val(unific_object.email);
jQuery('input#billing_first_name').val(unific_object.billing_first_name);
jQuery('input#billing_last_name').val(unific_object.billing_last_name);

jQuery('form[name="checkout"] input').on( 'change', function() {
        var field_name_arr = [ "billing_email", "billing_first_name", "billing_last_name", "billing_postcode" ];
        var field_name = jQuery(this).attr("name");
        if(jQuery.inArray( field_name, field_name_arr ) !== -1) {
                unific_validate_and_post();
        }
});

function unific_validate_and_post(){
        billing_first_name =  jQuery.trim(jQuery('#billing_first_name').val());
        billing_last_name = jQuery.trim(jQuery('#billing_last_name').val());
        billing_email = jQuery.trim(jQuery('#billing_email').val());

        if(billing_first_name && billing_last_name && billing_email){
                if(UnificValidateEmail(billing_email)){
                        unific_post_user_data();
                }
        }
} 

// Function that validates email address through a regular expression.
function UnificValidateEmail(email) {
        var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
        if (filter.test(email)) return true;
        
        return false;
}

function unific_post_user_data(){
        var data = {};
        jQuery.each(jQuery('form[name="checkout"]').serializeArray(), function(_, obj) {
                        data[obj.name] = obj.value;
                        });
        data.action = "save_user";
        jQuery.post( unific_object.admin_ajax_url, data, function(response) {
        });
}
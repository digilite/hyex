<?php
/*
Plugin Name: Direct Freight Express shipping
Plugin URI: https://digilite.ca
Description: Direct Freight Express shipping for woocommerce
Version: 1.0.0
Author: Narek Smbatyan
Author URI: https://digilite.ca
*/

/**
 * Check if WooCommerce is active
 */
if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {

	function your_shipping_method_init() {
		if ( ! class_exists( 'WC_Your_Shipping_Method' ) ) {
			class WC_Your_Shipping_Method extends WC_Shipping_Method {
				/**
				 * Constructor for your shipping class
				 *
				 * @access public
				 * @return void
				 */
				public function __construct() {
					$this->id                 = 'your_shipping_method'; // Id for your shipping method. Should be uunique.
					$this->method_title       = __( 'Direct Freight Express shipping' );  // Title shown in admin
					$this->method_description = __( 'Direct Freight Express shipping woocommerce' ); // Description shown in admin
					

					//$this->enabled            = "yes"; // This can be added as an setting but for this example its forced enabled
					//$this->title              = "Direct Freight Express shipping"; // This can be added as an setting but for this example its forced.
                    
					$this->init();
					$this->enabled = isset( $this->settings['enabled'] ) ? $this->settings['enabled'] : 'yes';
					$this->title = isset( $this->settings['title'] ) ? $this->settings['title'] : __( 'Direct Freight Express', 'your_shipping_method' );
					$this->authorization = $this->settings['authorization'];
					$this->account_number = $this->settings['account_number'];
					$this->store_city = $this->settings['store_city'];
					$this->store_postcode = $this->settings['store_postcode'];
				}

				/**
				 * Init your settings
				 *
				 * @access public
				 * @return void
				 */
				function init() {
					// Load the settings API
					$this->init_form_fields(); // This is part of the settings API. Override the method to add your own settings
					$this->init_settings(); // This is part of the settings API. Loads settings you previously init.

					// Save settings in admin if you have any defined
					add_action( 'woocommerce_update_options_shipping_' . $this->id, array( $this, 'process_admin_options' ) );
				}


				function init_form_fields() { 
 
                    include 'includes/fields.php';
 
                }

				/**
				 * calculate_shipping function.
				 *
				 * @access public
				 * @param mixed $package
				 * @return void
				 */
	public function calculate_shipping( $package = array() ) {
					
		           include 'includes/calculate-shipping.php';
	
				}
			}
		}
	}

	add_action( 'woocommerce_shipping_init', 'your_shipping_method_init' );

	function add_your_shipping_method( $methods ) {
		$methods['your_shipping_method'] = 'WC_Your_Shipping_Method';
		return $methods;
	}

	add_filter( 'woocommerce_shipping_methods', 'add_your_shipping_method' );
	
	
} ?>
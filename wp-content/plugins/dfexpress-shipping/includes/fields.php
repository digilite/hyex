<?php $this->form_fields = array(
 
 'enabled' => array(
      'title' => __( 'Enable', 'your_shipping_method' ),
      'type' => 'checkbox',
      'description' => __( 'Enable this shipping.', 'your_shipping_method' ),
      'default' => 'yes'
      ),

 'title' => array(
    'title' => __( 'Title', 'your_shipping_method' ),
      'type' => 'text',
      'description' => __( 'Title to be display on site', 'your_shipping_method' ),
      'default' => __( 'Direct Freight Express', 'your_shipping_method' )
     ),
     
 'authorization' => array(
        'title' => __( 'Authorization', 'your_shipping_method' ),
          'type' => 'text',
          'description' => __( 'Type Direct Freight Express Authorization key', 'your_shipping_method' ),
          'default' => __( '', 'your_shipping_method' )
      ),

  'account_number' => array(
            'title' => __( 'Account Number', 'your_shipping_method' ),
              'type' => 'text',
              'description' => __( 'Type Direct Freight Express Account number', 'your_shipping_method' ),
              'default' => __( '', 'your_shipping_method' )
  ),

  'store_city' => array(
    'title' => __( 'Store City', 'your_shipping_method' ),
      'type' => 'text',
      'description' => __( 'Your store city', 'your_shipping_method' ),
      'default' => __( 'Gordon', 'your_shipping_method' )
  ),

  'store_postcode' => array(
    'title' => __( 'Store postal code', 'your_shipping_method' ),
      'type' => 'text',
      'description' => __( 'Your store postal code', 'your_shipping_method' ),
      'default' => __( '2072', 'your_shipping_method' )
)

 ); ?>
    <?php
    $weight = 0;
    $cost = 0;
	$city = $package['destination']['city'];
	$postcode = $package['destination']['postcode'];
	
 
    foreach ( $package['contents'] as $item_id => $values ) 
    { 
        $_product = $values['data']; 
        $weight = ceil($weight + $_product->get_weight() * $values['quantity']); 
    }
 
        $weight = wc_get_weight( $weight, 'kg' );
        $weight = ceil($weight);
        

		$curl = curl_init();

		curl_setopt_array($curl, array(
		CURLOPT_URL => "https://webservices.directfreight.com.au/Dispatch/api/GetConsignmentPrice",
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 0,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "POST",
		CURLOPT_POSTFIELDS =>"{\"SuburbFrom\":\"{$this->store_city}\",\"PostcodeFrom\":\"{$this->store_postcode}\",\"SuburbTo\":\"{$city}\",\"PostcodeTo\":\"{$postcode}\",\"ConsignmentLineItems\":[{\"RateType\":\"ITEM\",\"Items\":1,\"Kgs\":$weight}]}",
		CURLOPT_HTTPHEADER => array(
			"Authorisation: {$this->authorization}",
			"AccountNumber: {$this->account_number}",
			"ContentType: application/json",
			"Accept: application/json",
			"Timeout: 120000",
			"Content-Type: application/json"
		),
		));

		$response = curl_exec($curl);

		curl_close($curl);

		$response = json_decode($response, true);
		$response['TotalFreightCharge'];
		$int1 = (float)$response['TotalFreightCharge'];
		$int2 = (float)$response['StandardCharge'];
		$int3 = (float)$response['OnForwardCharge'];
		$int4 = (float)$response['FuelLevy'];
		$int5 = (float)$response['FuelLevyCharge'];
		$cost = $int1 + $int5;
        
					
					
					
					
					
					
					
					$rate = array(
						'label' => $this->title,
						'cost' => $cost,
						'taxes' => '',
						'calc_tax' => 'per_order'
					);

					// Register the rate
					$this->add_rate( $rate );
        
        ?>
<?php function edit_dfe_button_color()
{
	$buttoncolor = get_option('df_button_color');

	?>
        <input type="color" id="df_btn_color" name="df_button_color" value="<?= $buttoncolor; ?>">
	<?php

	
}

function edit_dfe_button_bg()
{
	$buttonbg = get_option('df_bg_color');

	?>
		<input type="color" id="df_btn_bg" name="df_bg_color" value="<?= $buttonbg; ?>">
	<?php

	
}

function edit_dfe_field_color()
{
	$fieldcolor = get_option('df_field_color');

	?>
		<input type="color" id="df_field_color" name="df_field_color" value="<?= $fieldcolor; ?>">
	<?php

	
}

function edit_dfe_field_bg()
{
	$fieldbg = get_option('df_field_bg');

	?>
		<input type="color" id="df_field_bg" name="df_field_bg" value="<?= $fieldbg; ?>">
	<?php

	
}

function df_tracking_title()
{
	$trackingtitle = get_option('df_tracking_title');

	?>
		<input type="text" id="df_tracking_title" name="df_tracking_title" value="<?= $trackingtitle; ?>">
	<?php

	
}

function df_notice()
{
	$df_notice = get_option('df_notice');

	?>
		<input type="text" id="df_notice" name="df_notice" value="<?= $df_notice; ?>">
	<?php

	
}

function df_field_border_color()
{
	$df_field_border_color = get_option('df_field_border_color');

	?>
		<input type="color" id="df_field_border_color" name="df_field_border_color" value="<?= $df_field_border_color; ?>">
	<?php

	
}

function df_button_border_color()
{
	$df_button_border_color = get_option('df_button_border_color');

	?>
		<input type="color" id="df_button_border_color" name="df_button_border_color" value="<?= $df_button_border_color; ?>">
	<?php

	
}

function display_theme_panel_fields()
{
	add_settings_section("section", "Direct Freight Express tracking styles", null, "theme-options");
	
    add_settings_field("df_button_color", "Button text color", "edit_dfe_button_color", "theme-options", "section");
	add_settings_field("df_bg_color", "Button background color", "edit_dfe_button_bg", "theme-options", "section");
    add_settings_field("df_button_border_color", "Button border color", "df_button_border_color", "theme-options", "section");
	add_settings_field("df_field_color", "Field text color", "edit_dfe_field_color", "theme-options", "section");
	add_settings_field("df_field_bg", "Field background color", "edit_dfe_field_bg", "theme-options", "section");
    add_settings_field("df_field_border_color", "Field border color", "df_field_border_color", "theme-options", "section");
	add_settings_field("df_tracking_title", "Block title", "df_tracking_title", "theme-options", "section");
    add_settings_field("df_notice", "Notification text", "df_notice", "theme-options", "section");

    register_setting("section", "df_button_color");
	register_setting("section", "df_bg_color");
	register_setting("section", "df_field_color");
	register_setting("section", "df_field_bg");
	register_setting("section", "df_tracking_title");
    register_setting("section", "df_notice");
    register_setting("section", "df_field_border_color");
    register_setting("section", "df_button_border_color");
}

add_action("admin_init", "display_theme_panel_fields");
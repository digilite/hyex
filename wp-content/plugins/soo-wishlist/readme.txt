=== WooCommerce Checkout Field Editor (Manager) Pro ===
Contributors: UIX Themes
Tags: woocommerce wishlist
Requires at least: 4.0
Tested up to: 5.1
Stable tag: 1.1.2
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Soo Wishlist - Premium WooCommerce plugin to add wishlists to your store.

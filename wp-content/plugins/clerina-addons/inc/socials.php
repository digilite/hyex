<?php
/**
 * Hooks for share socials
 *
 * @package Clerina
 */

if ( ! function_exists( 'clerina_addons_share_link_socials' ) ) :
	function clerina_addons_share_link_socials( $title, $link, $media ) {
		$socials = array();
		if ( is_singular('post') ) {
			$socials = clerina_get_option( 'post_socials_share' );
		}

		$socials_html = '';
		if ( $socials ) {
			if ( in_array( 'facebook', $socials ) ) {
				$socials_html .= sprintf(
					'<li><a class="share-facebook clerina-facebook" title="%s" href="http://www.facebook.com/sharer.php?u=%s&t=%s" target="_blank"><i class="social_facebook"></i></a></li>',
					esc_attr( $title ),
					urlencode( $link ),
					urlencode( $title )
				);
			}

			if ( in_array( 'twitter', $socials ) ) {
				$socials_html .= sprintf(
					'<li><a class="share-twitter clerina-twitter" href="http://twitter.com/share?text=%s&url=%s" title="%s" target="_blank"><i class="social_twitter"></i></a></li>',
					urlencode( $title ),
					urlencode( $link ),
					esc_attr( $title )
				);
			}

			if ( in_array( 'pinterest', $socials ) ) {
				$socials_html .= sprintf(
					'<li><a class="share-pinterest clerina-pinterest" href="http://pinterest.com/pin/create/button?media=%s&url=%s&description=%s" title="%s" target="_blank"><i class="social_pinterest"></i></a></li>',
					urlencode( $media ),
					urlencode( $link ),
					esc_attr( $title ),
					urlencode( $title )
				);
			}

			if ( in_array( 'google', $socials ) ) {
				$socials_html .= sprintf(
					'<li><a class="share-google-plus clerina-google-plus" href="https://plus.google.com/share?url=%s&text=%s" title="%s" target="_blank"><i class="social_googleplus"></i></a></li>',
					urlencode( $link ),
					esc_attr( $title ),
					urlencode( $title )
				);
			}

			if ( in_array( 'linkedin', $socials ) ) {
				$socials_html .= sprintf(
					'<li><a class="share-linkedin clerina-linkedin" href="http://www.linkedin.com/shareArticle?url=%s&title=%s" title="%s" target="_blank"><i class="ion-social-linkedin"></i></a></li>',
					urlencode( $link ),
					esc_attr( $title ),
					urlencode( $title )
				);
			}

			if ( in_array( 'tumblr', $socials ) ) {
				$socials_html .= sprintf(
					'<li><a class="share-tumblr clerina-tumblr" href="http://www.tumblr.com/share/link?url=%s" title="%s" target="_blank"><i class="social_tumblr"></i></a></li>',
					urlencode( $link ),
					esc_attr( $title )
				);
			}
		}

		if ( $socials_html ) {
			return sprintf( '<ul class="clerina-social-share socials-inline">%s</ul>', $socials_html );
		}
		?>
		<?php
	}

endif;


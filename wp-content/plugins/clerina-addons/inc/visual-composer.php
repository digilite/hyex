<?php
/**
 * Custom functions for Visual Composer
 *
 * @package    Clerina
 * @subpackage Visual Composer
 */

if ( ! function_exists( 'is_plugin_active' ) ) {
	require_once( ABSPATH . 'wp-admin/includes/plugin.php' );
}

/**
 * Class CleanPro
 *
 * @since 1.0.0
 */
class Clerina_VC {

	/**
	 * Temporary cached terms variable
	 *
	 * @var array
	 */
	public $terms = array();

	/**
	 * Construction
	 */
	function __construct() {
		// Stop if VC is not installed
		if ( ! is_plugin_active( 'js_composer/js_composer.php' ) ) {
			return false;
		}

		add_action( 'init', array( $this, 'map_shortcodes' ), 20 );
	}

	/**
	 * Add new params or add new shortcode to VC
	 *
	 * @since 1.0
	 *
	 * @return void
	 */
	function map_shortcodes() {
		// Empty Space
		vc_map(
			array(
				'name'     => esc_html__( 'Clerina Empty Space', 'clerina' ),
				'base'     => 'cleanpro_empty_space',
				'class'    => '',
				'category' => esc_html__( 'Clerina', 'clerina' ),
				'params'   => array(
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__( 'Height(px)', 'clerina' ),
						'param_name'  => 'height',
						'admin_label' => true,
						'description' => esc_html__( 'Enter empty space height on Desktop.', 'clerina' )
					),
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__( 'Height on Tablet(px)', 'clerina' ),
						'param_name'  => 'height_tablet',
						'admin_label' => true,
						'description' => esc_html__( 'Enter empty space height on Mobile. Leave empty to use the height of the desktop', 'clerina' )
					),
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__( 'Height on Mobile(px)', 'clerina' ),
						'param_name'  => 'height_mobile',
						'admin_label' => true,
						'description' => esc_html__( 'Enter empty space height on Mobile. Leave empty to use the height of the tablet', 'clerina' )
					),
					array(
						'type'       => 'colorpicker',
						'heading'    => esc_html__( 'Background Color', 'clerina' ),
						'param_name' => 'bg_color',
						'value'      => '',
					),
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__( 'Extra class name', 'clerina' ),
						'param_name'  => 'el_class',
						'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'clerina' ),
					),
				),
			)
		);

		// Contact form 7
		$mail_forms    = get_posts( 'post_type=wpcf7_contact_form&posts_per_page=-1' );
		$mail_form_ids = array(
			esc_html__( 'Select Form', 'clerina' ) => '',
		);
		foreach ( $mail_forms as $form ) {
			$mail_form_ids[ $form->post_title ] = $form->ID;
		}
		vc_map(
			array(
				'name'     => esc_html__( 'Cleanpro Contact Form 7', 'clerina' ),
				'base'     => 'cleanpro_contact_form_7',
				'class'    => '',
				'category' => esc_html__( 'Clerina', 'clerina' ),
				'params'   => array(
					array(
						'type'       => 'dropdown',
						'heading'    => esc_html__( 'Style Version', 'clerina' ),
						'param_name' => 'style',
						'value'      => array(
							esc_html__( '1 Column', 'clerina' )   => '1',
							esc_html__( '2 Columns ', 'clerina' ) => '2',
						),
					),
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__( 'Section title', 'clerina' ),
						'param_name'  => 'title',
						'value'       => '',
						'description' => esc_html__( 'Enter title for this section', 'clerina' ),
					),
					array(
						'type'       => 'dropdown',
						'heading'    => esc_html__( 'Form Version', 'clerina' ),
						'param_name' => 'color',
						'value'      => array(
							esc_html__( 'Dark', 'clerina' )  => 'dark',
							esc_html__( 'Light', 'clerina' ) => 'light',
						),
					),
					array(
						'type'       => 'dropdown',
						'heading'    => esc_html__( 'Style submit', 'clerina' ),
						'param_name' => 'style_sub',
						'value'      => array(
							esc_html__( 'Classic', 'clerina' ) => '1',
							esc_html__( 'Modern', 'clerina' )  => '2',
						),
						'dependency' => array(
							'element' => 'style',
							'value'   => array( '1' ),
						),
					),
					array(
						'type'       => 'dropdown',
						'heading'    => esc_html__( 'Contact Form 7', 'clerina' ),
						'param_name' => 'form',
						'value'      => $mail_form_ids,
					),
					array(
						'type'       => 'colorpicker',
						'heading'    => esc_html__( 'Form Background Color', 'clerina' ),
						'param_name' => 'form_bg',
						'value'      => '',
						'group'      => esc_html__( 'CSS', 'clerina' ),
					),
					array(
						'type'       => 'textfield',
						'heading'    => esc_html__( 'Padding Top (px)', 'clerina' ),
						'param_name' => 'padding_top',
						'value'      => '',
						'group'      => esc_html__( 'CSS', 'clerina' ),
					),
					array(
						'type'       => 'textfield',
						'heading'    => esc_html__( 'Padding Right (px)', 'clerina' ),
						'param_name' => 'padding_right',
						'value'      => '',
						'group'      => esc_html__( 'CSS', 'clerina' ),
					),
					array(
						'type'       => 'textfield',
						'heading'    => esc_html__( 'Padding Bottom (px)', 'clerina' ),
						'param_name' => 'padding_bottom',
						'value'      => '',
						'group'      => esc_html__( 'CSS', 'clerina' ),
					),
					array(
						'type'       => 'textfield',
						'heading'    => esc_html__( 'Padding Left (px)', 'clerina' ),
						'param_name' => 'padding_left',
						'value'      => '',
						'group'      => esc_html__( 'CSS', 'clerina' ),
					),
					array(
						'type'       => 'textfield',
						'heading'    => esc_html__( 'Border Radius (px)', 'clerina' ),
						'param_name' => 'border_radius',
						'value'      => '',
						'group'      => esc_html__( 'CSS', 'clerina' ),
					),
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__( 'Extra class name', 'clerina' ),
						'param_name'  => 'el_class',
						'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'clerina' ),
					),
				),
			)
		);

		// Price table
		vc_map(
			array(
				'name'     => esc_html__( 'Clerina Price table', 'clerina' ),
				'base'     => 'cleanpro_price_table',
				'class'    => '',
				'category' => esc_html__( 'Clerina', 'clerina' ),
				'params'   => array(
					array(
						'type'       => 'dropdown',
						'heading'    => esc_html__( 'Style Version', 'clerina' ),
						'param_name' => 'style',
						'value'      => array(
							esc_html__( 'Style 1', 'clerina' ) => '1',
							esc_html__( 'Style 2', 'clerina' ) => '2',
						),
					),
					array(
						'type'       => 'textfield',
						'heading'    => esc_html__( 'Title', 'clerina' ),
						'param_name' => 'title',
						'value'      => '',
					),
					array(
						'type'       => 'textfield',
						'heading'    => esc_html__( 'Price', 'clerina' ),
						'param_name' => 'price',
						'value'      => '',
					),
					array(
						'type'       => 'textfield',
						'heading'    => esc_html__( 'Unit', 'clerina' ),
						'param_name' => 'unit',
						'value'      => '',
					),
					array(
						'type'       => 'textfield',
						'heading'    => esc_html__( 'Text after', 'clerina' ),
						'param_name' => 'text_after',
						'value'      => '',
					),
					array(
						'type'       => 'vc_link',
						'heading'    => esc_html__( 'Link (URL)', 'clerina' ),
						'param_name' => 'link',
						'value'      => '',
					),
					array(
						'type'       => 'param_group',
						'heading'    => esc_html__( 'Content', 'clerina' ),
						'value'      => '',
						'param_name' => 'setting',
						'params'     => array(
							array(
								'heading'     => esc_html__( 'Title service', 'clerina' ),
								'type'        => 'textfield',
								'param_name'  => 'service',
								'admin_label' => true,
							),
						),
						'group'      => esc_html__( 'List Service', 'clerina' ),
					),
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__( 'Extra class name', 'clerina' ),
						'param_name'  => 'el_class',
						'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'clerina' ),
						'group'       => esc_html__( 'Add class', 'clerina' ),
					),
				),
			)
		);

		// Call to action
		vc_map(
			array(
				'name'     => esc_html__( 'Clerina Call to action', 'clerina' ),
				'base'     => 'cleanpro_cta',
				'class'    => '',
				'category' => esc_html__( 'Clerina', 'clerina' ),
				'params'   => array(
					array(
						'type'       => 'dropdown',
						'heading'    => esc_html__( 'Style Version', 'clerina' ),
						'param_name' => 'style',
						'value'      => array(
							esc_html__( 'Style button text', 'clerina' )  => '1',
							esc_html__( 'Style button image', 'clerina' ) => '2',
						),
					),
					array(
						'type'       => 'textfield',
						'heading'    => esc_html__( 'Title', 'clerina' ),
						'param_name' => 'title',
						'value'      => '',
					),
					array(
						'type'       => 'textarea',
						'heading'    => esc_html__( 'Description', 'clerina' ),
						'param_name' => 'desc',
						'value'      => '',
					),
					array(
						'type'       => 'vc_link',
						'heading'    => esc_html__( 'Link (URL)', 'clerina' ),
						'param_name' => 'link',
						'value'      => '',
						'dependency' => array(
							'element' => 'style',
							'value'   => '1',
						),
					),
					array(
						'heading'     => esc_html__( 'Image size', 'clerina' ),
						'description' => esc_html__( 'Enter image size. Example: "thumbnail", "medium", "large", "full" or other sizes defined by current theme. Alternatively enter image size in pixels: 200x100 (Width x Height). Leave empty to use "thumbnail" size.', 'clerina' ),
						'type'        => 'textfield',
						'param_name'  => 'image_size',
						'value'       => '',
						'dependency'  => array(
							'element' => 'style',
							'value'   => '2',
						),
					),
					array(
						'type'       => 'param_group',
						'heading'    => esc_html__( 'List button image', 'clerina' ),
						'value'      => '',
						'param_name' => 'setting',
						'params'     => array(
							array(
								'type'       => 'attach_image',
								'heading'    => esc_html__( 'Image button', 'clerina' ),
								'param_name' => 'image',
							),
							array(
								'heading'    => esc_html__( 'URL (Link)', 'clerina' ),
								'type'       => 'vc_link',
								'param_name' => 'link',
							),
						),
						'dependency' => array(
							'element' => 'style',
							'value'   => '2',
						),
					),
					array(
						'type'       => 'colorpicker',
						'heading'    => esc_html__( 'Title color', 'clerina' ),
						'param_name' => 'color_tit',
						'value'      => '',
						'group'      => esc_html__( 'Style', 'clerina' ),
					),
					array(
						'type'       => 'colorpicker',
						'heading'    => esc_html__( 'Description color', 'clerina' ),
						'param_name' => 'color_desc',
						'value'      => '',
						'group'      => esc_html__( 'Style', 'clerina' ),
					),
					array(
						'type'       => 'colorpicker',
						'heading'    => esc_html__( 'Background button color', 'clerina' ),
						'param_name' => 'color_bk',
						'value'      => '',
						'group'      => esc_html__( 'Style', 'clerina' ),
						'dependency' => array(
							'element' => 'style',
							'value'   => '1',
						),
					),
					array(
						'type'       => 'colorpicker',
						'heading'    => esc_html__( 'Button color ', 'clerina' ),
						'param_name' => 'color_btn',
						'value'      => '',
						'group'      => esc_html__( 'Style', 'clerina' ),
						'dependency' => array(
							'element' => 'style',
							'value'   => '1',
						),
					),
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__( 'Extra class name', 'clerina' ),
						'param_name'  => 'el_class',
						'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'clerina' ),
						'group'       => esc_html__( 'Add class', 'clerina' ),
					),
				),
			)
		);

		// Team carousel
		vc_map(
			array(
				'name'     => esc_html__( 'Clerina Team Carousel', 'clerina' ),
				'base'     => 'cleanpro_team_carousel',
				'class'    => '',
				'category' => esc_html__( 'Clerina', 'clerina' ),
				'params'   => array(
					array(
						'type'       => 'dropdown',
						'heading'    => esc_html__( 'Columns', 'clerina' ),
						'param_name' => 'item',
						'value'      => array(
							esc_html__( '3 items', 'clerina' ) => '3',
							esc_html__( '4 items', 'clerina' ) => '4',
						),
					),
					array(
						'type'        => 'checkbox',
						'heading'     => esc_html__( 'Nav', 'clerina' ),
						'param_name'  => 'nav',
						'value'       => array( esc_html__( 'Yes', 'clerina' ) => '1' ),
						'description' => esc_html__( 'If "YES" show arrow', 'clerina' ),
					),
					array(
						'type'        => 'checkbox',
						'heading'     => esc_html__( 'Autoplay', 'clerina' ),
						'param_name'  => 'autoplay',
						'value'       => array( esc_html__( 'Yes', 'clerina' ) => 'yes' ),
						'description' => esc_html__( 'If "YES" Enable autoplay', 'clerina' ),
					),
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__( 'Autoplay speed', 'clerina' ),
						'param_name'  => 'autoplay_speed',
						'value'       => '2000',
						'description' => esc_html__( 'Set auto play speed (in ms).', 'clerina' ),
						'dependency'  => array(
							'element' => 'autoplay',
							'value'   => array( 'yes' ),
						),
					),
					array(
						'heading'     => esc_html__( 'Image size', 'clerina' ),
						'description' => esc_html__( 'Enter image size. Example: "thumbnail", "medium", "large", "full" or other sizes defined by current theme. Alternatively enter image size in pixels: 200x100 (Width x Height). Leave empty to use "thumbnail" size.', 'clerina' ),
						'type'        => 'textfield',
						'param_name'  => 'image_size',
						'value'       => '',
					),
					array(
						'type'       => 'param_group',
						'heading'    => esc_html__( 'Members', 'clerina' ),
						'value'      => '',
						'param_name' => 'setting',
						'params'     => array(
							array(
								'type'        => 'textfield',
								'heading'     => esc_html__( 'Name', 'clerina' ),
								'param_name'  => 'name',
								'value'       => '',
								'admin_label' => true,
							),
							array(
								'heading'    => esc_html__( 'Avatar image', 'clerina' ),
								'type'       => 'attach_image',
								'param_name' => 'image',
							),
							array(
								'type'        => 'textfield',
								'heading'     => esc_html__( 'Office', 'clerina' ),
								'param_name'  => 'office',
								'value'       => '',
								'admin_label' => true,
							),
							array(
								'type'       => 'param_group',
								'heading'    => esc_html__( 'Social', 'clerina' ),
								'value'      => '',
								'param_name' => 'setting_icon',
								'params'     => array(
									array(
										'heading'     => esc_html__( 'Icon', 'clerina' ),
										'description' => esc_html__( 'Select icon from library.', 'clerina' ),
										'type'        => 'iconpicker',
										'param_name'  => 'icon_fontawesome',
										'value'       => 'fa fa-adjust',
										'settings'    => array(
											'emptyIcon'    => false,
											'iconsPerPage' => 4000,
										),
									),
									array(
										'heading'    => esc_html__( 'URL (Link)', 'clerina' ),
										'type'       => 'vc_link',
										'param_name' => 'link',
									),
								),
							),
						),
						'group'      => esc_html__( 'List members', 'clerina' ),
					),
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__( 'Extra class name', 'clerina' ),
						'param_name'  => 'el_class',
						'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'clerina' ),
						'group'       => esc_html__( 'Add class', 'clerina' ),
					),
				),
			)
		);

		// Image single
		vc_map(
			array(
				'name'     => esc_html__( 'Clerina Image Single', 'clerina' ),
				'base'     => 'cleanpro_image_single',
				'class'    => '',
				'category' => esc_html__( 'Clerina', 'clerina' ),
				'params'   => array(
					array(
						'type'       => 'attach_image',
						'heading'    => esc_html__( 'Image', 'clerina' ),
						'param_name' => 'image',
					),
					array(
						'heading'     => esc_html__( 'Image size', 'clerina' ),
						'description' => esc_html__( 'Enter image size. Example: "thumbnail", "medium", "large", "full" or other sizes defined by current theme. Alternatively enter image size in pixels: 200x100 (Width x Height). Leave empty to use "thumbnail" size.', 'clerina' ),
						'type'        => 'textfield',
						'param_name'  => 'image_size',
						'value'       => '',
					),
					array(
						'type'       => 'dropdown',
						'heading'    => esc_html__( 'Image alignment', 'clerina' ),
						'param_name' => 'image_alignment',
						'value'      => array(
							esc_html__( 'Left', 'clerina' )   => 'left',
							esc_html__( 'Right', 'clerina' )  => 'right',
							esc_html__( 'Center', 'clerina' ) => 'center',
						),
					),
					array(
						'type'       => 'textfield',
						'heading'    => esc_html__( 'Border radius', 'clerina' ),
						'param_name' => 'border_radius',

					),
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__( 'Extra class name', 'clerina' ),
						'param_name'  => 'el_class',
						'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'clerina' ),
					),
				),
			)
		);

		// Section title
		vc_map(
			array(
				'name'     => esc_html__( 'Clerina Section title', 'clerina' ),
				'base'     => 'cleanpro_section_title',
				'class'    => '',
				'category' => esc_html__( 'Clerina', 'clerina' ),
				'params'   => array(
					array(
						'type'       => 'dropdown',
						'heading'    => esc_html__( 'Style', 'clerina' ),
						'param_name' => 'style',
						'value'      => array(
							esc_html__( 'Light', 'clerina' ) => 'light',
							esc_html__( 'Dark', 'clerina' )  => 'dark',
						),
					),
					array(
						'heading'     => esc_html__( 'Title', 'clerina' ),
						'type'        => 'textfield',
						'param_name'  => 'title',
						'value'       => '',
						'admin_label' => true,
					),
					array(
						'type'       => 'textarea_html',
						'heading'    => esc_html__( 'Description', 'clerina' ),
						'param_name' => 'content',
						'value'      => '',
					),
					array(
						'type'        => 'dropdown',
						'heading'     => esc_html__( 'Alignment', 'clerina' ),
						'description' => esc_html__( 'Section title alignment', 'clerina' ),
						'param_name'  => 'alignment',
						'value'       => array(
							esc_html__( 'Left', 'clerina' )   => 'left',
							esc_html__( 'Right', 'clerina' )  => 'right',
							esc_html__( 'Center', 'clerina' ) => 'center',
						),
					),
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__( 'Font size', 'clerina' ),
						'description' => esc_html__( 'Title font size. Example: 16px', 'clerina' ),
						'param_name'  => 'font_size',
						'value'       => '',
						'admin_label' => true,
						'group'       => esc_html__( 'Setting', 'clerina' ),
					),
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__( 'Line height', 'clerina' ),
						'description' => esc_html__( 'Title line height. Example: 16px', 'clerina' ),
						'param_name'  => 'line_height',
						'value'       => '',
						'admin_label' => true,
						'group'       => esc_html__( 'Setting', 'clerina' ),
					),
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__( 'Padding', 'clerina' ),
						'description' => esc_html__( 'Title Padding. Example: 16px', 'clerina' ),
						'param_name'  => 'padding',
						'value'       => '',
						'admin_label' => true,
						'group'       => esc_html__( 'Setting', 'clerina' ),
					),
					array(
						'type'        => 'colorpicker',
						'heading'     => esc_html__( 'Color', 'clerina' ),
						'description' => esc_html__( 'Title color', 'clerina' ),
						'param_name'  => 'text_color',
						'value'       => '',
						'group'       => esc_html__( 'Setting', 'clerina' ),
					),
					array(
						'type'        => 'colorpicker',
						'heading'     => esc_html__( 'Color', 'clerina' ),
						'description' => esc_html__( 'Content color', 'clerina' ),
						'param_name'  => 'content_color',
						'value'       => '',
						'group'       => esc_html__( 'Setting', 'clerina' ),
					),
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__( 'Extra class name', 'clerina' ),
						'param_name'  => 'el_class',
						'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'clerina' ),
						'group'       => esc_html__( 'Setting', 'clerina' ),
					),
				),
			)
		);

		// Image carousel
		vc_map(
			array(
				'name'     => esc_html__( 'Clerina Image Carousel', 'clerina' ),
				'base'     => 'cleanpro_image_carousel',
				'class'    => '',
				'category' => esc_html__( 'Clerina', 'clerina' ),
				'params'   => array(
					array(
						'type'       => 'dropdown',
						'heading'    => esc_html__( 'Columns', 'clerina' ),
						'param_name' => 'item',
						'value'      => array(
							esc_html__( '1 Column', 'clerina' )  => '1',
							esc_html__( '2 Columns', 'clerina' ) => '2',
							esc_html__( '3 Columns', 'clerina' ) => '3',
							esc_html__( '4 Columns', 'clerina' ) => '4',
							esc_html__( '5 Columns', 'clerina' ) => '5',
						),
					),
					array(
						'type'        => 'checkbox',
						'heading'     => esc_html__( 'Nav', 'clerina' ),
						'param_name'  => 'nav',
						'value'       => array( esc_html__( 'Yes', 'clerina' ) => '1' ),
						'dependency'  => array(
							'element' => 'item',
							'value'   => array( '1' ),
						),
						'description' => esc_html__( 'If "YES" show arrow', 'clerina' ),
					),
					array(
						'type'       => 'dropdown',
						'heading'    => esc_html__( 'Rows', 'clerina' ),
						'param_name' => 'row',
						'value'      => array(
							esc_html__( '1 row', 'clerina' )  => '1',
							esc_html__( '2 rows', 'clerina' ) => '2',
						),
					),
					array(
						'type'        => 'checkbox',
						'heading'     => esc_html__( 'Dot', 'clerina' ),
						'param_name'  => 'dots',
						'value'       => array( esc_html__( 'Yes', 'clerina' ) => '1' ),
						'description' => esc_html__( 'If "YES" show dot navigation', 'clerina' ),
					),
					array(
						'type'        => 'checkbox',
						'heading'     => esc_html__( 'Autoplay', 'clerina' ),
						'param_name'  => 'autoplay',
						'value'       => array( esc_html__( 'Yes', 'clerina' ) => 'yes' ),
						'description' => esc_html__( 'If "YES" Enable autoplay', 'clerina' ),
					),
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__( 'Autoplay speed', 'clerina' ),
						'param_name'  => 'autoplay_speed',
						'value'       => '2000',
						'description' => esc_html__( 'Set auto play speed (in ms).', 'clerina' ),
						'dependency'  => array(
							'element' => 'autoplay',
							'value'   => array( 'yes' ),
						),
					),
					array(
						'heading'     => esc_html__( 'Image size', 'clerina' ),
						'description' => esc_html__( 'Enter image size. Example: "thumbnail", "medium", "large", "full" or other sizes defined by current theme. Alternatively enter image size in pixels: 200x100 (Width x Height). Leave empty to use "thumbnail" size.', 'clerina' ),
						'type'        => 'textfield',
						'param_name'  => 'image_size',
						'value'       => '',
					),
					array(
						'type'       => 'param_group',
						'heading'    => esc_html__( 'List images', 'clerina' ),
						'value'      => '',
						'param_name' => 'setting',
						'params'     => array(
							array(
								'heading'    => esc_html__( 'Avatar image', 'clerina' ),
								'type'       => 'attach_image',
								'param_name' => 'image',
							),
							array(
								'heading'    => esc_html__( 'URL (Link)', 'clerina' ),
								'type'       => 'vc_link',
								'param_name' => 'link',
							),
						),
						'group'      => esc_html__( 'Image', 'clerina' ),
					),
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__( 'Extra class name', 'clerina' ),
						'param_name'  => 'el_class',
						'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'clerina' ),
						'group'       => esc_html__( 'Add class', 'clerina' ),
					),
				),
			)
		);

		// Counter
		vc_map(
			array(
				'name'     => esc_html__( 'Clerina Counter', 'clerina' ),
				'base'     => 'cleanpro_counter',
				'class'    => '',
				'category' => esc_html__( 'Clerina', 'clerina' ),
				'params'   => array(
					array(
						'type'       => 'colorpicker',
						'heading'    => esc_html__( 'Text color', 'clerina' ),
						'param_name' => 'color',
						'value'      => '',
					),
					array(
						'type'       => 'textfield',
						'heading'    => esc_html__( 'Number', 'clerina' ),
						'param_name' => 'number',
						'value'      => '',
					),
					array(
						'type'       => 'textfield',
						'heading'    => esc_html__( 'Text after number', 'clerina' ),
						'param_name' => 'text_after',
						'value'      => '',
					),
					array(
						'type'       => 'textarea',
						'heading'    => esc_html__( 'Description', 'clerina' ),
						'param_name' => 'desc',
						'value'      => '',
					),
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__( 'Duration', 'clerina' ),
						'param_name'  => 'duration',
						'description' => esc_html__( 'Add number ex: 1, 2, 3... (1 -> 1s) ', 'clerina' ),
					),
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__( 'Extra class name', 'clerina' ),
						'param_name'  => 'el_class',
						'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'clerina' ),
						'group'       => esc_html__( 'Add class', 'clerina' ),
					),
				),
			)
		);

		// List
		vc_map(
			array(
				'name'     => esc_html__( 'Clerina List Nav', 'clerina' ),
				'base'     => 'cleanpro_list',
				'class'    => '',
				'category' => esc_html__( 'Clerina', 'clerina' ),
				'params'   => array(
					array(
						'type'       => 'dropdown',
						'heading'    => esc_html__( 'Theme', 'clerina' ),
						'param_name' => 'theme',
						'value'      => array(
							esc_html__( 'Dark', 'clerina' )  => 'dark',
							esc_html__( 'Light', 'clerina' ) => 'light',
						),
					),
					array(
						'heading'     => esc_html__( 'Icon library', 'clerina' ),
						'description' => esc_html__( 'Select icon library.', 'clerina' ),
						'param_name'  => 'icon_library',
						'type'        => 'dropdown',
						'value'       => array(
							esc_html__( 'Dot', 'clerina' )          => '0',
							esc_html__( 'Font Awesome', 'clerina' ) => '1',
							esc_html__( 'Svg icon', 'clerina' )     => '2',
						),
					),
					array(
						'heading'     => esc_html__( 'Icon', 'clerina' ),
						'description' => esc_html__( 'Pick an icon from library.', 'clerina' ),
						'type'        => 'iconpicker',
						'param_name'  => 'icon_fontawesome',
						'value'       => 'fa fa-adjust',
						'settings'    => array(
							'emptyIcon'    => false,
							'iconsPerPage' => 4000,
						),
						'dependency'  => array(
							'element' => 'icon_library',
							'value'   => '1',
						),
					),
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__( 'Svg icon', 'clerina' ),
						'param_name'  => 'name_svg',
						'value'       => '',
						'description' => esc_html__( 'Enter name svg icon from cleanpro theme' ),
						'dependency'  => array(
							'element' => 'icon_library',
							'value'   => '2',
						),
					),
					array(
						'type'       => 'colorpicker',
						'heading'    => esc_html__( 'Icon Color', 'clerina' ),
						'param_name' => 'icon_color',
						'value'      => '',
						'dependency' => array(
							'element' => 'icon_library',
							'value'   => array( '1', '2' ),
						),
					),
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__( 'Icon size', 'clerina' ),
						'param_name'  => 'icon_size',
						'description' => esc_html__( 'Font size for icon example: 15px,22px.', 'clerina' ),
						'dependency'  => array(
							'element' => 'icon_library',
							'value'   => array( '1', '2' ),
						),

					),
					array(
						'type'       => 'param_group',
						'heading'    => esc_html__( 'Element Carousel', 'clerina' ),
						'value'      => '',
						'param_name' => 'setting',
						'params'     => array(
							array(
								'type'       => 'textfield',
								'heading'    => esc_html__( 'Li', 'clerina' ),
								'param_name' => 'content_list',
							),
							array(
								'type'       => 'colorpicker',
								'heading'    => esc_html__( 'Li Color', 'clerina' ),
								'param_name' => 'li_color',
								'value'      => '',
							),
							array(
								'heading'    => esc_html__( 'URL (Link)', 'clerina' ),
								'type'       => 'vc_link',
								'param_name' => 'link',
							),
						),
					)
				),
			)
		);

		// Button
		vc_map(
			array(
				'name'     => esc_html__( 'Clerina Button', 'clerina' ),
				'base'     => 'cleanpro_button',
				'class'    => '',
				'category' => esc_html__( 'Clerina', 'clerina' ),
				'params'   => array(

					array(
						'type'        => 'textfield',
						'heading'     => esc_html__( 'Text Button', 'clerina' ),
						'param_name'  => 'text_bt',
						'value'       => '',
						'admin_label' => true,
					),
					array(
						'heading'    => esc_html__( 'URL (Link)', 'clerina' ),
						'type'       => 'vc_link',
						'param_name' => 'link',
					),
					array(
						'type'       => 'textfield',
						'heading'    => esc_html__( 'Button Border', 'clerina' ),
						'param_name' => 'bt_border',
						'value'      => '',
						'group'      => esc_html__( 'Setting', 'clerina' )
					),
					array(
						'type'       => 'colorpicker',
						'heading'    => esc_html__( 'Button Border Color', 'clerina' ),
						'param_name' => 'bt_border_color',
						'value'      => '',
						'group'      => esc_html__( 'Setting', 'clerina' )
					),
					array(
						'type'       => 'dropdown',
						'heading'    => esc_html__( 'Button Border Style', 'clerina' ),
						'param_name' => 'bt_border_style',
						'value'      => array(
							esc_html__( 'Hidden', 'clerina' ) => 'hidden',
							esc_html__( 'Solid', 'clerina' )  => 'solid',
							esc_html__( 'Dotted', 'clerina' ) => 'dotted',
							esc_html__( 'Dashed', 'clerina' ) => 'dashed',
							esc_html__( 'Double', 'clerina' ) => 'double',
						),
						'group'      => esc_html__( 'Setting', 'clerina' )
					),
					array(
						'type'       => 'colorpicker',
						'heading'    => esc_html__( 'Button Background Color', 'clerina' ),
						'param_name' => 'bt_bg_color',
						'value'      => '',
						'group'      => esc_html__( 'Setting', 'clerina' )
					),
					array(
						'type'       => 'colorpicker',
						'heading'    => esc_html__( 'Button Text Color', 'clerina' ),
						'param_name' => 'bt_text_color',
						'value'      => '',
						'group'      => esc_html__( 'Setting', 'clerina' )
					),
					array(
						'type'       => 'textfield',
						'heading'    => esc_html__( 'Border radius', 'clerina' ),
						'param_name' => 'border_radius',
						'value'      => '',
						'group'      => esc_html__( 'Setting', 'clerina' )
					),
					array(
						'type'       => 'textfield',
						'heading'    => esc_html__( 'Padding', 'clerina' ),
						'param_name' => 'padding',
						'value'      => '',
						'group'      => esc_html__( 'Setting', 'clerina' )
					),
					array(
						'type'       => 'dropdown',
						'heading'    => esc_html__( 'Button Hover', 'clerina' ),
						'param_name' => 'bt_hover',
						'value'      => array(
							esc_html__( 'Style 1', 'clerina' ) => '1',
							esc_html__( 'Style 2', 'clerina' ) => '2',
							esc_html__( 'Style 3', 'clerina' ) => '3',
						),
					),
					array(
						'type'       => 'dropdown',
						'heading'    => esc_html__( 'Button Align', 'clerina' ),
						'param_name' => 'bt_align',
						'value'      => array(
							esc_html__( 'Left', 'clerina' )   => 'left',
							esc_html__( 'Center', 'clerina' ) => 'center',
							esc_html__( 'Right', 'clerina' )  => 'right',
						),
					),
					array(
						'heading'     => esc_html__( 'Icon library', 'clerina' ),
						'description' => esc_html__( 'Select icon library.', 'clerina' ),
						'param_name'  => 'icon_type',
						'type'        => 'dropdown',
						'value'       => array(
							esc_html__( 'None', 'clerina' )         => 'none',
							esc_html__( 'Font Awesome', 'clerina' ) => 'fa_font',
							esc_html__( 'Svg icon', 'clerina' )     => 'svg_icon',
							esc_html__( 'Custom image', 'clerina' ) => 'image',
						),
					),
					array(
						'heading'     => esc_html__( 'Icon', 'clerina' ),
						'description' => esc_html__( 'Pick an icon from library.', 'clerina' ),
						'type'        => 'iconpicker',
						'param_name'  => 'icon_fontawesome',
						'value'       => 'fa fa-adjust',
						'settings'    => array(
							'emptyIcon'    => false,
							'iconsPerPage' => 4000,
						),
						'dependency'  => array(
							'element' => 'icon_type',
							'value'   => 'fa_font',
						),
					),
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__( 'Svg icon', 'clerina' ),
						'param_name'  => 'value_icon',
						'value'       => '',
						'description' => esc_html__( 'Enter name svg icon from cleanpro theme' ),
						'dependency'  => array(
							'element' => 'icon_type',
							'value'   => 'svg_icon',
						),
					),
					array(
						'heading'     => esc_html__( 'Icon Image', 'clerina' ),
						'description' => esc_html__( 'Upload icon image', 'clerina' ),
						'type'        => 'attach_image',
						'param_name'  => 'image',
						'value'       => '',
						'dependency'  => array(
							'element' => 'icon_type',
							'value'   => 'image',
						),
					),
					array(
						'heading'     => esc_html__( 'Image size', 'clerina' ),
						'description' => esc_html__( 'Enter image size. Example: "thumbnail", "medium", "large", "full" or other sizes defined by current theme. Alternatively enter image size in pixels: 200x100 (Width x Height). Leave empty to use "thumbnail" size.', 'clerina' ),
						'type'        => 'textfield',
						'param_name'  => 'image_size',
						'value'       => '',
						'dependency'  => array(
							'element' => 'icon_type',
							'value'   => 'image',
						),
					),
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__( 'Extra class name', 'clerina' ),
						'param_name'  => 'el_class',
						'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'clerina' ),
						'group'       => esc_html__( 'Setting', 'clerina' )
					),
				),
			)
		);

		// Portfolio Attribute
		vc_map(
			array(
				'name'     => esc_html__( 'Clerina Portfolio Attribute', 'clerina' ),
				'base'     => 'cleanpro_portfolio_attribute',
				'class'    => '',
				'category' => esc_html__( 'Clerina', 'clerina' ),
				'params'   => array(
					array(
						'type'       => 'textfield',
						'heading'    => esc_html__( 'Client', 'clerina' ),
						'param_name' => 'client',
					),
					array(
						'heading'    => esc_html__( 'Work Type', 'clerina' ),
						'type'       => 'textfield',
						'param_name' => 'work_type',
					),
					array(
						'heading'    => esc_html__( 'Date', 'clerina' ),
						'type'       => 'textfield',
						'param_name' => 'date',
					),
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__( 'Extra class name', 'clerina' ),
						'param_name'  => 'el_class',
						'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'clerina' ),
						'group'       => esc_html__( 'Setting', 'clerina' )
					),
				)
			)
		);

		// Service Carousel
		vc_map(
			array(
				'name'     => esc_html__( 'Clerina Services Carousel', 'clerina' ),
				'base'     => 'cleanpro_service_carousel',
				'class'    => '',
				'category' => esc_html__( 'Clerina', 'clerina' ),
				'params'   => array(
					array(
						'type'        => 'dropdown',
						'heading'     => esc_html__( 'Items per row', 'clerina' ),
						'param_name'  => 'column',
						'value'       => array(
							esc_html__( '2 items', 'clerina' ) => '2',
							esc_html__( '3 items', 'clerina' ) => '3',
							esc_html__( '4 items', 'clerina' ) => '4',
							esc_html__( '5 items', 'clerina' ) => '5',
							esc_html__( '6 items', 'clerina' ) => '6',
						),
						'admin_label' => true,
					),
					array(
						'type'       => 'textfield',
						'heading'    => esc_html__( 'Service items show at most', 'clerina' ),
						'param_name' => 'value_item',
						'value'      => '',
					),
					array(
						'type'       => 'textfield',
						'heading'    => esc_html__( 'Excerpt content', 'clerina' ),
						'param_name' => 'excerpt_content',
						'value'      => '',
					),
					array(
						'type'       => 'checkbox',
						'heading'    => esc_html__( 'Show Nav', 'clerina' ),
						'param_name' => 'nav',
						'value'      => array( esc_html__( 'Yes', 'clerina' ) => '1' ),
					),
					array(
						'type'       => 'checkbox',
						'heading'    => esc_html__( 'Show Dot', 'clerina' ),
						'param_name' => 'dots',
						'value'      => array( esc_html__( 'Yes', 'clerina' ) => '1' ),
					),
					array(
						'type'        => 'checkbox',
						'heading'     => esc_html__( 'Autoplay', 'clerina' ),
						'param_name'  => 'autoplay',
						'value'       => array( esc_html__( 'Yes', 'clerina' ) => 'yes' ),
						'description' => esc_html__( 'If "YES" Enable autoplay', 'clerina' ),
						'dependency'  => array(
							'element' => 'type',
						),
					),
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__( 'Autoplay speed', 'clerina' ),
						'param_name'  => 'autoplay_speed',
						'value'       => '2000',
						'description' => esc_html__( 'Set auto play speed (in ms).', 'clerina' ),
						'dependency'  => array(
							'element' => 'autoplay',
							'value'   => array( 'yes' ),
						),
					),
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__( 'Extra class name', 'clerina' ),
						'param_name'  => 'el_class',
						'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'clerina' ),
					),
				),

			)
		);

		// Address box
		vc_map(
			array(
				'name'     => esc_html__( 'Clerina Address box', 'clerina' ),
				'base'     => 'cleanpro_address_box',
				'class'    => '',
				'category' => esc_html__( 'Clerina', 'clerina' ),
				'params'   => array(
					array(
						'type'       => 'param_group',
						'heading'    => esc_html__( 'Notes', 'clerina' ),
						'value'      => '',
						'param_name' => 'setting',
						'params'     => array(
							array(
								'heading'     => esc_html__( 'Title', 'clerina' ),
								'type'        => 'textfield',
								'param_name'  => 'title',
								'admin_label' => true,
							),
							array(
								'heading'    => esc_html__( 'Description', 'clerina' ),
								'type'       => 'textarea',
								'param_name' => 'desc',
							),
						),
					),
					array(
						'type'       => 'param_group',
						'heading'    => esc_html__( 'Socials', 'clerina' ),
						'value'      => '',
						'param_name' => 'setting_icon',
						'params'     => array(
							array(
								'heading'     => esc_html__( 'Icon', 'clerina' ),
								'description' => esc_html__( 'Select icon from library.', 'clerina' ),
								'type'        => 'iconpicker',
								'param_name'  => 'icon_fontawesome',
								'value'       => 'fa fa-adjust',
								'settings'    => array(
									'emptyIcon'    => false,
									'iconsPerPage' => 4000,
								),
							),
							array(
								'heading'    => esc_html__( 'URL (Link)', 'clerina' ),
								'type'       => 'vc_link',
								'param_name' => 'link',
							),
						),
					),
					array(
						'type'       => 'colorpicker',
						'heading'    => esc_html__( 'Background color box', 'clerina' ),
						'param_name' => 'bk_box',
						'value'      => '',
						'group'      => esc_html__( 'Style', 'clerina' ),
					),
					array(
						'type'       => 'colorpicker',
						'heading'    => esc_html__( 'Title color', 'clerina' ),
						'param_name' => 'color_tit',
						'value'      => '',
						'group'      => esc_html__( 'Style', 'clerina' ),
					),
					array(
						'type'       => 'colorpicker',
						'heading'    => esc_html__( 'Description color', 'clerina' ),
						'param_name' => 'color_desc',
						'value'      => '',
						'group'      => esc_html__( 'Style', 'clerina' ),
					),
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__( 'Extra class name', 'clerina' ),
						'param_name'  => 'el_class',
						'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'clerina' ),
						'group'       => esc_html__( 'Add class', 'clerina' ),
					),
				),
			)
		);

		//Icon box
		vc_map(
			array(
				'name'     => esc_html__( 'Clerina Icon Box', 'clerina' ),
				'base'     => 'cleanpro_icon_box',
				'class'    => '',
				'category' => esc_html__( 'Clerina', 'clerina' ),
				'params'   => array(
					array(
						'type'        => 'dropdown',
						'heading'     => esc_html__( 'Style', 'clerina' ),
						'param_name'  => 'style',
						'value'       => array(
							esc_html__( 'Style 1', 'clerina' ) => '1',
							esc_html__( 'Style 2', 'clerina' ) => '2',
							esc_html__( 'Style 3', 'clerina' ) => '3',
						),
						'admin_label' => true,
					),
					array(
						'type'       => 'dropdown',
						'heading'    => esc_html__( 'Row', 'clerina' ),
						'param_name' => 'row',
						'value'      => array(
							esc_html__( '1 Row', 'clerina' ) => '1',
							esc_html__( '2 Row', 'clerina' ) => '2',
						),
						'dependency' => array(
							'element' => 'style',
							'value'   => '3',
						),
					),

					array(
						'type'       => 'dropdown',
						'heading'    => esc_html__( 'Align', 'clerina' ),
						'param_name' => 'align',
						'value'      => array(
							esc_html__( 'Left', 'clerina' )  => 'left',
							esc_html__( 'Right', 'clerina' ) => 'right',
						),
						'dependency' => array(
							'element' => 'style',
							'value'   => '1',
						),
					),

					array(
						'type'       => 'dropdown',
						'heading'    => esc_html__( 'Theme', 'clerina' ),
						'param_name' => 'theme',
						'value'      => array(
							esc_html__( 'Light', 'clerina' ) => 'light',
							esc_html__( 'Dark', 'clerina' )  => 'dark',
						),
					),

					array(
						'heading'    => esc_html__( 'Title', 'clerina' ),
						'type'       => 'vc_link',
						'param_name' => 'title',

					),
					array(
						'type'       => 'colorpicker',
						'heading'    => esc_html__( 'Title Color', 'clerina' ),
						'param_name' => 'text_color',
						'value'      => '',
						'group'      => esc_html__( 'Setting', 'clerina' )
					),
					array(
						'type'       => 'textarea_html',
						'heading'    => esc_html__( 'Description', 'clerina' ),
						'param_name' => 'content',
						'value'      => '',
					),
					array(
						'heading'     => esc_html__( 'Icon library', 'clerina' ),
						'description' => esc_html__( 'Select icon library.', 'clerina' ),
						'param_name'  => 'icon_type',
						'type'        => 'dropdown',
						'value'       => array(
							esc_html__( 'Font Awesome', 'clerina' ) => 'fa_font',
							esc_html__( 'Elegant icon', 'clerina' ) => 'elegant_icon',
							esc_html__( 'Svg icon', 'clerina' )     => 'svg_icon',
							esc_html__( 'Custom image', 'clerina' ) => 'image',
						),
					),
					array(
						'heading'     => esc_html__( 'Icon', 'clerina' ),
						'description' => esc_html__( 'Pick an icon from library.', 'clerina' ),
						'type'        => 'iconpicker',
						'param_name'  => 'icon_fontawesome',
						'value'       => 'fa fa-adjust',
						'settings'    => array(
							'emptyIcon'    => false,
							'iconsPerPage' => 4000,
						),
						'dependency'  => array(
							'element' => 'icon_type',
							'value'   => 'fa_font',
						),
					),
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__( 'Elegant icon', 'clerina' ),
						'param_name'  => 'value_elegant_icon',
						'value'       => '',
						'description' => esc_html__( 'Enter name elegantIcons icon from cleanpro theme' ),
						'dependency'  => array(
							'element' => 'icon_type',
							'value'   => 'elegant_icon',
						),
					),
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__( 'Svg icon', 'clerina' ),
						'param_name'  => 'value_icon',
						'value'       => '',
						'description' => esc_html__( 'Enter name svg icon from cleanpro theme' ),
						'dependency'  => array(
							'element' => 'icon_type',
							'value'   => 'svg_icon',
						),
					),
					array(
						'heading'     => esc_html__( 'Icon Image', 'clerina' ),
						'description' => esc_html__( 'Upload icon image', 'clerina' ),
						'type'        => 'attach_image',
						'param_name'  => 'image',
						'value'       => '',
						'dependency'  => array(
							'element' => 'icon_type',
							'value'   => 'image',
						),
					),
					array(
						'heading'     => esc_html__( 'Image size', 'clerina' ),
						'description' => esc_html__( 'Enter image size. Example: "thumbnail", "medium", "large", "full" or other sizes defined by current theme. Alternatively enter image size in pixels: 200x100 (Width x Height). Leave empty to use "thumbnail" size.', 'clerina' ),
						'type'        => 'textfield',
						'param_name'  => 'image_size',
						'value'       => '',
						'dependency'  => array(
							'element' => 'icon_type',
							'value'   => 'image',
						),
					),
					array(
						'type'       => 'colorpicker',
						'heading'    => esc_html__( 'Icon Color', 'clerina' ),
						'param_name' => 'icon_color',
						'value'      => '',
						'dependency' => array(
							'element' => 'icon_type',
							'value'   => array( 'fa_font', 'elegant_icon', 'svg_icon' ),
						),
					),

					array(
						'type'        => 'textfield',
						'heading'     => esc_html__( 'Icon size (px)', 'clerina' ),
						'param_name'  => 'icon_size',
						'description' => esc_html__( 'Font size for icon example: 15px,22px.', 'clerina' ),
						'dependency'  => array(
							'element' => 'icon_type',
							'value'   => array( 'fa_font', 'elegant_icon', 'svg_icon' ),
						),
					),
					array(
						'type'       => 'textfield',
						'heading'    => esc_html__( 'Icon Link', 'clerina' ),
						'param_name' => 'icon_link',
						'value'      => '',
					),
					array(
						'type'       => 'checkbox',
						'heading'    => esc_html__( 'Read more', 'clerina' ),
						'param_name' => 'readmore',
						'value'      => '',
					),

					array(
						'type'        => 'textfield',
						'heading'     => esc_html__( 'Padding', 'clerina' ),
						'param_name'  => 'padding',
						'description' => esc_html__( 'Icon box padding. Example: 15px 22px or 15px', 'clerina' ),
						'group'       => esc_html__( 'Setting', 'clerina' )
					),
					array(
						'heading'     => esc_html__( 'Background Image', 'clerina' ),
						'description' => esc_html__( 'Upload image background', 'clerina' ),
						'type'        => 'attach_image',
						'param_name'  => 'background_image',
						'value'       => '',
						'dependency'  => array(
							'element' => 'style',
							'value'   => '3',
						),
						'group'       => esc_html__( 'Setting', 'clerina' ),
					),
					array(
						'heading'    => esc_html__( 'Background size', 'clerina' ),
						'param_name' => 'background_size',
						'type'       => 'dropdown',
						'value'      => array(
							esc_html__( 'Auto', 'clerina' )    => 'auto',
							esc_html__( 'Cover', 'clerina' )   => 'cover',
							esc_html__( 'Contain', 'clerina' ) => 'contain',
						),
						'dependency' => array(
							'element' => 'style',
							'value'   => '3',
						),
						'group'      => esc_html__( 'Setting', 'clerina' ),
					),
					array(
						'heading'    => esc_html__( 'Background Color', 'clerina' ),
						'param_name' => 'background_color',
						'type'       => 'colorpicker',
						'dependency' => array(
							'element' => 'style',
							'value'   => '3',
						),
						'group'      => esc_html__( 'Setting', 'clerina' ),
					),
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__( 'Extra class name', 'clerina' ),
						'param_name'  => 'el_class',
						'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'clerina' ),
						'group'       => esc_html__( 'Setting', 'clerina' )
					),
				),
			)

		);

		// Portfolio Carousel
		vc_map(
			array(
				'name'     => esc_html__( 'Clerina Portfolio', 'clerina' ),
				'base'     => 'cleanpro_portfolio',
				'class'    => '',
				'category' => esc_html__( 'Clerina', 'clerina' ),
				'params'   => array(

					array(
						'type'        => 'checkbox',
						'heading'     => esc_html__( 'Show Nav Filter', 'clerina' ),
						'param_name'  => 'filter',
						'value'       => array( esc_html__( 'Yes', 'clerina' ) => '1' ),
						'description' => esc_html__( 'If "YES" Enable Nav Filter', 'clerina' ),
					),
					array(
						'type'       => 'dropdown',
						'heading'    => esc_html__( 'Nav Filter Color', 'clerina' ),
						'param_name' => 'nav_color',
						'value'      => array(
							esc_html__( 'Light', 'clerina' ) => 'light',
							esc_html__( 'Dark', 'clerina' )  => 'dark',
						),
						'dependency' => array(
							'element' => 'filter',
							'value'   => '1',
						),
					),
					array(
						'type'        => 'dropdown',
						'heading'     => esc_html__( 'Version', 'clerina' ),
						'param_name'  => 'version',
						'value'       => array(
							esc_html__( 'Dark', 'clerina' )  => 'dark',
							esc_html__( 'Light', 'clerina' ) => 'light',
						),
						'description' => esc_html__( 'Work on Style 1 and 2', 'clerina' ),
					),
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__( 'Number of Portfolio', 'clerina' ),
						'param_name'  => 'number',
						'value'       => 'All',
						'description' => esc_html__( 'Set numbers of Portfolio you want to display. Set -1 to display all portfolio', 'clerina' ),
					),
					array(
						'type'        => 'checkbox',
						'heading'     => esc_html__( 'Autoplay', 'clerina' ),
						'param_name'  => 'autoplay',
						'value'       => array( esc_html__( 'Yes', 'clerina' ) => 'yes' ),
						'description' => esc_html__( 'If "YES" Enable autoplay', 'clerina' ),
					),
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__( 'Autoplay speed', 'clerina' ),
						'param_name'  => 'autoplay_speed',
						'value'       => '800',
						'description' => esc_html__( 'Set auto play speed (in ms).', 'clerina' ),
						'dependency'  => array(
							'element' => 'autoplay',
							'value'   => array( 'yes' ),
						),
					),
					array(
						'type'        => 'checkbox',
						'heading'     => esc_html__( 'Show Navigation', 'clerina' ),
						'param_name'  => 'nav',
						'value'       => array( esc_html__( 'Yes', 'clerina' ) => 'yes' ),
						'description' => esc_html__( 'If "YES" Enable navigation', 'clerina' ),
					),
					array(
						'type'        => 'checkbox',
						'heading'     => esc_html__( 'Show Dots', 'clerina' ),
						'param_name'  => 'dot',
						'value'       => array( esc_html__( 'Yes', 'clerina' ) => 'yes' ),
						'description' => esc_html__( 'If "YES" Enable dots', 'clerina' ),

					),
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__( 'Extra class name', 'clerina' ),
						'param_name'  => 'el_class',
						'value'       => '',
						'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'clerina' ),
					),
				),
			)
		);

		//Image Box Carousel
		vc_map(
			array(
				'name'     => esc_html__( 'Clerina Image Box Carousel', 'clerina' ),
				'base'     => 'cleanpro_image_box_carousel',
				'class'    => '',
				'category' => esc_html__( 'Clerina', 'clerina' ),
				'params'   => array(
					array(
						'type'       => 'param_group',
						'heading'    => esc_html__( 'List image box', 'clerina' ),
						'value'      => '',
						'param_name' => 'setting',
						'params'     => array(
							array(
								'heading'    => esc_html__( 'Image', 'clerina' ),
								'type'       => 'attach_image',
								'param_name' => 'image',
							),
							array(
								'heading'    => esc_html__( 'URL (Link)', 'clerina' ),
								'type'       => 'vc_link',
								'param_name' => 'link',
							),
							array(
								'type'       => 'textfield',
								'heading'    => esc_html__( 'Title', 'clerina' ),
								'param_name' => 'title',
							),
							array(
								'type'       => 'textarea',
								'heading'    => esc_html__( 'Description', 'clerina' ),
								'param_name' => 'description',
								'value'      => '',
							),
						),
					),
					array(
						'type'       => 'dropdown',
						'heading'    => esc_html__( 'Columns', 'clerina' ),
						'param_name' => 'columns',
						'value'      => array(
							esc_html__( '1', 'clerina' ) => '1',
							esc_html__( '2', 'clerina' ) => '2',
							esc_html__( '3', 'clerina' ) => '3',
							esc_html__( '4', 'clerina' ) => '4',
							esc_html__( '5', 'clerina' ) => '5',
						),
					),
					array(
						'heading'     => esc_html__( 'Image size', 'clerina' ),
						'description' => esc_html__( 'Enter image size. Example: "thumbnail", "medium", "large", "full" or other sizes defined by current theme. Alternatively enter image size in pixels: 200x100 (Width x Height). Leave empty to use "thumbnail" size.', 'clerina' ),
						'type'        => 'textfield',
						'param_name'  => 'image_size',
						'value'       => '',
					),
					array(
						'type'       => 'checkbox',
						'heading'    => esc_html__( 'Show nav', 'clerina' ),
						'param_name' => 'nav',
						'value'      => array( esc_html__( 'Yes', 'clerina' ) => '1' ),
					),
					array(
						'type'       => 'checkbox',
						'heading'    => esc_html__( 'Show Dots', 'clerina' ),
						'param_name' => 'dots',
						'value'      => array( esc_html__( 'Yes', 'clerina' ) => '1' ),
					),
					array(
						'type'       => 'checkbox',
						'heading'    => esc_html__( 'Autoplay', 'clerina' ),
						'param_name' => 'autoplay',
						'value'      => array( esc_html__( 'Yes', 'clerina' ) => '1' ),
						'dependency' => array(
							'element' => 'type',
							'value'   => array( esc_html__( 'Yes', 'clerina' ) => '1' ),
						),
					),
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__( 'Autoplay speed', 'clerina' ),
						'param_name'  => 'speed',
						'value'       => '2000',
						'description' => esc_html__( 'Set auto play speed (in ms).', 'clerina' ),
						'dependency'  => array(
							'element' => 'autoplay',
							'value'   => array( '1' ),
						),
					),
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__( 'Extra class name', 'clerina' ),
						'param_name'  => 'el_class',
						'value'       => '',
						'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'clerina' ),
					),
				),
			)
		);

		// Video Banner
		vc_map(
			array(
				'name'     => esc_html__( 'Clerina Video Banner', 'clerina' ),
				'base'     => 'cleanpro_video_banner',
				'category' => esc_html__( 'Clerina', 'clerina' ),
				'params'   => array(
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__( 'Video file URL', 'clerina' ),
						'description' => esc_html__( 'Only support YouTube and Vimeo', 'clerina' ),
						'param_name'  => 'video',
						'value'       => '',
					),
					array(
						'type'       => 'dropdown',
						'heading'    => esc_html__( 'Style', 'clerina' ),
						'param_name' => 'video_style',
						'value'      => array(
							esc_html__( 'Full', 'clerina' ) => 'full',
							esc_html__( 'Box', 'clerina' )  => 'box',
						),
					),
					array(
						'type'       => 'textfield',
						'heading'    => esc_html__( 'Min Height(px)', 'clerina' ),
						'param_name' => 'min_height',
						'value'      => '500',
					),
					array(
						'type'       => 'attach_image',
						'heading'    => esc_html__( 'Video Background Image', 'clerina' ),
						'param_name' => 'image',
					),
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__( 'Image size', 'clerina' ),
						'param_name'  => 'image_size',
						'description' => esc_html__( 'Enter image size (Example: "thumbnail", "medium", "large", "full" or other sizes defined by theme). Alternatively enter size in pixels (Example: 200x100 (Width x Height)).', 'clerina' ),
					),
					array(
						'type'       => 'textfield',
						'heading'    => esc_html__( 'Extra Class', 'clerina' ),
						'param_name' => 'el_class',
						'value'      => '',
					),
				),
			)
		);

		// Coming soon
		vc_map(
			array(
				'name'     => esc_html__( 'Clerina Coming Soon', 'clerina' ),
				'base'     => 'cleanpro_coming_soon',
				'class'    => '',
				'category' => esc_html__( 'Clerina', 'clerina' ),
				'params'   => array(
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__( 'Date', 'clerina' ),
						'param_name'  => 'date',
						'value'       => '',
						'description' => esc_html__( 'Enter the date by format: YYYY-MM-DD', 'clerina' ),
					),
					array(
						'heading'    => esc_html__( 'Text Color', 'clerina' ),
						'param_name' => 'text_color',
						'type'       => 'dropdown',
						'value'      => array(
							esc_html__( 'Dark', 'clerina' )  => 'dark',
							esc_html__( 'Light', 'clerina' ) => 'light',
						),
					),
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__( 'Extra class name', 'clerina' ),
						'param_name'  => 'el_class',
						'value'       => '',
						'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file . ', 'clerina' ),
					),
				),
			)
		);

		// Social
		vc_map(
			array(
				'name'     => esc_html__( 'Clerina Socials', 'clerina' ),
				'base'     => 'cleanpro_socials',
				'class'    => '',
				'category' => esc_html__( 'Clerina', 'clerina' ),
				'params'   => array(
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__( 'Extra class name', 'clerina' ),
						'param_name'  => 'el_class',
						'value'       => '',
						'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file . ', 'clerina' ),
					),
					array(
						'type'       => 'param_group',
						'heading'    => esc_html__( 'Socials', 'clerina' ),
						'value'      => '',
						'param_name' => 'socials',
						'params'     => array(
							array(
								'heading'     => esc_html__( 'Icon library', 'sober' ),
								'description' => esc_html__( 'Select icon library.', 'sober' ),
								'param_name'  => 'icon_type',
								'type'        => 'dropdown',
								'value'       => array(
									esc_html__( 'Font Awesome', 'sober' ) => 'fontawesome',
									esc_html__( 'Custom Image', 'sober' ) => 'image',
								),
							),
							array(
								'heading'     => esc_html__( 'Icon', 'sober' ),
								'description' => esc_html__( 'Select icon from library.', 'sober' ),
								'type'        => 'iconpicker',
								'param_name'  => 'icon_fontawesome',
								'value'       => 'fa fa-adjust',
								'settings'    => array(
									'emptyIcon'    => false,
									'iconsPerPage' => 4000,
								),
								'dependency'  => array(
									'element' => 'icon_type',
									'value'   => 'fontawesome',
								),
							),
							array(
								'heading'     => esc_html__( 'Icon Image', 'sober' ),
								'description' => esc_html__( 'Upload icon image', 'sober' ),
								'type'        => 'attach_image',
								'param_name'  => 'image',
								'value'       => '',
								'dependency'  => array(
									'element' => 'icon_type',
									'value'   => 'image',
								),
							),
							array(
								'type'        => 'textfield',
								'heading'     => esc_html__( 'Link Socials', 'clerina' ),
								'param_name'  => 'link',
								'value'       => '',
								'description' => esc_html__( 'Enter link for this social', 'clerina' ),
							),
						),
					),
				),
			)
		);

		// GG maps
		vc_map(
			array(
				'name'     => esc_html__( 'Clerina Google Maps', 'clerina' ),
				'base'     => 'cleanpro_gmap',
				'category' => esc_html__( 'Clerina', 'clerina' ),
				'params'   => array(
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__( 'Api Key', 'clerina' ),
						'param_name'  => 'api_key',
						'value'       => '',
						'description' => sprintf( __( 'Please go to <a href="%s">Google Maps APIs</a> to get a key', 'clerina' ), esc_url( 'https://developers.google.com/maps/documentation/javascript/get-api-key#get-an-api-key' ) ),
					),
					array(
						'type'        => 'attach_image',
						'heading'     => esc_html__( 'Marker', 'clerina' ),
						'param_name'  => 'marker',
						'value'       => '',
						'description' => esc_html__( 'Choose an image from media library', 'clerina' ),
					),
					array(
						'type'       => 'param_group',
						'heading'    => esc_html__( 'Address Infomation', 'clerina' ),
						'value'      => '',
						'param_name' => 'info',
						'params'     => array(
							array(
								'type'        => 'textfield',
								'heading'     => esc_html__( 'Address', 'clerina' ),
								'param_name'  => 'address',
								'admin_label' => true,
							),
							array(
								'type'       => 'textarea',
								'heading'    => esc_html__( 'Details', 'clerina' ),
								'param_name' => 'details',
							),
						),
					),
					array(
						'type'       => 'textfield',
						'heading'    => esc_html__( 'Width(px)', 'clerina' ),
						'param_name' => 'width',
						'value'      => '',
					),
					array(
						'type'       => 'textfield',
						'heading'    => esc_html__( 'Height(px)', 'clerina' ),
						'param_name' => 'height',
						'value'      => '500',
					),
					array(
						'type'       => 'textfield',
						'heading'    => esc_html__( 'Zoom', 'clerina' ),
						'param_name' => 'zoom',
						'value'      => '13',
					),
					array(
						'type'       => 'dropdown',
						'heading'    => esc_html__( 'Style', 'clerina' ),
						'param_name' => 'style',
						'value'      => array(
							esc_html__( 'Style 1', 'clerina' ) => '1',
							esc_html__( 'Style 2', 'clerina' ) => '2',
						),
					),

					array(
						'type'       => 'colorpicker',
						'heading'    => esc_html__( 'Map Colors', 'clerina' ),
						'param_name' => 'map_color',
						'value'      => '#efba2c',
						'dependency' => array(
							'element' => 'style',
							'value'   => array( '1' ),
						),
					),
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__( 'Extra class name', 'clerina' ),
						'param_name'  => 'el_class',
						'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file . ', 'clerina' ),
					),
				),
			)
		);

		// Blog Grid
		vc_map(
			array(
				'name'     => esc_html__( 'Clerina Blog Section', 'clerina' ),
				'base'     => 'cleanpro_blog_section',
				'class'    => '',
				'category' => esc_html__( 'Clerina', 'clerina' ),
				'params'   => array(
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__( 'Number of Posts', 'clerina' ),
						'param_name'  => 'number',
						'value'       => 'All',
						'description' => esc_html__( 'Set numbers of Posts you want to display. Set -1 to display all posts', 'clerina' ),
					),
					array(
						'type'       => 'textfield',
						'heading'    => esc_html__( 'Button Text', 'clerina' ),
						'param_name' => 'btn_text',
						'value'      => esc_html__( 'View More', 'clerina' ),
						'dependency' => array(
							'element' => 'type',
							'value'   => array( 'grid' ),
						),
					),
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__( 'Extra class name', 'clerina' ),
						'param_name'  => 'el_class',
						'value'       => '',
						'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'clerina' ),
					),
				),
			)
		);

		// Testimonials Carousel
		vc_map(
			array(
				'name'     => esc_html__( 'Clerina Testimonials Carousel', 'clerina' ),
				'base'     => 'cleanpro_testimonials_carousel',
				'class'    => '',
				'category' => esc_html__( 'Clerina', 'clerina' ),
				'params'   => array(
					array(
						'type'       => 'dropdown',
						'heading'    => esc_html__( 'Style', 'clerina' ),
						'param_name' => 'style',
						'value'      => array(
							esc_html__( 'Style 1', 'clerina' ) => '1',
							esc_html__( 'Style 2', 'clerina' ) => '2',
						),
					),
					array(
						'type'       => 'dropdown',
						'heading'    => esc_html__( 'Background', 'clerina' ),
						'param_name' => 'background',
						'value'      => array(
							esc_html__( 'Dark', 'clerina' )  => '1',
							esc_html__( 'Light', 'clerina' ) => '2',
						),
						'dependency' => array(
							'element' => 'style',
							'value'   => array( '1' ),
						),
					),
					array(
						'type'        => 'checkbox',
						'heading'     => esc_html__( 'Rating', 'clerina' ),
						'param_name'  => 'rating',
						'value'       => array( esc_html__( 'Yes', 'clerina' ) => 'yes' ),
						'description' => esc_html__( 'If "YES" Disable Rating', 'clerina' ),
					),
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__( 'Number of Posts', 'clerina' ),
						'param_name'  => 'number',
						'value'       => 'All',
						'description' => esc_html__( 'Set numbers of Posts you want to display. Set -1 to display all posts', 'clerina' ),
					),
					array(
						'type'        => 'checkbox',
						'heading'     => esc_html__( 'Autoplay', 'clerina' ),
						'param_name'  => 'autoplay',
						'value'       => array( esc_html__( 'Yes', 'clerina' ) => 'yes' ),
						'description' => esc_html__( 'If "YES" Enable autoplay', 'clerina' ),
						'dependency'  => array(
							'element' => 'type',
							'value'   => array( 'carousel' ),
						),
					),
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__( 'Autoplay speed', 'clerina' ),
						'param_name'  => 'autoplay_speed',
						'value'       => '800',
						'description' => esc_html__( 'Set auto play speed (in ms).', 'clerina' ),
						'dependency'  => array(
							'element' => 'autoplay',
							'value'   => array( 'yes' ),
						),
					),
					array(
						'type'        => 'checkbox',
						'heading'     => esc_html__( 'Show Navigation', 'clerina' ),
						'param_name'  => 'nav',
						'value'       => array( esc_html__( 'Yes', 'clerina' ) => 'yes' ),
						'description' => esc_html__( 'If "YES" Enable navigation', 'clerina' ),
						'dependency'  => array(
							'element' => 'type',
							'value'   => array( 'carousel' ),
						),
					),
					array(
						'type'       => 'dropdown',
						'heading'    => esc_html__( 'Style Nav', 'clerina' ),
						'param_name' => 'style_nav',
						'value'      => array(
							esc_html__( 'Square', 'clerina' ) => '1',
							esc_html__( 'Circle', 'clerina' ) => '2',
						),
					),
					array(
						'type'        => 'checkbox',
						'heading'     => esc_html__( 'Show Dots', 'clerina' ),
						'param_name'  => 'dot',
						'value'       => array( esc_html__( 'Yes', 'clerina' ) => 'yes' ),
						'description' => esc_html__( 'If "YES" Enable dots', 'clerina' ),
						'dependency'  => array(
							'element' => 'type',
							'value'   => array( 'carousel' ),
						),
					),
					array(
						'type'       => 'dropdown',
						'heading'    => esc_html__( 'Columns', 'clerina' ),
						'param_name' => 'column',
						'value'      => array(
							esc_html__( '1', 'clerina' ) => '1',
							esc_html__( '2', 'clerina' ) => '2',
						),
					),
					array(
						'heading'     => esc_html__( 'Extra class name', 'clerina' ),
						'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'clerina' ),
						'param_name'  => 'el_class',
						'type'        => 'textfield',
						'value'       => '',
					),
				),
			)
		);
	}
}
<?php

/**
 * Define theme shortcodes
 *
 * @package Drcore
 */
class Clerina_Shortcodes {

	/**
	 * Store variables for js
	 *
	 * @var array
	 */
	public $l10n = array();

	/**
	 * Store variables for maps
	 *
	 * @var array
	 */
	public $maps = array();
	public $api_key = '';

	/**
	 * Check if WooCommerce plugin is actived or not
	 *
	 * @var bool
	 */
	private $wc_actived = false;

	/**
	 * Construction
	 *
	 * @return Clerina_Shortcodes
	 */
	function __construct() {
		$this->wc_actived = function_exists( 'is_woocommerce' );

		$shortcodes = array(
			'cleanpro_empty_space',
			'cleanpro_contact_form_7',
			'cleanpro_price_table',
			'cleanpro_cta',
			'cleanpro_team_carousel',
			'cleanpro_counter',
			'cleanpro_image_single',
			'cleanpro_section_title',
			'cleanpro_list',
			'cleanpro_button',
			'cleanpro_service_carousel',
			'cleanpro_portfolio_attribute',
			'cleanpro_icon_box',
			'cleanpro_image_carousel',
			'cleanpro_portfolio',
			'cleanpro_coming_soon',
			'cleanpro_socials',
			'cleanpro_image_box_carousel',
			'cleanpro_video_banner',
			'cleanpro_address_box',
			'cleanpro_image_box_carousel',
			'cleanpro_gmap',
			'cleanpro_blog_section',
			'cleanpro_testimonials_carousel',
		);

		foreach ( $shortcodes as $shortcode ) {
			add_shortcode( $shortcode, array( $this, $shortcode ) );
		}

		add_action( 'wp_footer', array( $this, 'footer' ) );
	}


	public function footer() {
		// Load Google maps only when needed
		if ( isset( $this->l10n['map'] ) ) {
			echo '<script>if ( typeof google !== "object" || typeof google.maps !== "object" )
				document.write(\'<script src="//maps.google.com/maps/api/js?sensor=false&key=' . $this->api_key . '"><\/script>\')</script>';
		}

		wp_enqueue_script(
			'shortcodes', CLERINA_ADDONS_URL . 'assets/js/frontend.js', array(
			'jquery',
		), '20171018', true
		);

		$this->l10n['days']    = esc_html__( 'days', 'baroque' );
		$this->l10n['hours']   = esc_html__( 'hours', 'baroque' );
		$this->l10n['minutes'] = esc_html__( 'minutes', 'baroque' );
		$this->l10n['seconds'] = esc_html__( 'seconds', 'baroque' );

		$this->l10n['isRTL'] = is_rtl();

		wp_localize_script( 'shortcodes', 'clerinaShortCode', $this->l10n );
	}

	/**
	 * Get empty space
	 *
	 * @since  1.0
	 *
	 * @return string
	 */
	function cleanpro_empty_space( $atts, $content ) {
		$atts = shortcode_atts(
			array(
				'height'        => '',
				'height_mobile' => '',
				'height_tablet' => '',
				'bg_color'      => '',
				'el_class'      => '',
			), $atts
		);

		$css_class = array(
			'clerina-empty-space',
			$atts['el_class'],
		);

		$style = '';

		if ( $atts['bg_color'] ) {
			$style = 'background-color:' . $atts['bg_color'] . ';';
		}

		$height = $atts['height'] ? (float) $atts['height'] : 0;

		if ( ! empty( $atts['height_tablet'] ) || $atts['height_tablet'] == '0' ) {
			$height_tablet = (float) $atts['height_tablet'];
		} else {
			$height_tablet = $height;
		}

		if ( ! empty( $atts['height_mobile'] ) || $atts['height_mobile'] == '0' ) {
			$height_mobile = (float) $atts['height_mobile'];
		} else {
			$height_mobile = $height_tablet;
		}

		$inline_css        = $height >= 0.0 ? ' style="height: ' . esc_attr( $height ) . 'px"' : '';
		$inline_css_tablet = $height_tablet >= 0.0 ? ' style="height: ' . esc_attr( $height_tablet ) . 'px"' : '';
		$inline_css_mobile = $height_mobile >= 0.0 ? ' style="height: ' . esc_attr( $height_mobile ) . 'px"' : '';

		return sprintf(
			'<div class="%s" style="%s">' .
			'<div class="clerina_empty_space_lg" %s></div>' .
			'<div class="clerina_empty_space_md" %s></div>' .
			'<div class="clerina_empty_space_xs" %s></div>' .
			'</div>',
			esc_attr( implode( ' ', $css_class ) ),
			$style,
			$inline_css,
			$inline_css_tablet,
			$inline_css_mobile
		);
	}

	/**
	 * @param string $atts
	 *
	 * @return string
	 */
	protected function cleanpro_font_size( $atts ) {
		$atts = preg_replace( '/\s+/', '', $atts );

		$pattern = '/^(\d*(?:\.\d+)?)\s*(px|\%|in|cm|mm|em|rem|ex|pt|pc|vw|vh|vmin|vmax)?$/';
		// allowed metrics: http://www.w3schools.com/cssref/css_units.asp
		$regexr   = preg_match( $pattern, $atts, $matches );
		$value    = isset( $matches[1] ) ? (float) $matches[1] : (float) $atts;
		$unit     = isset( $matches[2] ) ? $matches[2] : 'px';
		$fontSize = $value . $unit;

		return $fontSize;
	}

	/**
	 * Get limited words from given string.
	 * Strips all tags and shortcodes from string.
	 *
	 * @since 1.0.0
	 *
	 * @param integer $num_words The maximum number of words
	 * @param string $more More link.
	 *
	 * @return string Limited content.
	 */
	protected function cleanpro_addons_content_limit( $content, $num_words, $more = "&hellip;" ) {
		// Strip tags and shortcodes so the content truncation count is done correctly
		$content = strip_tags( strip_shortcodes( $content ), apply_filters( 'clerina_content_limit_allowed_tags', '<script>,<style>' ) );

		// Remove inline styles / scripts
		$content = trim( preg_replace( '#<(s(cript|tyle)).*?</\1>#si', '', $content ) );

		// Truncate $content to $max_char
		$content = wp_trim_words( $content, $num_words );

		if ( $more ) {
			return sprintf(
				'<div class="excerpt">%s <a href="%s" class="more-link" title="%s">%s</a></div>',
				$content,
				get_permalink(),
				sprintf( esc_html__( 'Continue reading &quot;%s&quot;', 'clerina' ), the_title_attribute( 'echo=0' ) ),
				esc_html( $more )
			);
		}

		return sprintf( '<div class="excerpt">%s</div>', $content );

	}

	/**
	 * Get vc link
	 *
	 * @param  array $atts
	 * @param  string $content
	 *
	 * @return string
	 */
	protected function get_vc_link( $atts, $content ) {
		$attributes = array(
			'class' => 'clerina-link',
		);

		// style btn
		$css_btn = array();

		if ( isset( $atts['color_btn'] ) && ! empty( $atts['color_btn'] ) ) {
			$css_btn[] = sprintf( ' color:%s; ', $atts['color_btn'] );
		}

		if ( isset( $atts['color_bk'] ) && ! empty( $atts['color_bk'] ) ) {
			$css_btn[] = sprintf( ' background-color:%s; ', $atts['color_bk'] );
		}

		$link = vc_build_link( $atts['link'] );

		if ( ! empty( $link['url'] ) ) {
			$attributes['href'] = $link['url'];
		}

		if ( ! $content ) {
			$content             = $link['title'];
			$attributes['title'] = $content;
		}

		if ( ! empty( $link['target'] ) ) {
			$attributes['target'] = $link['target'];
		}

		if ( ! empty( $link['rel'] ) ) {
			$attributes['rel'] = $link['rel'];
		}

		$attr = array();

		foreach ( $attributes as $name => $v ) {
			$attr[] = $name . '="' . esc_attr( $v ) . '"';
		}

		$button = sprintf(
			'<%1$s %2$s style="%3$s" >%4$s</%1$s>',
			empty( $attributes['href'] ) ? 'span' : 'a',
			implode( ' ', $attr ),
			implode( ' ', $css_btn ),
			$content
		);

		return $button;
	}

	/**
	 * @param        $image
	 * @param string $size
	 *
	 * @return string
	 */

	protected function get_vc_image( $image, $size = 'thumbnail' ) {
		$image_src = '';
		if ( function_exists( 'wpb_getImageBySize' ) ) {
			$image = wpb_getImageBySize(
				array(
					'attach_id'  => $image,
					'thumb_size' => $size,
				)
			);

			if ( $image['thumbnail'] ) {
				$image_src = $image['thumbnail'];
			} elseif ( $image['p_img_large'] ) {
				$image_src = sprintf( '<img alt="" src="%s">', esc_url( $image['p_img_large'][0] ) );
			}

		}

		if ( empty( $image_src ) ) {
			$image_src = wp_get_attachment_image( $image, $size );
		}

		return $image_src;
	}

	// Contact form 7
	function cleanpro_contact_form_7( $atts, $content ) {
		$atts = shortcode_atts(
			array(
				'title'          => '',
				'style'          => '1',
				'color'          => 'dark',
				'style_sub'      => '1',
				'form'           => '',
				'form_bg'        => '',
				'padding_top'    => '',
				'padding_right'  => '',
				'padding_bottom' => '',
				'padding_left'   => '',
				'border_radius'   => '',
				'el_class'       => '',
			), $atts
		);

		$css_class = array(
			'cl-contact-form-7',
			'form-' . $atts['color'],
			'style-' . $atts['style'],
			'border-' . $atts['style_sub'],
			$atts['el_class']
		);

		$title = $atts['title'] ? sprintf( '<h3>%s</h3>', $atts['title'] ) : '';

		$style = array();

		$p_top      = intval( $atts['padding_top'] );
		$p_right    = intval( $atts['padding_right'] );
		$p_bottom   = intval( $atts['padding_bottom'] );
		$p_left     = intval( $atts['padding_left'] );
		$b_radius   = intval( $atts['border_radius'] );

		if ( $atts['form_bg'] ) {
			$style[] = 'background-color:' . $atts['form_bg'] . ';';
		}

		if ( $atts['padding_top'] ) {
			$style[] = 'padding-top: ' . $p_top . 'px;';
		}

		if ( $atts['padding_right'] ) {
			$style[] = 'padding-right: ' . $p_right . 'px;';
		}

		if ( $atts['padding_bottom'] ) {
			$style[] = 'padding-bottom: ' . $p_bottom . 'px;';
		}

		if ( $atts['padding_left'] ) {
			$style[] = 'padding-left: ' . $p_left . 'px;';
		}

		if ( $atts['border_radius'] ) {
			$style[] = 'border-radius: ' . $b_radius . 'px;';
		}

		return sprintf(
			'<div class="%s" style="%s">%s%s</div>',
			esc_attr( implode( ' ', $css_class ) ),
			implode( ' ', $style ),
			$title,
			do_shortcode( '[contact-form-7 id="' . esc_attr( $atts['form'] ) . '" title=" ' . get_the_title( $atts['form'] ) . ' "]' )
		);
	}

	// Price table
	function cleanpro_price_table( $atts, $content ) {
		$atts = shortcode_atts(
			array(
				'title'      => '',
				'style'      => '1',
				'price'      => '',
				'unit'       => '',
				'text_after' => '',
				'link'       => '',
				'setting'    => '',
				'el_class'   => '',
			), $atts
		);
		// add class
		$css_class = array(
			'cl-price-table',
			'style-' . $atts['style'],
			$atts['el_class']
		);

		// check content
		$title = $atts['title'] ? sprintf( '<h3>%s</h3>', $atts['title'] ) : '';
		$unit  = $atts['unit'] ? sprintf( '<span>%s</span>', $atts['unit'] ) : '';
		$price = $atts['price'] ? sprintf( '<h2>%s<span>%s</span></h2>', $atts['unit'], $atts['price'] ) : '';

		$top_table = sprintf(
			'<div class="top-table">' .
			'%s' .
			'%s' .
			'</div>',
			$price,
			$atts['text_after']
		);

		// param service
		$infor   = vc_param_group_parse_atts( $atts['setting'] );
		$outputs = array();

		if ( ! empty( $infor ) ) {
			foreach ( $infor as $key => $value ) {
				$outputs[] = sprintf( '<li>%s</li>', $value['service'] );
			}
			$list_sv = sprintf(
				'<ul>' .
				'%s' .
				'</ul>',
				implode( '', $outputs )
			);
		}

		$btn[] = $this->get_vc_link( $atts, $content );


		return sprintf(
			'<div class="%s">%s %s %s %s</div>',
			esc_attr( implode( ' ', $css_class ) ),
			$title,
			$top_table,
			$list_sv,
			implode( '', $btn )
		);
	}

	// CTA
	function cleanpro_cta( $atts, $content ) {
		$atts = shortcode_atts(
			array(
				'title'      => '',
				'style'      => '1',
				'desc'       => '',
				'link'       => '',
				'image_size' => '',
				'setting'    => '',
				'color_tit'  => '',
				'color_desc' => '',
				'color_bk'   => '',
				'color_btn'  => '',
				'el_class'   => '',
			), $atts
		);

		// add class
		$css_class = array(
			'cl-cta',
			'style-' . $atts['style'],
			$atts['el_class']
		);

		// check color
		$color_tit  = $atts['color_tit'] ? sprintf( 'style= color:%s;', $atts['color_tit'] ) : '';
		$color_desc = $atts['color_desc'] ? sprintf( 'style= color:%s;', $atts['color_desc'] ) : '';

		// content
		$title = $atts['title'] ? sprintf( '<h3 %s>%s</h3>', $color_tit, $atts['title'] ) : '';
		$desc  = $atts['desc'] ? sprintf( '<p %s>%s</p>', $color_desc, $atts['desc'] ) : '';

		// param btn
		$list_btn = '';
		$outputs  = array();
		if ( $atts['style'] == '2' ) {
			$infor    = vc_param_group_parse_atts( $atts['setting'] );
			if ( ! empty( $infor ) ) {
				foreach ( $infor as $key => $value ) {

					$bk_image = isset( $value['image'] ) && ! empty ( $value['image'] ) ? $this->get_vc_image( $value['image'], $atts['image_size'] ) : '';
					$link     = isset( $value['link'] ) && ! empty( $value['link'] ) ? vc_build_link( $value['link'] ) : '';

					$href   = isset( $link['url'] ) && ! empty( $link['url'] ) ? $link['url'] : '';
					$button = isset( $link['url'] ) && ! empty( $link['url'] ) ? sprintf( '<a href="%s">%s</a>', $href, $bk_image ) : $bk_image;

					if ( isset( $link['url'] ) && ! empty( $link['url'] ) ) {
						$href   = $link['url'];
						$button = sprintf( '<a href="%s">%s</a>', $href, $bk_image );
					}
					$outputs[] = sprintf( '<div class="btn-img">%s</div>', $button );
				}
				$list_btn = sprintf(
					'<div class="box-btn-img">' .
					'%s' .
					'</div>',
					implode( '', $outputs )
				);
			}
		}

		$btn = '';
		if ( $atts['style'] == '1' ) {
			$btn = $this->get_vc_link( $atts, $content );
		}

		return sprintf(
			'<div class="%s"><div class="box-title">%s %s</div> %s %s</div>',
			esc_attr( implode( ' ', $css_class ) ),
			$title,
			$desc,
			$list_btn,
			$btn
		);
	}

	// Team carousel
	function cleanpro_team_carousel( $atts, $content ) {
		$atts = shortcode_atts(
			array(
				'item'           => '3',
				'nav'            => '',
				'dots'           => '',
				'autoplay'       => '',
				'autoplay_speed' => '2000',
				'image_size'     => '',
				'setting'        => '',
				'el_class'       => '',
			), $atts
		);

		// add class
		$css_class = array(
			'cl-team-carousel',
			$atts['el_class']
		);

		if ( $atts['nav'] ) {
			$nav = true;
		} else {
			$nav = false;
		}

		if ( $atts['autoplay'] ) {
			$autoplay = true;
		} else {
			$autoplay = false;
		}

		$id    = uniqid( 'clerina-team-carousel-' );
		$slide = intval( $atts['item'] );
		$speed = intval( $atts['autoplay_speed'] );

		$this->l10n['teamCarousel'][ $id ] = array(
			'slide'    => $slide,
			'nav'      => $nav,
			'autoplay' => $autoplay,
			'speed'    => $speed,

		);

		//param team
		$infor   = vc_param_group_parse_atts( $atts['setting'] );
		$outputs = array();

		if ( ! empty( $infor ) ) {
			foreach ( $infor as $key => $value ) {

				$name   = isset( $value['name'] ) && ! empty( $value['name'] ) ? sprintf( '<h3>%s</h3>', $value['name'] ) : '';
				$office = isset( $value['office'] ) && ! empty( $value['office'] ) ? sprintf( '<div class="job">%s</div>', $value['office'] ) : '';
				$image  = isset( $value['image'] ) && ! empty( $value['image'] ) ? $this->get_vc_image( $value['image'], $atts['image_size'] ) : '';

				// param social
				$icon         = vc_param_group_parse_atts( $value['setting_icon'] );
				$outputs_icon = array();

				if ( ! empty( $icon ) ) {
					foreach ( $icon as $key_icon => $ic ) {

						$link = ! empty( $ic['link'] ) ? vc_build_link( $ic['link'] ) : '';
						$href = ! empty( $link['url'] ) ? $link['url'] : '';

						$outputs_icon[] = sprintf(
							'<li>' .
							'<a href="%s"><i class="%s"></i></a>' .
							'</li>',
							$href,
							$ic['icon_fontawesome']
						);
					}
					$content_contact = sprintf(
						'<ul>' .
						'%s' .
						'</ul>',
						implode( '', $outputs_icon )
					);
				}
				$outputs[] = sprintf(
					'<div class="item-team">' .
					'<div class="box-img">' .
					'%s' .
					'<div class="overlay-link">' .
					'%s' .
					'</div>' .
					'</div>' .
					'%s' .
					'%s' .
					'</div>',
					$image,
					$content_contact,
					$name,
					$office
				);
			}
		}

		return sprintf(
			'<div id="%s" class="%s">' .
			'%s' .
			'</div>',
			$id,
			implode( '', $css_class ),
			implode( '', $outputs )
		);
	}

	// Image single
	function cleanpro_image_single( $atts, $content ) {
		$atts                             = shortcode_atts(
			array(

				'image'           => '',
				'image_size'      => '',
				'image_alignment' => 'left',
				'el_class'        => '',
			), $atts
		);
		$align_img                        = $img = '';
		$id                               = uniqid( 'clerina-single-image-' );
		$border_radius                    = ! empty( $atts['border_radius'] ) ? sprintf( '%s', $atts['border_radius'] ) : '';
		$this->l10n['imageSingle'][ $id ] = array(
			'style' => $border_radius,
		);

		if ( isset( $atts['image'] ) ) {


			if ( function_exists( 'wpb_getImageBySize' ) ) {
				$image = wpb_getImageBySize(
					array(
						'attach_id'  => $atts['image'],
						'thumb_size' => $atts['image_size'],
						'class'      => 'image-single',
					)
				);
				$img   = $image['thumbnail'];
			} else {
				$image = wp_get_attachment_image_src( $atts['image'], $atts['image_size'] );
				if ( $image ) {
					$img_url  = $image[0];
					$img_size = $atts['image_size'];
					$img      = sprintf( "<img alt='%s'  src='%s' />", $img_size, $img_url );
				}
			}
			$align_img = $atts['image_alignment'];
		}

		return sprintf( '<div id="%s" class="clerina-single-image text-%s %s">%s</div>', $id, $align_img, $atts['el_class'], $img );
	}

	// Section title
	function cleanpro_section_title( $atts, $content ) {
		$atts          = shortcode_atts(
			array(
				'style'         => 'light',
				'title'         => '',
				'alignment'     => 'left',
				'font_size'     => '',
				'text_color'    => '',
				'content_color' => '',
				'line_height'   => '',
				'padding'       => '',
				'el_class'      => '',
			), $atts
		);
		$style         = $atts['style'];
		$alignment     = $atts['alignment'];
		$size          = $this->cleanpro_font_size( $atts['font_size'] );
		$font_size     = ! empty( $atts['font_size'] ) ? sprintf( 'font-size:%s;', $size ) : '';
		$line_height   = ! empty( $atts['line_height'] ) ? sprintf( 'line-height:%s;', $atts['line_height'] ) : '';
		$text_color    = ! empty( $atts['text_color'] ) ? sprintf( 'color:%s;', $atts['text_color'] ) : '';
		$padding       = ! empty( $atts['padding'] ) ? sprintf( 'padding:%s;', $atts['padding'] ) : '';
		$content_color = ! empty( $atts['content_color'] ) ? sprintf( 'color:%s;', $atts['content_color'] ) : '';
		$text_style    = ! empty( $font_size || $text_color || $line_height ) ? sprintf( 'style="%s %s %s %s"', $font_size, $text_color, $line_height, $padding ) : '';
		$title         = ! empty( $atts['title'] ) ? sprintf( '<h3 class="title" %s>%s</h3>', $text_style, $atts['title'] ) : '';
		$descr         = ! empty( $content ) ? sprintf( '<div class="description" %s><p>%s</p></div>', $content_color, $content ) : '';

		return sprintf( '<div class="clerina-section-title text-%s clerina-style-%s %s">%s %s</div>',
			$alignment, $style, $atts['el_class'], $title, $descr );
	}

	// Image carousel
	function cleanpro_image_carousel( $atts, $content ) {
		$atts = shortcode_atts(
			array(
				'row'            => '1',
				'item'           => '1',
				'nav'            => '',
				'dots'           => '',
				'autoplay'       => '',
				'autoplay_speed' => '2000',
				'image_size'     => '',
				'setting'        => '',
				'el_class'       => '',
			), $atts
		);

		// add class
		$css_class = array(
			'cl-image-carousel',
			' style-' . $atts['row'],
			$atts['el_class']
		);

		if ( $atts['dots'] ) {
			$dot = true;
		} else {
			$dot = false;
		}

		if ( $atts['nav'] ) {
			$nav = true;
		} else {
			$nav = false;
		}

		if ( $atts['autoplay'] ) {
			$autoplay = true;
		} else {
			$autoplay = false;
		}

		$id     = uniqid( 'clerina-image-carousel-' );
		$perrow = intval( $atts['item'] );
		$row    = intval( $atts['row'] );
		$speed  = intval( $atts['autoplay_speed'] );

		$this->l10n['imageCarousel'][ $id ] = array(
			'dot'      => $dot,
			'nav'      => $nav,
			'autoplay' => $autoplay,
			'speed'    => $speed,
			'perrow'    => $perrow,
			'row'      => $row,
		);

		// param content
		$infor   = vc_param_group_parse_atts( $atts['setting'] );
		$outputs = array();

		if ( ! empty( $infor ) ) {
			foreach ( $infor as $key => $value ) {

				$image = isset( $value['image'] ) && ! empty( $value['image'] ) ? $this->get_vc_image( $value['image'], $atts['image_size'] ) : '';

				$link = ! empty( $value['link'] ) ? vc_build_link( $value['link'] ) : '';

				if ( isset( $link['url'] ) && ! empty( $link['url'] ) ) {
					$href      = $link['url'];
					$outputs[] = sprintf(
						'<div class="item-img">' .
						'<a href="%s">%s</a>' .
						'</div>',
						$href,
						$image
					);
				} else {
					$outputs[] = sprintf(
						'<div class="item-img">' .
						'%s' .
						'</div>',
						$image
					);
				}

			}
		}

		return sprintf(
			'<div id="%s" class="%s">' .
			'%s' .
			'</div>',
			$id,
			implode( '', $css_class ),
			implode( '', $outputs )
		);
	}


	/**
	 * Get list Nav
	 *
	 * @since  1.0
	 *
	 * @return string
	 */
	function cleanpro_list( $atts, $content ) {
		$atts = shortcode_atts(
			array(
				'icon_library'     => '0',
				'icon_fontawesome' => '',
				'name_svg'         => '',
				'icon_color'       => '',
				'icon_size'        => '',
				'title_color'      => '',
				'setting'          => '',
				'theme'            => 'dark',
			), $atts
		);

		$icon_type      = $atts['icon_library'];
		$value_icon     = $atts['name_svg'];
		$icon_color     = $atts['icon_color'];
		$icon_size      = $atts['icon_size'];
		$theme          = $atts['theme'];
		$fa_font        = $atts['icon_fontawesome'];
		$size           = $this->cleanpro_font_size( $atts['icon_size'] );
		$style_ic_color = ! empty( $icon_color ) ? "color:$icon_color;" : '';
		$icon_size      = ! empty( $icon_size ) ? "font-size:$size;" : '';


		$style_ic = "style='$style_ic_color $icon_size '";

		switch ( $icon_type ) {
			case '0':
				$icon = "<span class='elegant icon_circle-slelected'></span>";
				break;
			case '1':
				$icon = "<span class='$fa_font svg-icon' $style_ic></span>";
				break;
			case '2':
				$icon = "<span class='svg-icon' $style_ic><svg viewBox='0 0 20 20'><use xlink:href='#$value_icon'></use></svg></span>";
				break;
			default:
				$icon = "";
		}
		$infor   = vc_param_group_parse_atts( $atts['setting'] );
		$outputs = array();

		if ( ! empty( $infor ) ) {
			foreach ( $infor as $key => $value ) {
				if ( $value['content_list'] ) {
					$text_color = ! empty( $value['li_color'] ) ? "style='color:" . $value['li_color'] . ";'" : '';
					$link       = ! empty( $value['link'] ) ? $value['link'] : '';
					if ( $link ) {
						$outputs[] = sprintf( "<li %s><a href='%s'>%s %s</a></li>", $text_color, $link, $icon, $value['content_list'] );
					} else {
						$outputs[] = sprintf( "<li %s>%s %s</li>", $text_color, $icon, $value['content_list'] );
					}
				}
			}
		}

		return sprintf( "<div class='clerina-list-nav list-theme-%s'> <ul>%s</ul></div>",
			$theme, implode( '', $outputs ) );
	}

	/**
	 * Get button
	 *
	 * @since  1.0
	 *
	 * @return string
	 */
	function cleanpro_button( $atts, $content ) {
		$atts            = shortcode_atts(
			array(

				'text_bt'          => '',
				'link'             => '',
				'bt_border'        => '',
				'bt_border_color'  => '',
				'bt_border_style'  => '',
				'bt_bg_color'      => '',
				'bt_text_color'    => '',
				'border_radius'    => '',
				'bt_align'         => 'left',
				'icon_type'        => 'none',
				'icon_color'       => '',
				'icon_size'        => '50px',
				'value_icon'       => '',
				'icon_fontawesome' => '',
				'image_size'       => '',
				'image'            => '',
				'padding'          => '',
				'bt_hover'         => '1',
				'el_class'         => '',

			), $atts
		);
		$value_icon      = $atts['value_icon'];
		$icon_type       = $atts['icon_type'];
		$text_bt         = $atts['text_bt'];
		$link            = vc_build_link( $atts['link'] );
		$link            = $link['url'];
		$bt_border       = $atts['bt_border'];
		$bt_border_color = $atts['bt_border_color'];
		$bt_border_style = $atts['bt_border_style'];
		$bt_bg_color     = $atts['bt_bg_color'];
		$bt_text_color   = $atts['bt_text_color'];
		$border_radius   = $atts['border_radius'];
		$bt_align        = $atts['bt_align'];
		$bt_hover        = $atts['bt_hover'];
		$padding         = $atts['padding'];
		$el_class        = $atts['el_class'];
		$image_ids       = $atts['image'];
		$bt_bg_color     = ! empty( $bt_bg_color ) ? "background-color: $bt_bg_color;" : '';
		$bt_text_color   = ! empty( $bt_text_color ) ? "color: $bt_text_color;" : '';
		$border_radius   = ! empty( $border_radius ) ? "border-radius: $border_radius;" : '';
		$padding         = ! empty( $padding ) ? "padding: $padding;" : '';
		$border          = '';
		if ( $bt_border || $bt_border_color || $bt_border_style ): $border = sprintf( "border: %s %s %s;", $bt_border, $bt_border_style, $bt_border_color );endif;

		$style          = sprintf( 'style="%s" ', $bt_bg_color . $bt_text_color . $border . $border_radius . $padding );
		$style_ic_color = ! empty( $icon_color ) ? "color:$icon_color;" : '';
		$size           = $this->cleanpro_font_size( $atts['icon_size'] );
		$icon_size      = ! empty( $icon_size ) ? "font-size:$size;" : '';
		$style_ic       = "style='$style_ic_color $icon_size '";

		switch ( $icon_type ) {
			case 'none':
				$icon = '';
				break;
			case 'fa_font':
				$value = $atts['icon_fontawesome'];
				$icon  = "<span class='$value svg-icon' $style_ic></span>";
				break;
			case 'svg_icon':
				$icon = "<span class='svg-icon'  $style_ic><svg viewBox='0 0 20 20'><use xlink:href='#$value_icon'></use></svg></span>";
				break;
			case 'image':
				$image_thumb = clerina_load_image( $image_ids, $atts['image_size'] );
				$icon        = "<span class='svg-icon'>$image_thumb</span>";
				break;
			default:
				$icon = "";
		}

		return sprintf( '<div class="clerina-button-group clerina-align-%s"><a href="%s" class="clerina-button %s hover-%s" %s>%s<span>%s</span></a></div>', $bt_align, $link, $el_class, $bt_hover, $style, $icon, $text_bt );
	}

	function cleanpro_counter( $atts, $content ) {
		$atts = shortcode_atts(
			array(
				'color'      => '',
				'number'     => '',
				'text_after' => '',
				'desc'       => '',
				'duration'   => '',
				'el_class'   => '',
			), $atts
		);

		// add class
		$css_class = array(
			'cl-counter',
			$atts['el_class']
		);

		// check css, content
		$css        = ! empty( $atts['color'] ) ? sprintf( 'style = "color: %s;"', $atts['color'] ) : '';
		$text_after = ! empty( $atts['text_after'] ) ? sprintf( '<span class="text-after">%s</span>', $atts['text_after'] ) : '';
		$number     = ! empty( $atts['number'] ) || ! empty( $atts['text_after'] ) ? sprintf( '<h3 %s><span class="counter-number" data-duration="%s">%s</span> %s</h3>', $css, $atts['duration'], $atts['number'], $text_after ) : '';

		return sprintf(
			'<div class="%s counter-value" %s data-duration="%s">' .
			'%s' .
			'%s' .
			'</div>',
			implode( '', $css_class ),
			$css,
			$atts['duration'],
			$number,
			$atts['desc']
		);
	}

	/**
	 **
	 * Get portfolio attribute
	 *
	 * @since  1.0
	 *
	 * @return string
	 */
	function cleanpro_portfolio_attribute( $atts, $content ) {
		$atts = shortcode_atts(
			array(
				'client'    => '',
				'work_type' => '',
				'date'      => '',
				'el_class'  => '',
			), $atts
		);

		$client    = $atts['client'];
		$work_type = $atts['work_type'];
		$date      = $atts['date'];

		$terms  = get_the_terms( get_the_ID(), 'portfolio_category' );
		$output = array();
		if ( $terms && ! is_wp_error( $terms ) ) :

			foreach ( $terms as $item ) {
				$id       = esc_url( get_term_link( $item->term_id, 'portfolio_category' ) );
				$name     = $item->name;
				$output[] = sprintf( '<a class="category" href="%s">%s</a>', $id, $name );
			}
		endif;
		$category  = ! empty( $output ) ? sprintf( "<li><p>%s :</p> <span> %s</span></li>", esc_html__( 'Category', 'clerina' ), implode( ', ', $output ) ) : '';
		$client    = ! empty( $client ) ? sprintf( "<li><p>%s :</p> <span> %s</span></li>", esc_html__( 'Client', 'clerina' ), $client ) : '';
		$work_type = ! empty( $work_type ) ? sprintf( "<li><p>%s :</p> <span> %s</span></li>", esc_html__( 'Work Type', 'clerina' ), $work_type ) : '';
		$date      = ! empty( $date ) ? sprintf( "<li><p>%s:</p> <span> %s</span></li>", esc_html__( 'Date', 'clerina' ), $date ) : '';

		return sprintf( "<div class='clerina_portfolio_atrrs'>
						<ul>%s</ul>
						</div>", $client . $work_type . $date . $category );
	}

	/**
	 * Get carousel
	 *
	 * @since  1.0
	 *
	 * @return string
	 */
	function cleanpro_service_carousel( $atts, $content ) {
		$atts = shortcode_atts(
			array(

				'column'          => '2',
				'value_item'      => '',
				'image_size'      => '',
				'excerpt_content' => '20',
				'nav'             => '',
				'dots'            => '',
				'autoplay'        => '',
				'autoplay_speed'  => '2000',
				'el_class'        => '',
			), $atts
		);

		if ( $atts['dots'] ) {
			$dot = true;
		} else {
			$dot = false;
		}

		if ( $atts['autoplay'] ) {
			$autoplay = true;
		} else {
			$autoplay = false;
		}

		$id    = uniqid( 'clerina-service-carousel-' );
		$slide = $scroll = intval( $atts['column'] );
		$speed = intval( $atts['autoplay_speed'] );

		$this->l10n['serviceCarousel'][ $id ] = array(
			'slide'    => $slide,
			'scroll'   => $scroll,
			'dot'      => $dot,
			'autoplay' => $autoplay,
			'speed'    => $speed,
			'rows'     => $slide,
		);

		$item_per_row   = $atts['column'];
		$posts_per_page = $atts['value_item'];
		$class_of_item  = 12 / $item_per_row;
		if ( $item_per_row == 5 ): $class_of_item = '5-2';endif;

		$args      = array( 'post_type' => 'service', 'posts_per_page' => $posts_per_page );
		$the_query = new WP_Query( $args );
		$html      = array();

		if ( $the_query->have_posts() ) :
			while ( $the_query->have_posts() ) : $the_query->the_post();
				$permalink = get_permalink();
				$the_title = get_the_title();
				$excerpt   = get_the_excerpt();
				$id_img    = get_the_ID();
				if ( function_exists( 'clerina_content_limit' ) ) {
					$excerpt = clerina_content_limit( $excerpt, $atts['excerpt_content'], false );
				}

				$value = array();
				$terms = get_the_terms( get_the_ID(), 'service_category' );
				if ( $terms && ! is_wp_error( $terms ) ) :

					foreach ( $terms as $item ) {
						$term_id      = esc_url( get_term_link( $item->term_id, 'service_category' ) );
						$name    = $item->name;
						$value[] = sprintf( '<a class="category" href="%s">%s</a>', $term_id, $name );
					}
				endif;
				$category = implode( ',', $value );

				if ( $id_img ) {
					$thumb       = get_the_post_thumbnail_url( $id_img, 'clerina-service' );
					$image_thumb = "<img alt='$id_img' src='$thumb'/>";
					$image_thumb = sprintf( '<a href="%s" class="emtry-thumbnail">%s</a>', $permalink, $image_thumb );
				} else {
					$image_thumb = "";
				}
				$readmore = esc_html__( 'Read more', 'clerina' );
				$html[]   = sprintf( "<div class='col-sm-%s clerina-service-grid-wrapter'>
						<div class='entry-wrapter'>
							%s
							<div class='hover'>
								<h3 class='entry-title'><a href='%s' class='title'>%s</a></h3>
								<div class='entry-content'>%s</div>
								<div class='entry-readmore'> <a href='%s' class='read-more'>%s</a></div>
							</div>
						</div>
					</div>", $class_of_item, $image_thumb, $permalink, $the_title, $excerpt, $permalink, $readmore );
			endwhile;
		endif;

		return sprintf( '<div class="clerina-service-carousel row" id="%s">%s</div>', $id, $out_put = implode( '', $html ) );
	}

	/**
	 * Get icon box
	 *
	 * @since  1.0
	 *
	 * @return string
	 */
	function cleanpro_icon_box( $atts, $content ) {
		$atts               = shortcode_atts(
			array(
				'style'              => '1',
				'align'              => 'left',
				'theme'              => 'light',
				'title'              => '',
				'text_color'         => '',
				'icon_type'          => 'fa_font',
				'icon_fontawesome'   => '',
				'value_icon'         => '',
				'image'              => '',
				'image_size'         => '',
				'icon_color'         => '',
				'icon_size'          => '',
				'icon_link'           => '',
				'readmore'           => '',
				'padding'            => '',
				'background_image'   => '',
				'background_color'   => '',
				'value_elegant_icon' => '',
				'row'                => '1',
				'background_size'    => 'auto',
				'el_class'           => '',
			), $atts
		);
		$style              = $atts['style'];
		$icon_type          = $atts['icon_type'];
		$value_elegant_icon = $atts['value_elegant_icon'];
		$value_icon         = $atts['value_icon'];
		$icon_color         = $atts['icon_color'];
		$image_ids          = $atts['image'];
		$icon_size          = $this->cleanpro_font_size( $atts['icon_size'] );

		$style_ic_color      = ! empty( $icon_color ) ? sprintf( 'color: %s;', $icon_color ) : '';
		$icon_size           = ! empty( $icon_size ) ? sprintf( 'font-size: %s;', $icon_size ) : '';
		$style_ic            = ! empty( $icon_color ) || ! empty( $icon_size ) ? sprintf( 'style="%s"', $style_ic_color . $icon_size ) : '';
		$position            = ! empty( $atts['align'] ) ? sprintf( 'text-%s', $atts['align'] ) : '';
		$padding             = ! empty( $atts['padding'] ) ? sprintf( 'padding: %s;', $atts['padding'] ) : '';
		$el_class            = ! empty( $atts['el_class'] ) ? sprintf( '%s', $atts['el_class'] ) : '';
		$theme               = ! empty( $atts['theme'] ) ? sprintf( '%s', $atts['theme'] ) : '';
		$background_image_id = ! empty( $atts['background_image'] ) ? $atts['background_image'] : '';
		$background_color    = ! empty( $atts['background_color'] ) ? sprintf( 'style="background-color: %s;"', $atts['background_color'] ) : '';
		$background_size     = ! empty( $atts['background_size'] ) ? sprintf( 'background-size: %s;', $atts['background_size'] ) : '';

		if ( $style == 3 ) : $row = sprintf( 'clerina-icon-box-%s-row', $atts['row'] );
		else : $row = ''; endif;

		$background_image_url = ! empty( $background_image_id ) ? wp_get_attachment_image_src( $background_image_id, 'full' )[0] : '';
		$image_background     = ! empty( $background_image_url ) ? sprintf( 'background-image:url(%s);', $background_image_url ) : '';
		$style_sheet          = ! empty( $padding ) || ! empty( $background_image_url ) ? sprintf( 'style="%s"', $padding . $image_background . $background_size ) : '';

		switch ( $icon_type ) {
			case 'fa_font':
				$value = $atts['icon_fontawesome'];
				$icon  = sprintf( "<span class='%s clerina-icon' %s></span>", $value, $style_ic );
				break;
			case 'elegant_icon':
				$icon = sprintf( "<span class='clerina-icon' %s><i class='icon-%s'></i></span>", $style_ic, $value_elegant_icon );
				break;
			case 'svg_icon':
				$icon = sprintf( "<span class='clerina-icon' %s><svg><use xlink:href='#%s'></use></svg></span>", $style_ic, $value_icon );
				break;
			case 'image':
				$image_thumb = clerina_load_image( $image_ids, $atts['image_size'] );
				$icon        = "<div class='clerina-icon'>$image_thumb</div>";
				break;
			default:
				$icon = "";
		}

		$entry_icon  = ! empty( $icon ) ? sprintf( '<div class="header-icon">%s</div>', $icon ) : '';
		if($atts['icon_link']){
			$entry_icon  = ! empty( $icon ) ? sprintf( '<div class="header-icon"><a href="%s">%s</a></div>', $atts['icon_link'], $icon ) : '';
		}
		$link        = vc_build_link( $atts['title'] );
		$href        = ! empty( $link['url'] ) ? sprintf( ' href="%s"', $link['url'] ) : '';
		$title       = ! empty( $link['title'] ) ? $link['title'] : '';
		$tag         = ! empty( $link['url'] ) ? 'a' : 'div';
		$entry_title = ! empty( $title ) ? sprintf( '<div class="iconbox-title"><h3 class="title"><%s%s>%s</%s></h3></div>', $tag, $href, $title, $tag ) : '';
		$descr       = ! empty( $content ) ? sprintf( '<div class="iconbox-content"><p>%s</p></div>', $content ) : '';

		return sprintf( '<div class="clerina-icon-box clerina-icon-box-%s %s clerina-style-%s %s %s" ><div class="background-image" %s></div><div class="backgound-color" %s></div> <div class="main-icon">%s</div> </div>',
			$style, $position, $theme, $row, $el_class, $style_sheet, $background_color, $entry_icon . $entry_title . $descr );
	}

	/**
	 * Shortcode to display portfolio
	 *
	 * @param  array $atts
	 * @param  string $content
	 *
	 * @return string
	 */
	function cleanpro_portfolio( $atts, $content ) {
		$atts = shortcode_atts(
			array(
				'filter'         => false,
				'nav_color'      => 'light',
				'version'        => 'dark',
				'number'         => '-1',
				'nav'            => false,
				'dot'            => false,
				'autoplay'       => false,
				'autoplay_speed' => '800',
				'el_class'       => '',
			), $atts
		);

		$css_class = array(
			'clerina_portfolio-carousel',
			'mf-portfolio-carousel',
			'style-carousel',
			$atts['el_class'],
		);


		$autoplay_speed = intval( $atts['autoplay_speed'] );

		if ( $atts['autoplay'] ) {
			$autoplay = true;
		} else {
			$autoplay = false;
		}

		if ( $atts['nav'] ) {
			$nav = true;
		} else {
			$nav = false;
		}

		if ( $atts['dot'] ) {
			$dot = true;
		} else {
			$dot = false;
		}

		$id = uniqid( 'portfolio-slider-' );

		$this->l10n['portfolio'][ $id ] = array(
			'nav'            => $nav,
			'dot'            => $dot,
			'autoplay'       => $autoplay,
			'autoplay_speed' => $autoplay_speed,
		);

		$output = array();
		$cats   = array();

		$query_args = array(
			'posts_per_page'      => $atts['number'],
			'post_type'           => 'portfolio',
			'ignore_sticky_posts' => true,
		);

		$query = new WP_Query( $query_args );

		while ( $query->have_posts() ) : $query->the_post();

			$permalink   = get_permalink();
			$the_title   = get_the_title();
			$sizethumb   = 'clerina-portfolio-full-width';
			$tx_readmore = esc_html__( 'View Project', 'clerina' );

			$post_categories = wp_get_post_terms( get_the_ID(), 'portfolio_category' );
			$slug            = array();
			foreach ( $post_categories as $cat ) {
				if ( empty( $cats[ $cat->term_id ] ) ) {
					$cats[ $cat->term_id ] = array( 'name' => $cat->name, 'slug' => $cat->slug, );
				}
				$slug[]                = 'portfolio_category-' . $cat->slug;
			}
			$class         = sprintf( 'project-wrapper col-sm-6 col-xs-12 col-md-3 col-4 %s', implode( ' ', $slug ) );
			$url_thumbnail = get_the_post_thumbnail_url( get_the_ID(), $sizethumb );
			$thumbnail     = sprintf( '<div class="thumbnail"><img alt="%s" src="%s"/></div> ', $the_title, $url_thumbnail );
			$read_more     = sprintf( '<div class="content"><div class="inner"><h3 class="title">%s</h3><div class="more"><a class="read-more" href="%s">%s</a></div></div></div>', $the_title, $permalink, $tx_readmore );
			$output[]      = sprintf( '<div class="%s">%s%s</div>', $class, $thumbnail, $read_more );

		endwhile;
		wp_reset_postdata();

		$filter = array(
			'<li><a href="#" class="active" data-filter="*">' . esc_html__( 'All Projects', 'clerina' ) . '</a></li>'
		);
		foreach ( $cats as $category ) {
			$filter[] = sprintf( '<li><a href="#" class="" data-filter=".portfolio_category-%s">%s</a></li>', esc_attr( $category['slug'] ), esc_html( $category['name'] ) );
		}

		$filter_html = '<ul class="nav-filter ' . $atts['nav_color'] . '">' . implode( "\n", $filter ) . '</ul>';

		$dir = '';

		if ( is_rtl() ) {
			$dir = 'dir="rtl"';
		}

		return sprintf(
			'<div class="%s">
				<div class="cl-portfolio-header nav-section">%s</div>
                <div %s class="portfolio-list %s" id="%s">%s</div>
            </div>',
			esc_attr( implode( ' ', $css_class ) ),
			$atts['filter'] == '1' ? $filter_html : '',
			$dir,
			'',
			esc_attr( $id ),
			implode( '', $output )
		);
	}

	/**
	 * Comming soon shortcode
	 *
	 * @param array $atts
	 * @param string $content
	 *
	 * @return string
	 */
	function cleanpro_coming_soon( $atts, $content ) {
		$atts = shortcode_atts(
			array(
				'date'       => '',
				'text_color' => 'dark',
				'el_class'   => '',
			), $atts
		);

		$css_class = array(
			'cl-coming-soon cl-time-format text-center',
			'text-' . $atts['text_color'],
			$atts['el_class'],
		);

		$second = 0;
		if ( $atts['date'] ) {
			$second_current = strtotime( date_i18n( 'Y/m/d H:i:s' ) );
			$date           = new DateTime( $atts['date'] );
			if ( $date ) {
				$second_discount = strtotime( date_i18n( 'Y/m/d H:i:s', $date->getTimestamp() ) );
				if ( $second_discount > $second_current ) {
					$second = $second_discount - $second_current;
				}
			}
		}

		$time_html = sprintf( '<div class="cl-time-countdown cl-countdown">%s</div>', $second );

		return sprintf(
			'<div class="%s">%s</div>',
			esc_attr( implode( ' ', $css_class ) ),
			$time_html
		);
	}

	function cleanpro_socials( $atts, $content ) {
		$atts = shortcode_atts(
			array(
				'socials'  => '',
				'el_class' => '',
			), $atts
		);

		$css_class = array(
			'cl-socials text-center',
			$atts['el_class'],
		);

		$socials       = vc_param_group_parse_atts( $atts['socials'] );
		$social_output = array();
		if ( ! empty( $socials ) ) {
			foreach ( $socials as $name => $value ) {
				$icon      = $image = $icon_html = '';
				$icon_type = $value['icon_type'];

				if ( isset( $value['icon_fontawesome'] ) && $value['icon_fontawesome'] ) {
					$icon = '<i class="' . esc_attr( $value['icon_fontawesome'] ) . '"></i>';
				}

				if ( isset( $value['image'] ) && $value['image'] ) {
					$image = wp_get_attachment_image( $value['image'], 'full' );
				}

				if ( $icon_type == 'fontawesome' ) {
					$icon_html = $icon;
				} else {
					$icon_html = $image;
				}

				if ( isset( $value['link'] ) && $value['link'] ) {
					$link = sprintf( '<a href="%s" target="_blank">%s</a>', esc_url( $value['link'] ), $icon_html );
				} else {
					$link = $icon_html;
				}

				$social_output[] = sprintf( '<li>%s</li>', $link );

			}
		}

		return sprintf(
			'<div class="%s"><ul>%s</ul></div>',
			esc_attr( implode( ' ', $css_class ) ),
			implode( '', $social_output )
		);
	}

	// Address box
	function cleanpro_address_box( $atts, $content ) {
		$atts = shortcode_atts(
			array(
				'setting'      => '',
				'setting_icon' => '',
				'bk_box'       => '',
				'color_tit'    => '',
				'color_desc'   => '',
				'el_class'     => '',
			), $atts
		);

		// add class
		$css_class = array(
			'cl-address-box',
			$atts['el_class']
		);

		// Check color
		$bk_box     = ! empty( $atts['bk_box'] ) ? sprintf( 'style= "background-color:%s;"', $atts['bk_box'] ) : '';
		$color_tit  = ! empty( $atts['color_tit'] ) ? sprintf( 'style= "color:%s;"', $atts['color_tit'] ) : '';
		$color_desc = ! empty( $atts['color_desc'] ) ? sprintf( 'style= "color:%s;"', $atts['color_desc'] ) : '';

		// param content
		$infor   = vc_param_group_parse_atts( $atts['setting'] );
		$outputs = array();
		if ( ! empty( $infor ) ) {
			foreach ( $infor as $key => $value ) {

				$title = isset( $value['title'] ) && ! empty( $value['title'] ) ? sprintf( '<h3 %s>%s</h3>', $color_tit, $value['title'] ) : '';
				$desc  = isset( $value['desc'] ) && ! empty( $value['desc'] ) ? sprintf( '<div class="desc" %s>%s</div>', $color_desc, $value['desc'] ) : '';

				$outputs[] = sprintf(
					'<div class="box-text">' .
					'%s' .
					'%s' .
					'</div>',
					$title,
					$desc
				);
			}
			$content = sprintf(
				'<div class="top-box">' .
				'%s' .
				'</div>',
				implode( '', $outputs )
			);
		}
		$icon         = vc_param_group_parse_atts( $atts['setting_icon'] );
		$outputs_icon = array();

		if ( ! empty( $icon ) ) {
			foreach ( $icon as $key_icon => $ic ) {

				$link = isset( $ic['link'] ) && ! empty( $ic['link'] ) ? vc_build_link( $ic['link'] ) : '';
				$href = ! empty( $link['url'] ) ? $link['url'] : '';

				$outputs_icon[] = sprintf(
					'<li>' .
					'<a href="%s"><i class="%s"></i></a>' .
					'</li>',
					$href,
					$ic['icon_fontawesome']
				);
			}
			$content_contact = sprintf(
				'<ul class="social">' .
				'%s' .
				'</ul>',
				implode( '', $outputs_icon )
			);
		}

		return sprintf(
			'<div class="%s" %s>' .
			'%s' .
			'%s' .
			'</div>',
			implode( '', $css_class ),
			$bk_box,
			$content,
			$content_contact
		);
	}


	/**
	 * Get image box carousel
	 *
	 * @since  1.0
	 *
	 * @return string
	 */
	function cleanpro_video_banner( $atts, $content = null ) {
		$atts = shortcode_atts(
			array(
				'video'       => '',
				'min_height'  => '500',
				'image'       => '',
				'image_size'  => '',
				'video_style' => 'full',
				'btn_text'    => esc_html__( 'Watch Project Video', 'clerina' ),
				'el_class'    => '',

			), $atts
		);

		if ( empty( $atts['video'] ) ) {
			return '';
		}

		$css_class = array(
			'clerina-video-banner',
			$atts['el_class'],
		);

		$min_height  = intval( $atts['min_height'] );
		$video_html  = $src = $btn = '';
		$style       = array();
		$video_url   = $atts['video'];
		$video_w     = '1024';
		$video_h     = '768';
		$video_style = $atts['video_style'];

		if ( $min_height ) {
			$style[] = 'min-height:' . $min_height . 'px;';
		}

		if ( $atts['image'] ) {
			$image = wp_get_attachment_image_src( $atts['image'], 'full' );
			if ( $image ) {
				$src = $image[0];
			}
			$style[] = 'background-image:url(' . $src . ');';
		}

		if ( filter_var( $video_url, FILTER_VALIDATE_URL ) ) {
			$atts = array(
				'width'  => $video_w,
				'height' => $video_h
			);
			if ( $oembed = @wp_oembed_get( $video_url, $atts ) ) {
				$video_html = $oembed;
			}
			if ( $video_html ) {
				$video_html = sprintf( '<div class="clerina-wrapper"><div class="clerina-video-wrapper">%s</div></div>', $video_html );
			}
		}

		return sprintf(
			'<div class="clerina-video-%s %s" style="%s">
				<div class="clerina-video-content"><a href="#" data-href="%s" class="photoswipe"><span class="icon-play-circle"></span></a></div>
			</div>',
			$video_style,
			esc_attr( implode( ' ', $css_class ) ),
			esc_attr( implode( ' ', $style ) ),
			esc_attr( $video_html )
		);
	}

	function cleanpro_image_box_carousel( $atts, $content ) {
		$atts = shortcode_atts(
			array(
				'columns'         => '1',
				'image_size'      => '',
				'excerpt_content' => '20',
				'nav'             => '',
				'setting'         => '',
				'dots'            => '',
				'autoplay'        => '',
				'autoplay_speed'  => '2000',
				'el_class'        => '',
			), $atts
		);

		$css_class = array(
			'clerina_image_box_carousel',
			'mf-image-box-carousel',
			'style-carousel',
			$atts['el_class'],
		);

		$image_size     = ! empty( $atts['image_size'] ) ? $atts['image_size'] : '';
		$autoplay_speed = intval( $atts['autoplay_speed'] );

		if ( $atts['autoplay'] ) {
			$autoplay = true;
		} else {
			$autoplay = false;
		}

		if ( $atts['nav'] ) {
			$nav = true;
		} else {
			$nav = false;
		}

		if ( $atts['dots'] ) {
			$dot = true;
		} else {
			$dot = false;
		}
		$slide = $scroll = intval( $atts['columns'] );
		$id    = uniqid( 'clerina-image-box-carousel-' );

		$this->l10n['imageBoxCarousel'][ $id ] = array(
			'nav'            => $nav,
			'dot'            => $dot,
			'autoplay'       => $autoplay,
			'autoplay_speed' => $autoplay_speed,
			'slide'          => $slide,

		);
		$infor                                 = vc_param_group_parse_atts( $atts['setting'] );
		$outputs                               = array();

		if ( ! empty( $infor ) ) {
			foreach ( $infor as $key => $value ) {

				$image_id    = ! empty( $value['image'] ) ? $value['image'] : '';
				$image       = ! empty( $image_id ) ? $this->get_vc_image( $image_id, $image_size ) : '';
				$entry_image = sprintf( '<div class="image-box-thumbnail">%s</div>', $image );

				$link        = ! empty( $value['link'] ) ? vc_build_link( $value['link'] ) : '';
				$title       = ! empty( $value['title'] ) ? $value['title'] : '';
				$href        = ! empty( $link ) ? sprintf( ' href="%s"', $link['url'] ) : '';
				$tag         = ! empty( $href ) ? 'a' : 'div';
				$entry_title = sprintf( '<h3 class="image-box-title"><%s%s class="title">%s</%s></h3>', $tag, $href, $title, $tag );
				$description   = ! empty( $value['description'] ) ? $value['description'] : '';
				$entry_content = sprintf( '<div class="image-box-content"><p>%s</p></div>', $description );

				$entry_readmore = '';
				if ( $href ) {
					$read_more      = esc_html__( 'Read More' );
					$entry_readmore = sprintf( '<div class="image-box-readmore"><a class="read-more" %s>%s</a> </div>', $href, $read_more );
				}
				$outputs[] = sprintf( '<div class="col-md-4 clerina-image-box-item"><div class="image-box-carousel-wrapper">%s%s%s%s</div></div>',
					$entry_image, $entry_title, $entry_content, $entry_readmore );
			}
		}

		return sprintf( '<div class="clerina-image-box-carousel" id="%s">%s</div>', $id, implode( '', $outputs ) );
	}

	/*
	 * GG Maps shortcode
	 */
	function cleanpro_gmap( $atts, $content ) {
		$atts = shortcode_atts(
			array(
				'api_key'   => '',
				'marker'    => '',
				'info'      => '',
				'width'     => '',
				'height'    => '500',
				'zoom'      => '13',
				'el_class'  => '',
				'style'     => '1',
				'map_color' => '#2685f9',
			), $atts
		);

		$class = array(
			'cl-map-shortcode',
			$atts['el_class'],
		);

		$style = '';
		if ( $atts['width'] ) {
			$unit = 'px;';
			if ( strpos( $atts['width'], '%' ) ) {
				$unit = '%;';
			}

			$atts['width'] = intval( $atts['width'] );
			$style         .= 'width: ' . $atts['width'] . $unit;
		}
		if ( $atts['height'] ) {
			$unit = 'px;';
			if ( strpos( $atts['height'], '%' ) ) {
				$unit = '%;';
			}

			$atts['height'] = intval( $atts['height'] );
			$style          .= 'height: ' . $atts['height'] . $unit;
		}
		if ( $atts['zoom'] ) {
			$atts['zoom'] = intval( $atts['zoom'] );
		}

		if ( $atts['style'] ) {
			$atts['style'] = intval( $atts['style'] );
		}

		$id   = uniqid( 'cl_map_' );
		$html = sprintf(
			'<div class="%s"><div id="%s" class="cl-map" style="%s"></div></div>',
			implode( ' ', $class ),
			$id,
			$style
		);

		$lats    = array();
		$lng     = array();
		$info    = array();
		$i       = 0;
		$fh_info = vc_param_group_parse_atts( $atts['info'] );

		foreach ( $fh_info as $key => $value ) {

			$coordinates = $this->get_coordinates( $value['address'] );
			$lats[]      = $coordinates['lat'];
			$lng[]       = $coordinates['lng'];
			$info[]      = isset( $value['details'] ) ? $value['details'] : '';

			if ( isset( $coordinates['error'] ) ) {
				return $coordinates['error'];
			}

			$i ++;
		}

		$marker = '';
		if ( $atts['marker'] ) {

			if ( filter_var( $atts['marker'], FILTER_VALIDATE_URL ) ) {
				$marker = $atts['marker'];
			} else {
				$attachment_image = wp_get_attachment_image_src( intval( $atts['marker'] ), 'full' );
				$marker           = $attachment_image ? $attachment_image[0] : '';
			}
		}

		$this->api_key = $atts['api_key'];

		$this->l10n['map'][ $id ] = array(
			'type'      => 'normal',
			'lat'       => $lats,
			'lng'       => $lng,
			'zoom'      => $atts['zoom'],
			'marker'    => $marker,
			'height'    => $atts['height'],
			'info'      => $info,
			'number'    => $i,
			'style'     => $atts['style'],
			'map_color' => $atts['map_color'],
		);

		return $html;

	}

	/**
	 * Helper function to get coordinates for map
	 *
	 * @since 1.0.0
	 *
	 * @param string $address
	 * @param bool $refresh
	 *
	 * @return array
	 */
	function get_coordinates( $address, $refresh = false ) {
		$address_hash = md5( $address );
		$coordinates  = get_transient( $address_hash );
		$results      = array( 'lat' => '', 'lng' => '' );

		if ( $refresh || $coordinates === false ) {
			$args     = array( 'address' => urlencode( $address ), 'sensor' => 'false' );
			$url      = add_query_arg( $args, 'http://maps.googleapis.com/maps/api/geocode/json' );
			$response = wp_remote_get( $url );

			if ( is_wp_error( $response ) ) {
				$results['error'] = esc_html__( 'Can not connect to Google Maps APIs', 'clerina' );

				return $results;
			}

			$data = wp_remote_retrieve_body( $response );

			if ( is_wp_error( $data ) ) {
				$results['error'] = esc_html__( 'Can not connect to Google Maps APIs', 'clerina' );

				return $results;
			}

			if ( $response['response']['code'] == 200 ) {
				$data = json_decode( $data );

				if ( $data->status === 'OK' ) {
					$coordinates = $data->results[0]->geometry->location;

					$results['lat']     = $coordinates->lat;
					$results['lng']     = $coordinates->lng;
					$results['address'] = (string) $data->results[0]->formatted_address;

					// cache coordinates for 3 months
					set_transient( $address_hash, $results, 3600 * 24 * 30 * 3 );
				} elseif ( $data->status === 'ZERO_RESULTS' ) {
					$results['error'] = esc_html__( 'No location found for the entered address.', 'clerina' );
				} elseif ( $data->status === 'INVALID_REQUEST' ) {
					$results['error'] = esc_html__( 'Invalid request. Did you enter an address?', 'clerina' );
				} else {
					$results['error'] = esc_html__( 'Something went wrong while retrieving your map, please ensure you have entered the short code correctly.', 'clerina' );
				}
			} else {
				$results['error'] = esc_html__( 'Unable to contact Google API service.', 'clerina' );
			}
		} else {
			$results = $coordinates; // return cached results
		}

		return $results;
	}

	/**
	 * Shortcode to display latest post
	 *
	 * @param  array $atts
	 * @param  string $content
	 *
	 * @return string
	 */
	function cleanpro_blog_section( $atts, $content ) {
		$atts = shortcode_atts(
			array(
				'number'   => '-1',
				'btn_text' => esc_html__( 'View More', 'clerina' ),
				'el_class' => '',
			), $atts
		);

		$css_class = array(
			'cl-latest-post blog-grid',
			$atts['el_class'],
		);

		$output = array();

		$query_args = array(
			'posts_per_page'      => $atts['number'],
			'post_type'           => 'post',
			'ignore_sticky_posts' => true,
		);

		$query = new WP_Query( $query_args );

		while ( $query->have_posts() ) : $query->the_post();
			global $cl_post;
			$cl_post['css']  = ' blog-wrapper-col-3 col-md-4 col-sm-6 col-xs-12';
			$cl_post['size'] = 'clerina-blog-grid-thumb';

			ob_start();
			get_template_part( 'template-parts/content', get_post_format() );
			$output[] = ob_get_clean();

		endwhile;
		wp_reset_postdata();

		return sprintf(
			'<div class="%s">
                <div class="post-list">%s</div>
            </div>',
			esc_attr( implode( ' ', $css_class ) ),
			implode( '', $output )
		);
	}

	/**
	 * Shortcode to display testimonial carousel
	 *
	 * @param  array $atts
	 * @param  string $content
	 *
	 * @return string
	 */
	function cleanpro_testimonials_carousel( $atts, $content ) {
		$atts = shortcode_atts(
			array(
				'style'          => '1',
				'background'     => '1',
				'rating'         => '',
				'number'         => '-1',
				'nav'            => false,
				'style_nav'      => '1',
				'dot'            => false,
				'autoplay'       => false,
				'autoplay_speed' => '800',
				'column'         => '1',
				'el_class'       => '',
			), $atts
		);

		$css_class = array(
			'cl-testimonial-carousel',
			'style-' . $atts['style'],
			$atts['el_class'],
		);

		$background = $rating = $style_nav = '';
		if ( ! empty( $atts['background'] ) && ( $atts['background'] == '2' ) ) {
			$background = 'light-style';
		}
		if ( ! empty( $atts['rating'] ) ) {
			$background = 'hide-rating';
		}
		if ( ! empty( $atts['style_nav'] ) && ( $atts['style_nav'] == '2' ) ) {
			$style_nav = 'nav-circle';
		}

		$autoplay_speed = intval( $atts['autoplay_speed'] );

		if ( $atts['autoplay'] ) {
			$autoplay = true;
		} else {
			$autoplay = false;
		}

		if ( $atts['nav'] ) {
			$nav = true;
		} else {
			$nav = false;
		}

		if ( $atts['dot'] ) {
			$dot = true;
		} else {
			$dot = false;
		}

		$is_carousel = 1;
		$column      = intval( $atts['column'] );

		$id                               = uniqid( 'testimonial-slider-' );
		$this->l10n['testimonial'][ $id ] = array(
			'column'         => $column,
			'nav'            => $nav,
			'dot'            => $dot,
			'autoplay'       => $autoplay,
			'autoplay_speed' => $autoplay_speed,
			'is_carousel'    => $is_carousel,
		);

		$output = array();

		$query_args = array(
			'posts_per_page'      => $atts['number'],
			'post_type'           => 'testimonial',
			'ignore_sticky_posts' => true,
		);

		$query = new WP_Query( $query_args );

		while ( $query->have_posts() ) : $query->the_post();

			ob_start();
			get_template_part( 'template-parts/content', 'testimonial' );
			$output[] = ob_get_clean();

		endwhile;
		wp_reset_postdata();

		return sprintf(
			'<div class="%s">
                <div class="testimonial-list %s %s" id="%s">%s</div>
            </div>',
			esc_attr( implode( ' ', $css_class ) ),
			$background,
			$style_nav,
			esc_attr( $id ),
			implode( '', $output )
		);
	}

}


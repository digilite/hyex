<?php
/**
 * Functions that used in the theme and plugin.
 */

/**
 * Get the sharing URL of a social
 *
 * @param string $social
 *
 * @return string
 */
function konte_addons_share_link( $social ) {
	$url    = '';
	$text   = esc_html__( 'Share on', 'konte-addons' ) . ' ' . ucfirst( $social );
	$icon   = $social;

	switch ( $social ) {
		case 'facebook':
			$url = add_query_arg( array( 'u' => get_permalink() ), 'https://www.facebook.com/sharer.php' );
			break;

		case 'twitter':
			$url = add_query_arg( array( 'url' => get_permalink(), 'text' => get_the_title() ), 'https://twitter.com/intent/tweet' );
			break;

		case 'pinterest';
			$params         = array(
				'description' => get_the_title(),
				'media'       => get_the_post_thumbnail_url( null, 'full' ),
				'url'         => get_permalink(),
			);
			$url            = add_query_arg( $params, 'https://www.pinterest.com/pin/create/button/' );
			$icon           = 'pinterest-p';
			break;

		case 'googleplus':
			$url = add_query_arg( array( 'url' => get_permalink() ), 'https://www.facebook.com/sharer.php' );
			$text = esc_html__( 'Share on Google+', 'konte-addons' );
			$icon = 'google-plus';
			break;

		case 'linkedin':
			$url = add_query_arg( array( 'url' => get_permalink(), 'title' => get_the_title() ), 'https://www.linkedin.com/shareArticle' );
			break;

		case 'tumblr':
			$url = add_query_arg( array( 'url' => get_permalink(), 'name' => get_the_title() ), 'https://www.tumblr.com/share/link' );
			break;

		case 'reddit':
			$url = add_query_arg( array( 'url' => get_permalink(), 'title' => get_the_title() ), 'https://reddit.com/submit' );
			break;

		case 'stumbleupon':
			$url = add_query_arg( array( 'url' => get_permalink(), 'title' => get_the_title() ), 'https://www.stumbleupon.com/submit' );
			$text = esc_html__( 'Share On StumbleUpon', 'konte-addons' );
			break;

		case 'telegram':
			$url = add_query_arg( array( 'url' => get_permalink() ), 'https://t.me/share/url' );
			break;

		case 'pocket':
			$url = add_query_arg( array( 'url' => get_permalink(), 'title' => get_the_title() ), 'https://getpocket.com/save' );
			$text = esc_html__( 'Save On Pocket', 'konte-addons' );
			$icon = 'get-pocket';
			break;

		case 'digg':
			$url = add_query_arg( array( 'url' => get_permalink() ), 'https://digg.com/submit' );
			break;

		case 'vk':
			$url = add_query_arg( array( 'url' => get_permalink() ), 'https://vk.com/share.php' );
			break;

		case 'email':
			$url  = 'mailto:?subject=' . get_the_title() . '&body=' . __( 'Check out this site:', 'konte-addons' ) . ' ' . get_permalink();
			$text = esc_html__( 'Share Via Email', 'konte-addons' );
			$icon = 'envelope';
			break;
	}

	if ( ! $url ) {
		return;
	}

	return sprintf(
		'<a href="%s" target="_blank" class="social-share-link %s"><i class="fa fa-%s"></i><span>%s</span></a>',
		esc_url( $url ),
		esc_attr( $social ),
		esc_attr( $icon ),
		$text
	);
}

/**
 * Recursive merge user defined arguments into defaults array.
 *
 * @param array $args
 * @param array $default
 *
 * @return array
 */
function konte_addons_recurse_parse_args( $args, $default = array() ) {
	$args   = (array) $args;
	$result = $default;

	foreach ( $args as $key => $value ) {
		if ( is_array( $value ) && isset( $result[ $key ] ) ) {
			$result[ $key ] = konte_addons_recurse_parse_args( $value, $result[ $key ] );
		} else {
			$result[ $key ] = $value;
		}
	}

	return $result;
}
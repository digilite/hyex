<# if ( data.depth == 0 ) { #>
<a href="#" class="media-menu-item active" data-title="<?php esc_attr_e( 'Mega Menu', 'konte-addons' ) ?>" data-panel="mega"><?php esc_html_e( 'Mega Menu', 'konte-addons' ) ?></a>
<a href="#" class="media-menu-item" data-title="<?php esc_attr_e( 'Mega Menu Design', 'konte-addons' ) ?>" data-panel="design"><?php esc_html_e( 'Design', 'konte-addons' ) ?></a>
<div class="separator"></div>
<a href="#" class="media-menu-item" data-title="<?php esc_attr_e( 'Menu Icon', 'konte-addons' ) ?>" data-panel="icon"><?php esc_html_e( 'Icon', 'konte-addons' ) ?></a>
<# } else if ( data.depth == 1 ) { #>
<a href="#" class="media-menu-item active" data-title="<?php esc_attr_e( 'Menu Setting', 'konte-addons' ) ?>" data-panel="settings"><?php esc_html_e( 'Settings', 'konte-addons' ) ?></a>
<a href="#" class="media-menu-item" data-title="<?php esc_attr_e( 'Mega Column Design', 'konte-addons' ) ?>" data-panel="design"><?php esc_html_e( 'Design', 'konte-addons' ) ?></a>
<a href="#" class="media-menu-item" data-title="<?php esc_attr_e( 'Menu Content', 'konte-addons' ) ?>" data-panel="content"><?php esc_html_e( 'Content', 'konte-addons' ) ?></a>
<div class="separator"></div>
<a href="#" class="media-menu-item" data-title="<?php esc_attr_e( 'Menu Icon', 'konte-addons' ) ?>" data-panel="icon"><?php esc_html_e( 'Icon', 'konte-addons' ) ?></a>
<# } else { #>
<a href="#" class="media-menu-item active" data-title="<?php esc_attr_e( 'Menu Content', 'konte-addons' ) ?>" data-panel="content"><?php esc_html_e( 'Content', 'konte-addons' ) ?></a>
<div class="separator"></div>
<a href="#" class="media-menu-item" data-title="<?php esc_attr_e( 'Menu Icon', 'konte-addons' ) ?>" data-panel="icon"><?php esc_html_e( 'Icon', 'konte-addons' ) ?></a>
<# } #>

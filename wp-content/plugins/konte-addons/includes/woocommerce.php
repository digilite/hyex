<?php
/**
 * WooCommerce hooks and functions.
 */

/**
 * Remove subcategories from the shop page and tell the Konte theme know that removed.
 */
function konte_addons_remove_shop_subcategories() {
	remove_filter( 'woocommerce_product_loop_start', 'woocommerce_maybe_show_product_subcategories' );
}

add_action( 'woocommerce_before_shop_loop', 'konte_addons_remove_shop_subcategories', 49 );
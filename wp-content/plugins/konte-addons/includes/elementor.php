<?php
namespace KonteAddons;

/**
 * Integrate with Elementor.
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class Elementor {
	/**
	 * Instance
	 *
	 * @access private
	 */
	private static $_instance = null;

	/**
	 * Instance
	 *
	 * Ensures only one instance of the class is loaded or can be loaded.
	 *
	 * @return Konte_Addons_Elementor An instance of the class.
	 */
	public static function instance() {
		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self();
		}

		return self::$_instance;
	}

	/**
	 * Constructor
	 */
	public function __construct() {
		spl_autoload_register( [ $this, 'autoload' ] );

		$this->_includes();
		$this->add_actions();
	}

	/**
	 * Auto load widgets
	 */
	public function autoload( $class ) {
		if ( 0 !== strpos( $class, __NAMESPACE__ ) ) {
			return;
		}

		if ( false === strpos( $class, 'Widgets' ) ) {
			return;
		}

		$path = explode( '\\', $class );
		$filename = strtolower( array_pop( $path ) );

		if ( 'Widgets' != array_pop( $path ) ) {
			return;
		}

		$filename = str_replace( '_', '-', $filename );
		$filename = KONTE_ADDONS_DIR . 'includes/elementor-widgets/' . $filename . '.php';

		if ( is_readable( $filename ) ) {
			include( $filename );
		}
	}

	/**
	 * Includes files which are not widgets
	 */
	private function _includes() {}

	/**
	 * Hooks to init
	 */
	protected function add_actions() {
		add_action( 'elementor/frontend/after_enqueue_styles', [ $this, 'styles' ] );
		add_action( 'elementor/frontend/after_register_scripts', [ $this, 'scripts' ] );

		add_action( 'elementor/controls/controls_registered', [ $this, 'init_controls' ] );
		add_action( 'elementor/widgets/widgets_registered', [ $this, 'init_widgets' ] );

		add_action( 'elementor/elements/categories_registered', [ $this, 'add_category' ] );
	}

	/**
	 * Register styles
	 */
	public function styles() {

	}

	/**
	 * Register styles
	 */
	public function scripts() {

	}

	/**
	 * Init Controls
	 */
	public function init_controls() {

	}

	/**
	 * Init Widgets
	 */
	public function init_widgets() {
		$widgets_manager = \Elementor\Plugin::instance()->widgets_manager;

		$widgets_manager->register_widget_type( new \KonteAddons\Elementor\Widgets\Icon_Box() );
	}

	/**
	 * Add Konte category
	 */
	public function add_category( $elements_manager ) {
		$elements_manager->add_category(
			'konte',
			[
				'title' => __( 'Konte', 'konte-addons' )
			]
		);
	}
}

Elementor::instance();
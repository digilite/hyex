<?php
class Clerina_Booking_Form_Handler {
	public static function init() {
		add_action( 'wp_loaded', array( __CLASS__, 'form_submit' ) );
		add_action( 'wp_ajax_cpb_submit_form', array( __CLASS__, 'ajax_form_submit' ) );
		add_action( 'wp_ajax_nopriv_cpb_submit_form', array( __CLASS__, 'ajax_form_submit' ) );

		add_action( 'cpb_added_new_order', array( __CLASS__, 'send_notification_email' ), 10, 3 );
	}

	/**
	 * Handle form submitting.
	 */
	public static function form_submit() {
		if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {
			return;
		}

		if ( empty( $_POST['cpb_form'] ) ) {
			return;
		}

		$form_id = intval( $_POST['cpb_form'] );

		if ( empty( $_POST['cpb_nonce'] ) || ! wp_verify_nonce( $_POST['cpb_nonce'], 'cpb_form_' . $form_id ) ) {
			return;
		}

		self::create_new_order( $form_id );

		wp_safe_redirect( add_query_arg( array( 'order_created' => '1' ) ) );
		exit;
	}

	/**
	 * Handle form submit via ajax.
	 */
	public static function ajax_form_submit() {
		if ( empty( $_POST['cpb_form'] ) ) {
			wp_send_json_error( __( 'No form provided', 'clerina-booking' ) );
			exit;
		}

		$form_id = intval( $_POST['cpb_form'] );

		if ( empty( $_POST['cpb_nonce'] ) || ! wp_verify_nonce( $_POST['cpb_nonce'], 'cpb_form_' . $form_id ) ) {
			wp_send_json_error( __( 'Verify failed', 'clerina-booking' ) );
			exit;
		}

		$data = self::create_new_order( $form_id );

		wp_send_json_success( $data );
		exit;
	}

	/**
	 * Create new order from submitted form data.
	 *
	 * @param int $form_id
	 */
	protected static function create_new_order( $form_id ) {
		$fields = get_post_meta( $form_id, '_form_fields', true );

		// Create a new order.
		$order_id = wp_insert_post( array(
			'post_title' => '',
			'post_content' => '',
			'post_status' => 'publish',
			'post_type' => 'cpb_order',
		) );

		wp_update_post( array(
			'ID' => $order_id,
			'post_title' => '#' . $order_id,
		) );

		$data = self::parse_form_data( $fields );

		if ( isset( $_POST['__cpb_linked_form'] ) ) {
			$linked_fields = get_post_meta( absint( $_POST['__cpb_linked_form'] ), '_form_fields', true );
			$data = array_merge( $data, self::parse_form_data( $linked_fields ) );
		}

		update_post_meta( $order_id, '_order_data', $data );
		update_post_meta( $order_id, '_order_form', $form_id );

		do_action( 'cpb_added_new_order', $order_id, $data, $form_id );

		return $data;
	}

	/**
	 * Parse the submitted form data.
	 *
	 * @param array $fields
	 *
	 * @return array
	 */
	public static function parse_form_data( $fields ) {
		$data = array();
		$linked_forms = array();

		foreach ( $fields as $field ) {
			$name = ! empty( $field['name'] ) ? $field['name'] : '';

			if ( ! $name && 'columns' != $field['type'] ) {
				continue;
			}

			switch ( $field['type'] ) {
				case 'columns':
					foreach ( $field['fields'] as $child_fields ) {
						$data = array_merge( $data, self::parse_form_data( $child_fields ) );
					}
					break;

				case 'header':
				case 'paragraph':
					break;

				case 'checkbox':
					$data[$name] = array(
						'type' => $field['type'],
						'label' => ! empty( $field['admin_label'] ) ? $field['admin_label'] : ( ! empty( $field['label'] ) ? $field['label'] : $name ),
						'value' => empty( $_POST[$name] ) ? '' : $_POST[$name],
					);
					break;

				case 'radio':
					if ( ! empty( $_POST[$name] ) ) {
						$value = $_POST[$name];

						foreach ( $field['options'] as $option ) {
							if ( $option['value'] == $value ) {
								$value = $option['label'] . ' (' . $value . ')';
							}
						}
					} else {
						$value = '';
					}

					$data[$name] = array(
						'type' => $field['type'],
						'label' => ! empty( $field['admin_label'] ) ? $field['admin_label'] : ( ! empty( $field['label'] ) ? $field['label'] : $name ),
						'value' => $value,
					);
					break;

				case 'checklist':
					if ( ! empty( $_POST[$name] ) ) {
						$list = $_POST[$name];
						$value = array();

						foreach ( $field['options'] as $option ) {
							if ( in_array( $option['value'], $list ) ) {
								$value[ $option['value'] ] = $option['label'] . ' (' . $option['value'] . ')';
							}
						}
					} else {
						$value = array();
					}

					$data[$name] = array(
						'type' => $field['type'],
						'label' => ! empty( $field['admin_label'] ) ? $field['admin_label'] : ( ! empty( $field['label'] ) ? $field['label'] : $name ),
						'value' => $value,
					);
					break;

				case 'estimate':
					if ( empty( $_POST[$name] ) ) {
						break;
					}

					$data[$name] = array(
						'type' => $field['type'],
						'label' => ! empty( $field['admin_label'] ) ? $field['admin_label'] : ( ! empty( $field['label'] ) ? $field['label'] : $name ),
						'value' => clerina_booking_format_price( $_POST[$name], $field['num_decimals'], $field['currency'], $field['currency_position'] ),
					);
					break;

				case 'pricing_table':
					if ( empty( $_POST[$name] ) ) {
						break;
					}

					// if ( 'open_new_form' == $field['button_behavior'] ) {
					// 	$linked_forms[] = $field['button_linked'];
					// }

					$data[$name] = array(
						'type' => $field['type'],
						'label' => ! empty( $field['admin_label'] ) ? $field['admin_label'] : ( ! empty( $field['label'] ) ? $field['label'] : $name ),
						'value' => $_POST[$name],
					);

					$data[$name . '_estimate_price'] = array(
						'type' => 'estimate',
						'label' => _x( 'Estimate Price', 'Estimate price of pricing table in an order', 'clerina-booking' ),
						'value' => clerina_booking_format_price( $_POST[$name . '_estimate_price'], $field['num_decimals'], $field['currency'], $field['currency_position'] ),
					);
					break;

				case 'button':
					// if ( 'open_new_form' == $field['behavior'] ) {
					// 	$linked_forms[] = $field['linked'];
					// }
					break;

				default:
					if ( ! isset( $_POST[$name] ) ) {
						break;
					}

					$data[$name] = array(
						'type' => $field['type'],
						'label' => ! empty( $field['admin_label'] ) ? $field['admin_label'] : ( ! empty( $field['label'] ) ? $field['label'] : $name ),
						'value' => $_POST[$name],
					);
					break;
			}
		}

		return $data;
	}

	/**
	 * Send a notification email to admin.
	 */
	public static function send_notification_email( $order_id, $order_data, $form_id ) {
		$settings = get_post_meta( $form_id, '_form_mail_settings', true );
		$default = array(
			'field_email' => '',
			'field_name' => '',
			'mail_subject' => 'You have just made an order on website [site_name]',
			'mail_body' => 'Hello [customer_name],

You have just made an order on our website - [site_name]. This is your order.

[order_details]',
		);
		$settings = wp_parse_args( $settings, $default );

		if ( empty( $settings ) || empty( $settings['mail_to'] ) ) {
			return;
		}

		$email = $settings['field_email'];

		if ( empty( $order_data[ $email ] ) ) {
			return;
		}

		$email = $order_data[ $email ];

		if ( ! is_email( $email ) ) {
			return;
		}

		$replacements = array(
			'[site_name]' => get_bloginfo( 'name' ),
			'[order_details]' => clerina_booking_get_order_details_html( $order_data ),
			'[customer_name]' => $name,
			'[customer_email]' => $email,
		);

		$name = $settings['field_name'];
		$name = ! empty( $order_data[ $name ] ) ? $order_data[ $name ] : '';
		$subject = empty( $settings['mail_subject'] ) ? $default['mail_subject'] : $settings['mail_subject'];
		$subject = str_replace( '[site_name]', get_bloginfo( 'name' ), $subject );
		$body = empty( $settings['mail_body'] ) ? $default['mail_body'] : $settings['mail_body'];
		$body = str_replace( array_keys( $replacements ), array_values( $replacements ), $body );
		$headers = array( 'Content-Type: text/html; charset=UTF-8' );

		// Send an email to customer.
		wp_mail( $email, $subject, $body, $headers );

		// Send an email to the site owner.
		$admin_email = get_bloginfo( 'admin_email' );
		$subject = __( 'New order was made', 'clerina-booking' ) . ' - #' . $order_id;
		$body = __( 'You have received an order from [customer_name]. The order is as follows: [order_details]', 'clerina-booking' );
		$body = str_replace( array_keys( $replacements ), array_values( $replacements ), $body );

		wp_mail( $admin_email, $subject, $body, $headers );
	}
}
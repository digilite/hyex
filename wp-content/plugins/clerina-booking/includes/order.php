<?php
class Clerina_Booking_Order {
	private $post_type = 'cpb_order';

	/**
	 * The single instance of the class
	 */
	protected static $instance = null;

	/**
	 * Initialize
	 */
	public static function init() {
		if ( null == self::$instance ) {
			self::$instance = new self();
		}

		return self::$instance;
	}

	/**
	 * Construction
	 */
	public function __construct() {
		$this->register_post_type();

		add_action( 'add_meta_boxes', array( $this, 'add_meta_boxes' ) );
		add_action( 'save_post', array( $this, 'save_meta_boxes' ), 10, 2 );
	}

	/**
	 * Register the order post type.
	 */
	public function register_post_type() {
		register_post_type( $this->post_type, array(
			'labels'              => array(
				'name'                  => esc_html__( 'Orders', 'clerina-booking' ),
				'singular_name'         => esc_html__( 'Order', 'clerina-booking' ),
				'menu_name'             => esc_html__( 'Orders', 'clerina-booking' ),
				'all_items'             => esc_html__( 'Orders', 'clerina-booking' ),
				'add_new'               => esc_html__( 'Add Order', 'clerina-booking' ),
				'add_new_item'          => esc_html__( 'Add New', 'clerina-booking' ),
				'edit_item'             => esc_html__( 'Edit Order', 'clerina-booking' ),
				'new_item'              => esc_html__( 'New Order', 'clerina-booking' ),
				'view_item'             => esc_html__( 'View Order', 'clerina-booking' ),
				'search_items'          => esc_html__( 'Search orders', 'clerina-booking' ),
				'not_found'             => esc_html__( 'No orders found', 'clerina-booking' ),
				'not_found_in_trash'    => esc_html__( 'No orders found in Trash', 'clerina-booking' ),
				'filter_items_list'     => esc_html__( 'Filter orders list', 'clerina-booking' ),
				'items_list_navigation' => esc_html__( 'Order list navigation', 'clerina-booking' ),
				'items_list'            => esc_html__( 'Order list', 'clerina-booking' ),
			),
			'supports'            => array( 'title' ),
			'public'              => false,
			'show_ui'             => true,
			'show_in_menu'        => current_user_can( 'manage_options' ) ? 'clerina-booking' : true,
			'show_in_nav_menus'   => false,
			'rewrite'             => false,
			'capability_type'     => 'page',
			'publicly_queryable'  => false,
			'exclude_from_search' => true,
			'hierarchical'        => false,
			'has_archive'         => false,
			'query_var'           => is_admin(),
		) );
	}

	/**
	 * Add meta boxes.
	 */
	public function add_meta_boxes() {
		add_meta_box(
			'clerina-booking-form-data',
			__( 'Order Details', 'clerina-booking' ),
			array( $this, 'order_details_meta_box' ),
			$this->post_type,
			'advanced',
			'high'
		);
	}

	/**
	 * Form fields meta box.
	 *
	 * @param object $post
	 */
	public function order_details_meta_box( $post ) {
		wp_nonce_field( 'cpb_order_details', 'cpb_nonce' );

		$order = get_post_meta( $post->ID, '_order_data', true );

		if ( ! $order ) {
			return;
		}

		include dirname( __FILE__ ) . '/views/meta-box-order-details.php';
	}

	/**
	 * Handles saving the meta box.
	 *
	 * @param int     $post_id Post ID.
	 * @param WP_Post $post    Post object.
	 * @return null
	 */
	public function save_meta_boxes( $post_id, $post ) {
		// Add nonce for security and authentication.
		$nonce_name   = isset( $_POST['cpb_nonce'] ) ? $_POST['cpb_nonce'] : '';
		$nonce_action = 'cpb_order_details';

		// Check if nonce is set.
		if ( ! isset( $nonce_name ) ) {
			return;
		}

		// Check if nonce is valid.
		if ( ! wp_verify_nonce( $nonce_name, $nonce_action ) ) {
			return;
		}

		// Check if user has permissions to save data.
		if ( ! current_user_can( 'edit_post', $post_id ) ) {
			return;
		}

		// Check if not an autosave.
		if ( wp_is_post_autosave( $post_id ) ) {
			return;
		}

		// Check if not a revision.
		if ( wp_is_post_revision( $post_id ) ) {
			return;
		}

		if ( isset( $_POST['_order_data'] ) ) {
			update_post_meta( $post_id, '_order_data', $_POST['_order_data'] );
		}
	}
}
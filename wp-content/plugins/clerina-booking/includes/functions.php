<?php

/**
 * Get field class name.
 *
 * @param string $field_type
 *
 * @return string
 */
function clerina_booking_get_field_classname( $field_type ) {
	$type  = str_replace( array( '-', '_' ), ' ', $field_type );
	$class = 'Clerina_Booking_' . ucwords( $type ) . '_Field';
	$class = str_replace( ' ', '_', $class );

	return apply_filters( 'clerina_booking_field_classname', $class, $field_type );
}

/**
 * Get all available fields.
 *
 * @return array
 */
function clerina_booking_get_fields() {
	$fields = array(
		array(
			'type'     => 'text',
			'name'     => __( 'Text', 'clerina-booking' ),
			'settings' => array(
				'required'    => '',
				'label'       => '',
				'name'        => '',
				'value'       => '',
				'placeholder' => '',
				'class'       => '',
				'admin_label' => '',
			),
		),
		array(
			'type'     => 'textarea',
			'name'     => __( 'Textarea', 'clerina-booking' ),
			'settings' => array(
				'required'    => '',
				'label'       => '',
				'name'        => '',
				'value'       => '',
				'placeholder' => '',
				'class'       => '',
				'rows'        => '3',
				'admin_label' => '',
			),
		),
		array(
			'type'     => 'number',
			'name'     => __( 'Number', 'clerina-booking' ),
			'settings' => array(
				'required'    => '',
				'label'       => '',
				'name'        => '',
				'value'       => '',
				'placeholder' => '',
				'class'       => '',
				'min'         => '0',
				'max'         => '100',
				'step'        => '1',
				'price' => '',
				'admin_label' => '',
			),
		),
		array(
			'type'     => 'checkbox',
			'name'     => __( 'Checkbox', 'clerina-booking' ),
			'settings' => array(
				'required' => '',
				'label'    => '',
				'name'     => '',
				'value'    => '1',
				'class'    => '',
				'price' => '',
				'admin_label' => '',
			),
		),
		array(
			'type'     => 'select',
			'name'     => __( 'Select', 'clerina-booking' ),
			'settings' => array(
				'required' => '',
				'label'    => '',
				'name'     => '',
				'class'    => '',
				'selected' => 0,
				'options'  => array(),
				'admin_label' => '',
			),
		),
		array(
			'type'     => 'radio',
			'name'     => __( 'Radio', 'clerina-booking' ),
			'settings' => array(
				'required' => '',
				'label'    => '',
				'name'     => '',
				'class'    => '',
				'selected' => 0,
				'style'    => 'normal',
				'options'  => array(),
				'admin_label' => '',
			),
		),
		array(
			'type'     => 'checklist',
			'name'     => __( 'Checklist', 'clerina-booking' ),
			'settings' => array(
				'required' => '',
				'label'    => '',
				'name'     => '',
				'class'    => '',
				'selected' => array(),
				'options'  => array(),
				'admin_label' => '',
			),
		),
		array(
			'type'     => 'date',
			'name'     => __( 'Date', 'clerina-booking' ),
			'settings' => array(
				'required'    => '',
				'label'       => '',
				'name'        => '',
				'value'       => '',
				'placeholder' => '',
				'class'       => '',
				'admin_label' => '',
			),
		),
		array(
			'type'     => 'range',
			'name'     => __( 'Range', 'clerina-booking' ),
			'settings' => array(
				'required'    => '',
				'label'       => '',
				'name'        => '',
				'value'       => '0',
				'placeholder' => '',
				'class'       => '',
				'min'         => '0',
				'max'         => '100',
				'step'        => '1',
				'price' => '',
				'admin_label' => '',
			),
		),
		array(
			'type'     => 'button',
			'name'     => __( 'Button', 'clerina-booking' ),
			'settings' => array(
				'label' => esc_attr__( 'Submit', 'clerina-booking' ),
				'class' => '',
				'behavior' => 'submit',
				'linked' => '',
				'admin_label' => '',
			),
		),
		array(
			'type'     => 'columns',
			'name'     => __( 'Columns', 'clerina-booking' ),
			'settings' => array(
				'class'  => '',
				'fields' => array(),
			),
		),
		array(
			'type'     => 'header',
			'name'     => __( 'Header', 'clerina-booking' ),
			'settings' => array(
				'text'  => '',
				'tag'   => 'h2',
				'class' => '',
			),
		),
		array(
			'type'     => 'paragraph',
			'name'     => __( 'Paragraph', 'clerina-booking' ),
			'settings' => array(
				'text'  => '',
				'class' => '',
			),
		),
		array(
			'type'     => 'estimate',
			'name'     => __( 'Price Estimate', 'clerina-booking' ),
			'settings' => array(
				'label'              => '',
				'name'              => '',
				'base'              => 0,
				'currency'          => '$',
				'currency_position' => 'left',
				'num_decimals'      => 2,
				'class'             => '',
				'admin_label' => '',
			),
		),
		array(
			'type'     => 'pricing_table',
			'name'     => __( 'Pricing Table', 'clerina-booking' ),
			'settings' => array(
				'label'             => '',
				'name'              => '',
				'base'              => 0,
				'currency'          => '$',
				'currency_position' => 'left',
				'num_decimals'      => 2,
				'description'       => '',
				'unit'              => esc_attr__( 'per month', 'clerina-booking' ),
				'button_text'       => esc_attr__( 'Book Now', 'clerina-booking' ),
				'button_behavior' => 'submit',
				'button_linked' => '',
				'class'             => '',
				'admin_label' => '',
			),
		),
	);

	return apply_filters( 'clerina_booking_form_fields', $fields );
}

/**
 * Format price
 *
 * @param float $price
 *
 * @return string
 */
function clerina_booking_format_price( $price, $decimals = 2, $currency = '$', $position = 'left' ) {
	$price = number_format_i18n( floatval( $price ), $decimals );
	$price = '<span class="cpb-price-amount">' . $price . '</span>';

	switch ( $position ) {
		case 'right':
			$symbol = '<span class="cpb-currency-symbol">' . trim( $currency ) . '</span>';
			$output = $price . $symbol;
			break;

		case 'right_space':
			$symbol = '<span class="cpb-currency-symbol">&nbsp;' . trim( $currency ) . '</span>';
			$output = $price . $symbol;
			break;

		case 'left_space':
			$symbol = '<span class="cpb-currency-symbol">' . trim( $currency ) . '&nbsp;</span>';
			$output = $symbol . $price;
			break;

		default:
			$symbol = '<span class="cpb-currency-symbol">' . trim( $currency ) . '</span>';
			$output = $symbol . $price;
			break;
	}

	return '<span class="cpb-price">' . $output . '</span>';
}

/**
 * Get order detail HTML for the notification email.
 *
 * @param  array $order_data
 * @return string
 */
function clerina_booking_get_order_details_html( $order_data ) {
	ob_start();
	?>

	<table class="form-table">
		<?php foreach ( $order_data as $detail ) : ?>
			<tr>
				<th>
					<label><?php echo $detail['label'] ?></label><br>
				</th>
				<td>
					<?php
					if ( is_array( $detail['value'] ) ) {
						echo '<ul><li>' . implode( '</li><li>', $detail['value'] ) . '</li></ul>';
					} else {
						echo $detail['value'];
					}
					?>
				</td>
			</tr>
		<?php endforeach; ?>
	</table>

	<?php

	return ob_get_clean();
}

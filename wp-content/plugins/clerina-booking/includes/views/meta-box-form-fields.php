<div id="cpb-form-fields" class="cpb-form-fields cpb-form-fields-container">
	<?php if ( empty( $fields ) ) : ?>
		<p class="empty-fieldset description"><?php _e( 'Add fields here', 'clerina-booking' ) ?></p>
	<?php else : ?>
		<div id="cpb-form-fields-data" data-fields="<?php echo esc_attr( json_encode( $fields ) ); ?>"></div>
	<?php endif; ?>
</div>

<div id="cpb-form-avaiable-fields" class="cpb-form-avaiable-fields">
	<?php foreach ( clerina_booking_get_fields() as $field ) : ?>
		<div class="cpb-form-field cpb-form-field-<?php echo esc_attr( $field['type'] ) ?>" data-type="<?php echo esc_attr( $field['type'] ) ?>" data-settings="<?php echo ! empty( $field['settings'] ) ? esc_attr( json_encode( $field['settings'] ) ) : ''; ?>">
			<div class="cpb-form-field-actions">
				<span class="field-control add-field"><?php _e( 'Add +', 'clerina-booking' ) ?></span>
				<span class="field-control edit-field"><i class="dashicons dashicons-edit"></i></span>
				<span class="field-control remove-field"><i class="dashicons dashicons-no-alt"></i></span>
			</div>
			<h4><?php echo $field['name'] ?></h4>
		</div>
	<?php endforeach; ?>
</div>

<div id="cpb-form-field-settings" class="cpb-form-field-settings">
	<div class="cpb-form-field-settings-actions">
		<button type="button" class="button-primary save"><?php _e( 'Save Changes', 'clerina-booking' ) ?></button>
		<button type="button" class="button-secondary cancel"><?php _e( 'Cancel', 'clerina-booking' ) ?></button>
	</div>
</div>

<input type="hidden" value="_form_fields" id="cpb-field-base-name">

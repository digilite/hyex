<table class="form-table">
	<?php foreach ( $order as $detail ) : ?>
		<tr>
			<th>
				<label><?php echo $detail['label'] ?></label><br>
				<p class="description"><?php echo $detail['type'] ?></p>
			</th>
			<td>
				<?php
				if ( is_array( $detail['value'] ) ) {
					echo '<ul><li>' . implode( '</li><li>', $detail['value'] ) . '</li></ul>';
				} else {
					echo $detail['value'];
				}
				?>
			</td>
		</tr>
	<?php endforeach; ?>
</table>

<table class="form-table">
	<tr>
		<th><label for="cbp-form-message-confirm"><?php _e( 'Confirmation message', 'clerina-booking' ) ?></label></th>
		<td>
			<textarea id="cbp-form-message-confirm" rows="3" name="_form_messages[confirm]" class="widefat textarea"><?php echo wp_unslash( $messages['confirm'] ); ?></textarea>
		</td>
	</tr>
	<tr>
		<th><label for="cbp-form-message-success"><?php _e( 'Success message', 'clerina-booking' ) ?></label></th>
		<td>
			<textarea id="cbp-form-message-success" rows="3" name="_form_messages[success]" class="widefat textarea"><?php echo wp_unslash( $messages['success'] ); ?></textarea>
		</td>
	</tr>

	<tr>
		<th><label for="cbp-form-message-failed"><?php _e( 'Failed message', 'clerina-booking' ) ?></label></th>
		<td>
			<textarea id="cbp-form-message-failed" rows="3" name="_form_messages[failed]" class="widefat textarea"><?php echo wp_unslash( $messages['failed'] ); ?></textarea>
		</td>
	</tr>

	<tr>
		<th><label for="cbp-form-message-error"><?php _e( 'Error message', 'clerina-booking' ) ?></label></th>
		<td>
			<textarea id="cbp-form-message-failed" rows="3" name="_form_messages[error]" class="widefat textarea"><?php echo wp_unslash( $messages['error'] ); ?></textarea>
		</td>
	</tr>
</table>
<?php
class Clerina_Booking_Radio_Field extends Clerina_Booking_Select_Field {
	public $type = 'radio';

	/**
	 * Template for field settings.
	 */
	public function settings_template_content() {
		?>

		<input type="hidden" value="<?php echo esc_attr( $this->type ) ?>" name="<?php echo $this->setting_name( 'type' ); ?>">
		<input type="hidden" value="<?php echo $this->setting_name( 'options' ); ?>" class="option-base-name">

		<table class="form-table">
			<tr>
				<th><label><?php _e( 'Required', 'clerina-booking' ) ?></label></th>
				<td><input type="checkbox" value="1" name="<?php echo $this->setting_name( 'required' ); ?>" <?php echo $this->setting_checked( 'required', '1' ) ?>></td>
			</tr>
			<tr>
				<th><label><?php _e( 'Label', 'clerina-booking' ) ?></label></th>
				<td><input type="text" value="{{data.label}}" name="<?php echo $this->setting_name( 'label' ); ?>" class="widefat field-label-input"></td>
			</tr>
			<tr>
				<th><label><?php _e( 'Name', 'clerina-booking' ) ?></label></th>
				<td><input type="text" value="{{data.name}}" name="<?php echo $this->setting_name( 'name' ); ?>" class="widefat"></td>
			</tr>
			<tr>
				<th><label><?php _e( 'Class', 'clerina-booking' ) ?></label></th>
				<td><input type="text" value="{{data.class}}" name="<?php echo $this->setting_name( 'class' ); ?>" class="widefat"></td>
			</tr>
			<tr>
				<th><label><?php _e( 'Style', 'clerina-booking' ) ?></label></th>
				<td>
					<label>
						<input type="radio" value="normal" name="<?php echo $this->setting_name( 'style' ); ?>" class="widefat" {{'normal' == data.style ? 'checked' : ''}}>
						<?php _e( 'Normal', 'clerina-booking' ) ?>
					</label>
					&nbsp;&nbsp;&nbsp;
					<label>
						<input type="radio" value="buttons" name="<?php echo $this->setting_name( 'style' ); ?>" class="widefat" {{'buttons' == data.style ? 'checked' : ''}}>
						<?php _e( 'Toggle Button', 'clerina-booking' ) ?>
					</label>
				</td>
			</tr>
			<tr>
				<th><label><?php _e( 'Options', 'clerina-booking' ) ?></label></th>
				<td>
					<div class="field-options" data-index="{{data.index}}">
						<# if ( ! _.isEmpty( data.options ) ) { #>
						<# _.each( data.options, function( value, key ) { #>
						<?php $this->option_settings_template_content(); ?>
						<# } ); #>
						<# } else { #>
						<# var key = 0, value = {label: '', value: '', price: ''}; #>
						<?php $this->option_settings_template_content(); ?>
						<# } #>
					</div>
					<p>
						<a href="#" class="field-options-add-new" data-type="<?php echo esc_attr( $this->type ) ?>"><?php _e( '+ Add New Option', 'clerina-booking' ) ?></a>
					</p>
				</td>
			</tr>
			<tr>
				<th><label><?php _e( 'Admin Label', 'clerina-booking' ) ?></label></th>
				<td>
					<input type="text" value="{{data.admin_label}}" name="<?php echo $this->setting_name( 'admin_label' ); ?>" class="widefat admin-label-input">
					<p class="description"><?php _e( 'The label to be used in the order', 'clerina-booking' ) ?></p>
				</td>
			</tr>
		</table>

		<?php
	}

	/**
	 * Render the field.
	 *
	 * @param array $settings
	 *
	 * @return string
	 */
	public function render( $settings = array() ) {
		$settings = wp_parse_args( $settings, $this->get_default_settings() );
		$price_data = $this->get_price_data( $settings['options'] );

		$output = '<div class="cpb-field cpb-radio-field ' . esc_attr( $settings['class'] ) . '" data-price="' . esc_attr( json_encode( $price_data ) ) . '" aria-label="' . esc_attr( $this->get_aria_label( $settings ) ) . '">';
		$output .= empty( $settings['label'] ) ? '' : sprintf( '<span class="cpb-input-label">%s</span>', esc_html( $settings['label'] ) );
		$output .= '<ul class="cpb-radio-' . esc_attr( $settings['style'] ) . '">';

		foreach ( $settings['options'] as $index => $option ) {
			$output .= sprintf(
				'<li><label><input type="radio" name="%s" value="%s" %s> <span>%s</span></label></li>',
				esc_attr( $settings['name'] ),
				esc_attr( $option['value'] ),
				checked( $index, $settings['selected'], false ),
				esc_html( $option['label'] )
			);
		}

		$output .= '</ul></div>';

		return $output;
	}
}
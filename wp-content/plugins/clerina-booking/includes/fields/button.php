<?php
class Clerina_Booking_Button_Field extends Clerina_Booking_Field {
	public $type = 'button';

	/**
	 * Template for field settings.
	 */
	public function settings_template_content() {
		?>

		<input type="hidden" value="<?php echo esc_attr( $this->type ) ?>" name="<?php echo $this->setting_name( 'type' ); ?>">

		<table class="form-table">
			<tr>
				<th><label><?php _e( 'Button Text', 'clerina-booking' ) ?></label></th>
				<td><input type="text" value="{{data.label}}" name="<?php echo $this->setting_name( 'label' ); ?>" class="widefat field-label-input"></td>
			</tr>
			<tr>
				<th><label><?php _e( 'Click Action', 'clerina-booking' ) ?></label></th>
				<td>
					<select name="<?php echo $this->setting_name( 'behavior' ); ?>" class="button-action-toggle">
						<option value="submit" {{ 'submit' == data.behavior ? 'selected' : '' }}><?php esc_attr_e( 'Submit the form', 'leanpro-booking' ) ?></option>
						<option value="confirm" {{ 'confirm' == data.behavior ? 'selected' : '' }}><?php esc_attr_e( 'Confirm to submit', 'leanpro-booking' ) ?></option>
						<option value="open_new_form" {{ 'open_new_form' == data.behavior ? 'selected' : '' }}><?php esc_attr_e( 'Open another form', 'leanpro-booking' ) ?></option>
					</select>
					<input type="text" name="<?php echo $this->setting_name( 'linked' ); ?>" value="{{data.linked}}" placeholder="<?php esc_attr_e( 'Enter the form ID', 'clerina-booking' ) ?>" class="linked-form {{ 'open_new_form' == data.behavior ? '' : 'hidden' }}">
				</td>
			</tr>
			<tr>
				<th><label><?php _e( 'Class', 'clerina-booking' ) ?></label></th>
				<td><input type="text" value="{{data.class}}" name="<?php echo $this->setting_name( 'class' ); ?>" class="widefat"></td>
			</tr>
			<tr>
				<th><label><?php _e( 'Admin Label', 'clerina-booking' ) ?></label></th>
				<td>
					<input type="text" value="{{data.admin_label}}" name="<?php echo $this->setting_name( 'admin_label' ); ?>" class="widefat admin-label-input">
					<p class="description"><?php _e( 'The label to be used in the order', 'clerina-booking' ) ?></p>
				</td>
			</tr>
		</table>

		<?php
	}

	/**
	 * Render the field.
	 *
	 * @param array $settings
	 *
	 * @return string
	 */
	public function render( $settings = array() ) {
		$settings = wp_parse_args( $settings, $this->get_default_settings() );

		return sprintf(
			'<div class="cpb-field cpb-%s-field %s"><button type="submit" data-action="%s">%s</button></div>',
			esc_attr( $this->type ),
			esc_attr( $settings['class'] ),
			esc_attr( $settings['behavior'] ),
			esc_html( $settings['label'] )
		);
	}
}
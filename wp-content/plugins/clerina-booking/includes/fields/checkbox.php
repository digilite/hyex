<?php
class Clerina_Booking_Checkbox_Field extends Clerina_Booking_Field {
	public $type = 'checkbox';

	/**
	 * Template for field settings.
	 */
	public function settings_template_content() {
		?>

		<input type="hidden" value="<?php echo esc_attr( $this->type ) ?>" name="<?php echo $this->setting_name( 'type' ); ?>">

		<table class="form-table">
			<tr>
				<th><label><?php _e( 'Required', 'clerina-booking' ) ?></label></th>
				<td><input type="checkbox" value="1" name="<?php echo $this->setting_name( 'required' ); ?>" <?php echo $this->setting_checked( 'required', '1' ) ?>></td>
			</tr>
			<tr>
				<th><label><?php _e( 'Label', 'clerina-booking' ) ?></label></th>
				<td><input type="text" value="{{data.label}}" name="<?php echo $this->setting_name( 'label' ); ?>" class="widefat field-label-input"></td>
			</tr>
			<tr>
				<th><label><?php _e( 'Name', 'clerina-booking' ) ?></label></th>
				<td><input type="text" value="{{data.name}}" name="<?php echo $this->setting_name( 'name' ); ?>" class="widefat"></td>
			</tr>
			<tr>
				<th><label><?php _e( 'Value', 'clerina-booking' ) ?></label></th>
				<td><input type="text" value="{{data.value}}" name="<?php echo $this->setting_name( 'value' ); ?>" class="widefat"></td>
			</tr>
			<tr>
				<th><label><?php _e( 'Class', 'clerina-booking' ) ?></label></th>
				<td><input type="text" value="{{data.class}}" name="<?php echo $this->setting_name( 'class' ); ?>" class="widefat"></td>
			</tr>
			<tr>
				<th><label><?php _e( 'Price', 'clerina-booking' ) ?></label></th>
				<td><input type="text" value="{{data.price}}" name="<?php echo $this->setting_name( 'price' ); ?>"></td>
			</tr>
			<tr>
				<th><label><?php _e( 'Admin Label', 'clerina-booking' ) ?></label></th>
				<td>
					<input type="text" value="{{data.admin_label}}" name="<?php echo $this->setting_name( 'admin_label' ); ?>" class="widefat admin-label-input">
					<p class="description"><?php _e( 'The label to be used in the order', 'clerina-booking' ) ?></p>
				</td>
			</tr>
		</table>

		<?php
	}

	/**
	 * Render the field.
	 *
	 * @param array $settings
	 *
	 * @return string
	 */
	public function render( $settings = array() ) {
		$settings = wp_parse_args( $settings, $this->get_default_settings() );

		$label = empty( $settings['label'] ) ? '' : sprintf( '<span class="cpb-input-label">%s</span>', esc_html( $settings['label'] ) );

		return sprintf(
			'<div class="cpb-field cpb-checkbox-field %s" data-price="%s" aria-label="%s"><label><input type="checkbox" name="%s" value="%s" class="cpb-input" %s> %s</label></div>',
			esc_attr( $settings['class'] ),
			floatval( $settings['price'] ),
			esc_attr( $this->get_aria_label( $settings ) ),
			esc_attr( $settings['name'] ),
			esc_attr( $settings['value'] ),
			$settings['required'] ? 'required' : '',
			$label
		);
	}
}
<?php
class Clerina_Booking_Select_Field extends Clerina_Booking_Field {
	public $type = 'select';

	/**
	 * Template for field settings.
	 */
	public function settings_template() {
		?>

		<script type="text/html" id="tmpl-cpb-field-<?php echo $this->type ?>-settings">
			<?php $this->settings_template_content(); ?>
		</script>

		<script type="text/html" id="tmpl-cpb-field-<?php echo $this->type ?>-option">
			<?php $this->option_settings_template_content(); ?>
		</script>

		<?php
	}

	/**
	 * Template for field settings.
	 */
	public function settings_template_content() {
		?>

		<input type="hidden" value="<?php echo esc_attr( $this->type ) ?>" name="<?php echo $this->setting_name( 'type' ); ?>">
		<input type="hidden" value="<?php echo $this->setting_name( 'options' ); ?>" class="option-base-name">

		<table class="form-table">
			<tr>
				<th><label><?php _e( 'Required', 'clerina-booking' ) ?></label></th>
				<td><input type="checkbox" value="1" name="<?php echo $this->setting_name( 'required' ); ?>" <?php echo $this->setting_checked( 'required', '1' ) ?>></td>
			</tr>
			<tr>
				<th><label><?php _e( 'Label', 'clerina-booking' ) ?></label></th>
				<td><input type="text" value="{{data.label}}" name="<?php echo $this->setting_name( 'label' ); ?>" class="widefat field-label-input"></td>
			</tr>
			<tr>
				<th><label><?php _e( 'Name', 'clerina-booking' ) ?></label></th>
				<td><input type="text" value="{{data.name}}" name="<?php echo $this->setting_name( 'name' ); ?>" class="widefat"></td>
			</tr>
			<tr>
				<th><label><?php _e( 'Class', 'clerina-booking' ) ?></label></th>
				<td><input type="text" value="{{data.class}}" name="<?php echo $this->setting_name( 'class' ); ?>" class="widefat"></td>
			</tr>
			<tr>
				<th><label><?php _e( 'Options', 'clerina-booking' ) ?></label></th>
				<td>
					<div class="field-options" data-index="{{data.index}}">
						<# if ( ! _.isEmpty( data.options ) ) { #>
							<# _.each( data.options, function( value, key ) { #>
								<?php $this->option_settings_template_content(); ?>
							<# } ); #>
						<# } else { #>
							<# var key = 0, value = {label: '', value: '', price: ''}; #>
							<?php $this->option_settings_template_content(); ?>
						<# } #>
					</div>
					<p>
						<a href="#" class="field-options-add-new" data-type="<?php echo esc_attr( $this->type ) ?>"><?php _e( '+ Add New Option', 'clerina-booking' ) ?></a>
					</p>
				</td>
			</tr>
			<tr>
				<th><label><?php _e( 'Admin Label', 'clerina-booking' ) ?></label></th>
				<td>
					<input type="text" value="{{data.admin_label}}" name="<?php echo $this->setting_name( 'admin_label' ); ?>" class="widefat admin-label-input">
					<p class="description"><?php _e( 'The label to be used in the order', 'clerina-booking' ) ?></p>
				</td>
			</tr>
		</table>

		<?php
	}

	/**
	 * Template for a single option.
	 */
	public function option_settings_template_content() {
		?>

		<div class="field-option">
			<span class="field-option-handle"><i class="dashicons dashicons-menu"></i></span>
			<input type="radio" name="<?php echo $this->setting_name( 'selected' ); ?>" value="{{key}}" {{data.selected == key ? "checked" : ""}} class="field-options-default-option">
			<input type="text" name="<?php echo $this->setting_name( 'options' ); ?>[{{key}}][label]" placeholder="<?php esc_attr_e( 'Label', 'clerina-booking' ) ?>" value="{{value.label}}">
			<input type="text" name="<?php echo $this->setting_name( 'options' ); ?>[{{key}}][value]" placeholder="<?php esc_attr_e( 'Value', 'clerina-booking' ) ?>" value="{{value.value}}">
			<input type="text" name="<?php echo $this->setting_name( 'options' ); ?>[{{key}}][price]" placeholder="<?php esc_attr_e( 'Price', 'clerina-booking' ) ?>" value="{{value.price}}">
			<span class="field-option-remove"><i class="dashicons dashicons-no-alt"></i></span>
		</div>

		<?php
	}

	/**
	 * Render the field.
	 *
	 * @param array $settings
	 *
	 * @return string
	 */
	public function render( $settings = array() ) {
		$settings = wp_parse_args( $settings, $this->get_default_settings() );
		$price_data = $this->get_price_data( $settings['options'] );

		$output = '<div class="cpb-field cpb-select-field ' . esc_attr( $settings['class'] ) . '" data-price="' . esc_attr( json_encode( $price_data ) ) . '" aria-label="' . esc_attr( $this->get_aria_label( $settings ) ) . '">';
		$output .= empty( $settings['label'] ) ? '' : sprintf( '<span class="cpb-input-label">%s</span>', esc_html( $settings['label'] ) );
		$output .= '<select name="' . esc_attr( $settings['name'] ) . '" ' . ( ! empty( $settings['required'] ) ? 'required' : '' ) . '>';

		foreach ( $settings['options'] as $index => $option ) {
			$output .= sprintf(
				'<option value="%s" %s> %s</option>',
				esc_attr( $option['value'] ),
				selected( $index, $settings['selected'], false ),
				esc_html( $option['label'] )
			);
		}

		$output .= '</select></div>';

		return $output;
	}

	/**
	 * Get price data
	 * 
	 * @param  array  $options
	 * @return array
	 */
	protected function get_price_data( $options = array() ) {
		$data = array();

		foreach ( $options as $option ) {
			$data[ $option['value'] ] = $option['price'];
		}

		return $data;
	}
}
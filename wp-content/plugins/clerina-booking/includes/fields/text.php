<?php
class Clerina_Booking_Text_Field extends Clerina_Booking_Field {
	public $type = 'text';

	/**
	 * Template for field settings.
	 */
	public function settings_template_content() {
		?>

		<input type="hidden" value="<?php echo esc_attr( $this->type ) ?>" name="<?php echo $this->setting_name( 'type' ); ?>">

		<table class="form-table">
			<tr>
				<th><label><?php _e( 'Required', 'clerina-booking' ) ?></label></th>
				<td><input type="checkbox" value="1" name="<?php echo $this->setting_name( 'required' ); ?>" <?php echo $this->setting_checked( 'required', '1' ) ?>></td>
			</tr>
			<tr>
				<th><label><?php _e( 'Label', 'clerina-booking' ) ?></label></th>
				<td><input type="text" value="{{data.label}}" name="<?php echo $this->setting_name( 'label' ); ?>" class="widefat field-label-input"></td>
			</tr>
			<tr>
				<th><label><?php _e( 'Name', 'clerina-booking' ) ?></label></th>
				<td><input type="text" value="{{data.name}}" name="<?php echo $this->setting_name( 'name' ); ?>" class="widefat"></td>
			</tr>
			<tr>
				<th><label><?php _e( 'Value', 'clerina-booking' ) ?></label></th>
				<td><input type="text" value="{{data.value}}" name="<?php echo $this->setting_name( 'value' ); ?>" class="widefat"></td>
			</tr>
			<tr>
				<th><label><?php _e( 'Placeholder', 'clerina-booking' ) ?></label></th>
				<td><input type="text" value="{{data.placeholder}}" name="<?php echo $this->setting_name( 'placeholder' ); ?>" class="widefat"></td>
			</tr>
			<tr>
				<th><label><?php _e( 'Class', 'clerina-booking' ) ?></label></th>
				<td><input type="text" value="{{data.class}}" name="<?php echo $this->setting_name( 'class' ); ?>" class="widefat"></td>
			</tr>
			<tr>
				<th><label><?php _e( 'Admin Label', 'clerina-booking' ) ?></label></th>
				<td>
					<input type="text" value="{{data.admin_label}}" name="<?php echo $this->setting_name( 'admin_label' ); ?>" class="widefat admin-label-input">
					<p class="description"><?php _e( 'The label to be used in the order', 'clerina-booking' ) ?></p>
				</td>
			</tr>
		</table>

		<?php
	}
}
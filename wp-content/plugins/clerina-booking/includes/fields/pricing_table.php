<?php

class Clerina_Booking_Pricing_Table_Field extends Clerina_Booking_Estimate_Field {
	public $type = 'pricing_table';

	/**
	 * Template for field settings.
	 */
	public function settings_template_content() {
		?>

		<input type="hidden" value="<?php echo esc_attr( $this->type ) ?>" name="<?php echo $this->setting_name( 'type' ); ?>">

		<table class="form-table">
			<tr>
				<th><label><?php _e( 'Label', 'clerina-booking' ) ?></label></th>
				<td>
					<input type="text" value="{{data.label}}" name="<?php echo $this->setting_name( 'label' ); ?>" class="widefat field-label-input">
				</td>
			</tr>
			<tr>
				<th><label><?php _e( 'Name', 'clerina-booking' ) ?></label></th>
				<td>
					<input type="text" value="{{data.name}}" name="<?php echo $this->setting_name( 'name' ); ?>" class="widefat">
					<p class="description"><?php _e( 'If you have more than one table, you should give them the same name.', 'clerina-booking' ) ?></p>
				</td>
			</tr>
			<tr>
				<th><label><?php _e( 'Base Price', 'clerina-booking' ) ?></label></th>
				<td><input type="text" value="{{data.base}}" name="<?php echo $this->setting_name( 'base' ); ?>"></td>
			</tr>
			<tr>
				<th><label><?php _e( 'Currency', 'clerina-booking' ) ?></label></th>
				<td>
					<input type="text" value="{{data.currency}}" name="<?php echo $this->setting_name( 'currency' ); ?>">
				</td>
			</tr>
			<tr>
				<th><label><?php _e( 'Currency Position', 'clerina-booking' ) ?></label></th>
				<td>
					<select value="{{data.currency_position}}" name="<?php echo $this->setting_name( 'currency_position' ); ?>">
						<option value="left" {{
						'left' == data.currency_position ? 'selected' : ''}}><?php _e( 'Left', 'clerina-booking' ) ?></option>
						<option value="right" {{
						'right' == data.currency_position ? 'selected' : ''}}><?php _e( 'Right', 'clerina-booking' ) ?></option>
						<option value="left_space" {{
						'left_space' == data.currency_position ? 'selected' : ''}}><?php _e( 'Left with space', 'clerina-booking' ) ?></option>
						<option value="right_space" {{
						'right_space' == data.currency_position ? 'selected' : ''}}><?php _e( 'Right with space', 'clerina-booking' ) ?></option>
					</select>
				</td>
			</tr>
			<tr>
				<th><label><?php _e( 'Number of decimals', 'clerina-booking' ) ?></label></th>
				<td>
					<input type="number" value="{{data.num_decimals}}" name="<?php echo $this->setting_name( 'num_decimals' ); ?>">
				</td>
			</tr>
			<tr>
				<th><label><?php _e( 'Unit', 'clerina-booking' ) ?></label></th>
				<td>
					<input type="text" value="{{data.unit}}" name="<?php echo $this->setting_name( 'unit' ); ?>" class="widefat">
				</td>
			</tr>
			<tr>
				<th><label><?php _e( 'Description', 'clerina-booking' ) ?></label></th>
				<td>
					<textarea name="<?php echo $this->setting_name( 'description' ); ?>" class="widefat">{{data.description}}</textarea>
				</td>
			</tr>
			<tr>
				<th><label><?php _e( 'Button Text', 'clerina-booking' ) ?></label></th>
				<td>
					<input type="text" value="{{data.button_text}}" name="<?php echo $this->setting_name( 'button_text' ); ?>" class="widefat">
				</td>
			</tr>
			<tr>
				<th><label><?php _e( 'Button Behavior', 'clerina-booking' ) ?></label></th>
				<td>
					<select name="<?php echo $this->setting_name( 'button_behavior' ); ?>" class="button-action-toggle">
						<option value="submit" {{ 'submit' == data.button_behavior ? 'selected' : '' }}><?php esc_attr_e( 'Submit the form', 'leanpro-booking' ) ?></option>
						<option value="confirm" {{ 'confirm' == data.button_behavior ? 'selected' : '' }}><?php esc_attr_e( 'Confirm to submit', 'leanpro-booking' ) ?></option>
						<option value="open_new_form" {{ 'open_new_form' == data.button_behavior ? 'selected' : '' }}><?php esc_attr_e( 'Open another form', 'leanpro-booking' ) ?></option>
					</select>
					<input type="text" name="<?php echo $this->setting_name( 'button_linked' ); ?>" value="{{data.button_linked}}" placeholder="<?php esc_attr_e( 'Enter the form ID', 'clerina-booking' ) ?>" class="linked-form {{ 'open_new_form' == data.button_behavior ? '' : 'hidden' }}">
				</td>
			</tr>
			<tr>
				<th><label><?php _e( 'Class', 'clerina-booking' ) ?></label></th>
				<td>
					<input type="text" value="{{data.class}}" name="<?php echo $this->setting_name( 'class' ); ?>" class="widefat">
				</td>
			</tr>
			<tr>
				<th><label><?php _e( 'Admin Label', 'clerina-booking' ) ?></label></th>
				<td>
					<input type="text" value="{{data.admin_label}}" name="<?php echo $this->setting_name( 'admin_label' ); ?>" class="widefat admin-label-input">
					<p class="description"><?php _e( 'The label to be used in the order', 'clerina-booking' ) ?></p>
				</td>
			</tr>
		</table>

		<?php
	}

	/**
	 * Render the field.
	 *
	 * @param array $settings
	 *
	 * @return string
	 */
	public function render( $settings = array() ) {
		$settings = wp_parse_args( $settings, $this->get_default_settings() );
		$estimate_settings   = array(
			'name' => $settings['name'] . '_estimate_price',
			'base'              => $settings['base'],
			'currency'          => $settings['currency'],
			'currency_position' => $settings['currency_position'],
			'num_decimals'      => $settings['num_decimals'],
		);

		$price = parent::render( $estimate_settings );

		return sprintf(
			'<div class="cpb-field cpb-%s-field %s" aria-label="%s">
				<div class="cpb-pricing-table-name">%s</div>
				%s
				<div class="cpb-pricing-table-unit">%s</div>
				<div class="cpb-pricing-table-description">%s</div>
				<input type="radio" name="%s" value="%s" class="pricing-table-name-input">
				<button type="submit" data-action="%s" data-linked_form="%s">%s</button>
			</div>',
			esc_attr( $this->type ),
			esc_attr( $settings['class'] ),
			esc_attr( $this->get_aria_label( $settings ) ),
			esc_html( $settings['label'] ),
			$price,
			$settings['unit'] ? '<span class="cpb-pricing-unit-separator">/</span>' . $settings['unit'] : '',
			wpautop( $settings['description'] ),
			esc_attr( $settings['name'] ),
			esc_attr( $settings['label'] ),
			esc_html( $settings['button_behavior'] ),
			'open_new_form' == $settings['button_behavior'] ? esc_html( $settings['button_linked'] ) : '',
			esc_html( $settings['button_text'] )
		);
	}
}
<?php
class Clerina_Booking_Paragraph_Field extends Clerina_Booking_Field {
	public $type = 'paragraph';

	/**
	 * Template for field settings.
	 */
	public function settings_template_content() {
		?>

		<input type="hidden" value="<?php echo esc_attr( $this->type ) ?>" name="<?php echo $this->setting_name( 'type' ); ?>">

		<table class="form-table">
			<tr>
				<th><label><?php _e( 'Text', 'clerina-booking' ) ?></label></th>
				<td><textarea rows="10" name="<?php echo $this->setting_name( 'text' ); ?>" class="widefat field-label-input">{{data.text}}</textarea></td>
			</tr>
			<tr>
				<th><label><?php _e( 'Class', 'clerina-booking' ) ?></label></th>
				<td><input type="text" value="{{data.class}}" name="<?php echo $this->setting_name( 'class' ); ?>" class="widefat"></td>
			</tr>
		</table>

		<?php
	}

	/**
	 * Render the field.
	 *
	 * @param array $settings
	 *
	 * @return string
	 */
	public function render( $settings = array() ) {
		$settings = wp_parse_args( $settings, $this->get_default_settings() );

		return sprintf(
			'<div class="cpb-field cpb-paragraph-field %s">%s</div>',
			esc_attr( $settings['class'] ),
			esc_attr( $settings['text'] )
		);
	}
}